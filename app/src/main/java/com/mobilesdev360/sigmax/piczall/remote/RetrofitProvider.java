package com.mobilesdev360.sigmax.piczall.remote;

import com.mobilesdev360.sigmax.piczall.utils.Paths;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitProvider {
    private static Retrofit retrofit = null;

   public static Retrofit getClient() {

        retrofit = new Retrofit.Builder()
                .baseUrl(Paths._BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

}

