package com.mobilesdev360.sigmax.piczall.remote;

import com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel;
import com.mobilesdev360.sigmax.piczall.utils.model.checkchase.CheckPurchase;
import com.mobilesdev360.sigmax.piczall.utils.model.getwallpaperrating.GetWallpaperRating;
import com.mobilesdev360.sigmax.piczall.utils.model.purchasewallpaper.PurchaseWallpaper;
import com.mobilesdev360.sigmax.piczall.utils.model.ratevideowallpaper.RateVideoWallpaper;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface SOService {
//    @GET("api/featured_videos_wallpapers/0/10")
    @GET("api/featured_videos_wallpapers2/0/10")

    Call<VideoWallpaperModel> getVideoData();
//    @GET("api/featured_videos_wallpapers/0/100")
  @GET("api/featured_videos_wallpapers2/0/100")
    Call<VideoWallpaperModel> getVideoDataall();
    //checkpurchase api
    @FormUrlEncoded
    @POST("userpurchase")
    Call<CheckPurchase> checkPurchaseWallpaper(@Field("user_id")String user_id,@Field("v_wallpaper_id")String v_wallpaper_id);

    //get wallpaper rating api
    @FormUrlEncoded
    @POST("getratting")
    Call<GetWallpaperRating> getWallpaperRating(@Field("wallpaper_id")String wallpaper_id, @Field("wallpaper_type")String wallpaper_type);

    //check purchased wallpaper api
    @FormUrlEncoded
    @POST("userpurchase")
    Call<CheckPurchase> checkPurchasedWallpaperById(@Field("user_id")String user_id, @Field("v_wallpaper_id")String v_wallpaper_id);

    //rate video wallpaper api
    @FormUrlEncoded
    @POST("rate")
    Call<RateVideoWallpaper> rateVideoWallpaperById(@Field("user_id")String user_id, @Field("wallpaper_id")String wallpaper_id,@Field("wallpaper_type")String wallpaper_type, @Field("rate")Float rate, @Field("comment")String comment);

    //purchase video wallpaper api
    @FormUrlEncoded
    @POST("purchasewallpaper")
    Call<PurchaseWallpaper> purchaseWallpaper(@Field("user_id")String user_id, @Field("v_wallpaper_id")String v_wallpaper_id);
}
