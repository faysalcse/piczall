package com.mobilesdev360.sigmax.piczall.utils.videowallpaperservice;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.Toast;

import com.mobilesdev360.sigmax.piczall.utils.ApplicationConstraints;
import com.mobilesdev360.sigmax.piczall.utils.preferenceutil.SharedPref;
import com.mobilesdev360.sigmax.piczall.utils.videofilepath.VideoFileUtils;


public class VideoWallpaperService extends WallpaperService {


    protected static int playheadTime = 0;
    public String name;


    @Override
    public Engine onCreateEngine() {
        return new VideoEngine();
    }

    class VideoEngine extends Engine implements SharedPreferences.OnSharedPreferenceChangeListener {

        private final String TAG = getClass().getSimpleName();
        private MediaPlayer mediaPlayer;
        private SharedPreferences mPreferences;
        SurfaceHolder holder;

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);

            mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            mPreferences.registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();

            // Don't forget to unregister the listener
            mPreferences.unregisterOnSharedPreferenceChangeListener(this);
        }

        public VideoEngine() {
            super();
            try {
                String path = SharedPref.read(ApplicationConstraints.WALLPAPER_NAME);
                System.out.println("------------------------------->>>>>> " +path);
                Uri uri = Uri.fromFile(VideoFileUtils.GetLocalFileByNames(getApplicationContext(), SharedPref.read(ApplicationConstraints.WALLPAPER_NAME)));
                mediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.parse(path));
                //mediaPlayer = new MediaPlayer();
                //mediaPlayer.setDataSource(path);
                mediaPlayer.setLooping(true);
            } catch (Exception ex) {

            }
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder) {
            if (mediaPlayer != null) {
                mediaPlayer.setSurface(holder.getSurface());
                mediaPlayer.start();
            }
            this.holder = holder;
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            Log.i(TAG, "( INativeWallpaperEngine ): onSurfaceDestroyed");
            if (mediaPlayer != null) {
                playheadTime = mediaPlayer.getCurrentPosition();
                mediaPlayer.reset();
                mediaPlayer.release();
            }
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(ApplicationConstraints.WALLPAPER_NAME)) {
                String path = SharedPref.read(ApplicationConstraints.WALLPAPER_NAME);
                Uri uri = Uri.fromFile(VideoFileUtils.GetLocalFileByNames(getApplicationContext(), SharedPref.read(ApplicationConstraints.WALLPAPER_NAME)));
                if (mediaPlayer != null) {
                    mediaPlayer.release();
                }
                mediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.parse(path));
                //mediaPlayer = new MediaPlayer();
                //mediaPlayer.setDataSource(path);
                mediaPlayer.setLooping(true);
                mediaPlayer.setSurface(holder.getSurface());
                mediaPlayer.start();
            }
        }
    }
}