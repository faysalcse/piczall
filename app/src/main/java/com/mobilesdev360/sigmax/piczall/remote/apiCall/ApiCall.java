package com.mobilesdev360.sigmax.piczall.remote.apiCall;

import android.util.Log;

import com.mobilesdev360.sigmax.piczall.remote.RetrofitProvider;
import com.mobilesdev360.sigmax.piczall.remote.SOService;
import com.mobilesdev360.sigmax.piczall.utils.model.checkchase.CheckPurchase;
import com.mobilesdev360.sigmax.piczall.utils.model.getwallpaperrating.GetWallpaperRating;
import com.mobilesdev360.sigmax.piczall.utils.model.purchasewallpaper.PurchaseWallpaper;
import com.mobilesdev360.sigmax.piczall.utils.model.ratevideowallpaper.RateVideoWallpaper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiCall {
    private SOService mService;
    private static final ApiCall ourInstance = new ApiCall();
    private ApiCallListener listener;

    public static ApiCall getInstance() {

        return ourInstance;
    }

    private ApiCall() {
    }

    //must call
    public void initApiCall(ApiCallListener listener) {
        mService = RetrofitProvider.getClient().create(SOService.class);
        this.listener = listener;
    }

    /**
     * @param user_id        wallpaperid
     * @param v_wallpaper_id
     */
    public void callCheckPurchaseApi(String user_id, String v_wallpaper_id) {
        if (listener != null && mService != null) {
            mService.checkPurchasedWallpaperById(user_id, v_wallpaper_id).enqueue(new Callback<CheckPurchase>() {
                @Override
                public void onResponse(Call<CheckPurchase> call, Response<CheckPurchase> response) {
                    if (response.isSuccessful()) {
                        CheckPurchase checkPurchase = response.body();
                        listener.OnCheckPurchaseSuccess(checkPurchase);
                    } else {
                        responseError("Error:" + response.code());
                    }
                }

                @Override
                public void onFailure(Call<CheckPurchase> call, Throwable t) {
                    responseError("Failure:" + t.getMessage());
                }
            });
        } else {
            responseError("Class initialization failed");
        }
    }

    /**
     * @param wallpaper_id   wallpaper id
     * @param wallpaper_type wallpaper type
     */
    public void callRateWallpaperRatingApi(String wallpaper_id, String wallpaper_type) {
        if (listener != null && mService != null) {
            mService.getWallpaperRating(wallpaper_id, wallpaper_type).enqueue(new Callback<GetWallpaperRating>() {
                @Override
                public void onResponse(Call<GetWallpaperRating> call, Response<GetWallpaperRating> response) {
                    if (response.isSuccessful()) {
                        GetWallpaperRating getWallpaperRating = response.body();
                        listener.OnGetWallpaperRatingSuccess(getWallpaperRating);
                    } else {
                        responseError("Error:" + response.code());
                    }

                }

                @Override
                public void onFailure(Call<GetWallpaperRating> call, Throwable t) {
                    responseError("Failure:" + t.getMessage());
                }
            });
        } else {
            responseError("Class initialization failed");
        }
    }

    /**
     * @param user_id        userid
     * @param v_wallpaper_id video wallpaper id
     */
    public void callPurchaseWallpaperApi(String user_id, String v_wallpaper_id) {
        if (listener != null && mService != null) {
            mService.purchaseWallpaper(user_id, v_wallpaper_id).enqueue(new Callback<PurchaseWallpaper>() {
                @Override
                public void onResponse(Call<PurchaseWallpaper> call, Response<PurchaseWallpaper> response) {
                    if (response.isSuccessful()) {
                        PurchaseWallpaper purchaseWallpaper = response.body();
                        listener.OnPurchaseWallpaperSuccess(purchaseWallpaper);
                    } else {
                        responseError("Error:" + response.code());
                    }

                }

                @Override
                public void onFailure(Call<PurchaseWallpaper> call, Throwable t) {
                    responseError("Failure:" + t.getMessage());
                }
            });
        } else {
            responseError("Class initialization failed");
        }
    }

    /**
     * @param user_id        userid
     * @param wallpaper_id   wallpaperid
     * @param wallpaper_type wallapertype
     * @param rate           rate
     * @param comment        comment
     */
    public void callRateVideoWallpaperApi(String user_id, String wallpaper_id, String wallpaper_type, String rate, String comment) {
        if (listener != null && mService != null) {
            mService.rateVideoWallpaperById(user_id, wallpaper_id, wallpaper_type, Float.parseFloat(""+rate), comment).enqueue(new Callback<RateVideoWallpaper>() {
                @Override
                public void onResponse(Call<RateVideoWallpaper> call, Response<RateVideoWallpaper> response) {
                    if (response.isSuccessful()) {
                        RateVideoWallpaper rateVideoWallpaper = response.body();
                        listener.OnRateVideoWallpaperSuccess(rateVideoWallpaper);
                        responseError("rate success:" + rateVideoWallpaper.toString());
                    } else {
                        responseError("Error:" + response.code());
                    }
                }


                @Override
                public void onFailure(Call<RateVideoWallpaper> call, Throwable t) {
                    responseError("Failure:" + t.getMessage());
                }
            });
        } else {
            responseError("Class initialization failed");
        }
    }

    //if(listener!=null&&mService!=null) {
//
//    }
//        else {
//        responseError("Class initialization failed");
//    }
    private void responseError(String message) {

       Log.d("chkfailure",message);

    }
}
