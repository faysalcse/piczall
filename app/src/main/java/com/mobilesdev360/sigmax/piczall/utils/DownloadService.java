package com.mobilesdev360.sigmax.piczall.utils;

import android.app.Service;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import androidx.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DownloadService extends Service {
    public static boolean isServiceRunning = false;
    Intent myIntent;
    Target target;
    Uri contentUri=null;

    public DownloadService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isServiceRunning = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        myIntent = intent;
        String responce = intent.getStringExtra("ImageAndOption");
        String[] linkAndCode = responce.split(",");
        DownloadImage(linkAndCode[0], Integer.parseInt(linkAndCode[1]), Integer.parseInt(linkAndCode[2]));
        return START_STICKY;
    }

    // In case the service is deleted or crashes some how
    @Override
    public void onDestroy() {
        isServiceRunning = false;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    void DownloadImage(String imageName, final int OnlyDownload, int idOfWallpaper) {
        Toast.makeText(DownloadService.this, "Downloading Start", Toast.LENGTH_SHORT).show();

        target = new Target() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                SaveFileIntoStorage(bitmap, idOfWallpaper);
                if (OnlyDownload == 200) {
                    try {
                        WallpaperManager myWallpaperManager = WallpaperManager.getInstance(DownloadService.this);
                        if (bitmap != null) {
                            myWallpaperManager.setBitmap(bitmap);
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("error",e.getMessage());
                    }
                } else if (OnlyDownload == 300) {
                    WallpaperManager myWallpaperManager
                            = WallpaperManager.getInstance(DownloadService.this);
                    try {

                        if (bitmap != null) {
                            myWallpaperManager.setBitmap(bitmap);
//                            myWallpaperManager.setBitmap(bitmap,null, true, WallpaperManager.FLAG_LOCK);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                Toast.makeText(DownloadService.this, "Successfully Download", Toast.LENGTH_SHORT).show();
                stopService(myIntent);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                Toast.makeText(DownloadService.this, "Downloading Failed", Toast.LENGTH_SHORT).show();
                stopService(myIntent);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.get()
                .load(imageName)
                .into(target);
    }

    boolean SaveFileIntoStorage(Bitmap imgBitmap, int idOfWallpaper) {
        boolean isSave = true;
        File sd = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File imageFolder = new File(sd.getAbsolutePath() + File.separator + Paths.imagesPathLocal);

        if (!imageFolder.isDirectory()) {
            imageFolder.mkdirs();
        }

        File mediaFile = new File(imageFolder + File.separator + "img_" + idOfWallpaper + ".jpg");

        Log.e("tag"," = =  image path  =  = " + mediaFile.getAbsolutePath());
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(mediaFile);
            imgBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.close();

            Uri contentUri = Uri.fromFile(mediaFile);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(contentUri);
            sendBroadcast(mediaScanIntent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            isSave = false;
        } catch (IOException e) {
            e.printStackTrace();
            isSave = false;
        }
        return isSave;
    }


}
