package com.mobilesdev360.sigmax.piczall.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.adapters.VideoAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.fragment.VideoWallpaper;
import com.mobilesdev360.sigmax.piczall.remote.RetrofitProvider;
import com.mobilesdev360.sigmax.piczall.remote.SOService;
import com.mobilesdev360.sigmax.piczall.utils.ApplicationConstraints;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel;
import com.mobilesdev360.sigmax.piczall.utils.preferenceutil.SharedPref;
import com.mobilesdev360.sigmax.piczall.utils.videowallpaperservice.VideoWallpaperService;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class VideoViews extends Activity {
    private ImageData imageData;
    private SOService mService;
    public ViewPager viewPager;
    ArrayList<ImageData> listOfTopImages;
    private Gson gson;
    protected String WallpaperName;
    VideoAdapter videoAdapter;


    Uri uri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gson = new Gson();
        setContentView(R.layout.video_main_layout);
        viewPager = findViewById(R.id.videopager);

        mService = RetrofitProvider.getClient().create(SOService.class);
        String imageDataString = getIntent().getStringExtra("SelectedImage");
        imageData = gson.fromJson(imageDataString, ImageData.class);
        listOfTopImages = new ArrayList<>();
        listOfTopImages.add(imageData);
        viewPager.setPageMargin(10);
        videoAdapter = new VideoAdapter(this, listOfTopImages);
        viewPager.setAdapter(videoAdapter);
        getVideoWallpaerper();
        initialInMobiBannerAd();
    }

    private void initialInMobiBannerAd() {
        AdView adView = findViewById(R.id.adView);
        adView.loadAd(new AdRequest.Builder().build());
    }

    private VideoWallpaper getVideoWallpaerper() {
        mService.getVideoDataall().enqueue(new Callback<VideoWallpaperModel>() {
            @Override
            public void onResponse(Call<VideoWallpaperModel> call, retrofit2.Response<VideoWallpaperModel> response) {
                if (response.isSuccessful()) {
                    VideoWallpaperModel model = response.body();
                    List<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper> videoWallpaperList = model.getVideoWallpaper();
                    Log.d("chkresponse", "success" + response.body());
                    initAll(videoWallpaperList);
                } else {
                    Log.d("chkresponse", "error:" + response.code());
                }
            }

            @Override
            public void onFailure(Call<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel> call, Throwable t) {
                Log.d("chkresponse", "failed:" + t.getMessage());
            }
        });
        return null;
    }

    private void initAll(List<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper> videoWallpapers) {
        ArrayList<ImageData> list = new ArrayList<>();

        for (com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper v : videoWallpapers) {
            ImageData imageData = new ImageData();
            imageData.setId(v.getId());
            imageData.setWallpaperName(v.getWallpaperName());
            imageData.setCategoryId(3);
            imageData.setLikeCount(3);
            imageData.setIsLike(2);
            imageData.setVideoType(true);
            imageData.setCredits("2.0");
            imageData.setIs_premium(v.getIs_premium());


            //new
            imageData.setAverageRating(v.getAverageRating());
            imageData.setWallpaper(v.getVideo());
            imageData.setVideo_wallpaper_thumbnail(v.getVideo_wallpaper_thumbnail());
            if (!imageData.getWallpaperName().equals(listOfTopImages.get(0).getWallpaperName())) {
                list.add(imageData);
            } else {
                Log.e("Not added", imageData.getWallpaperName());
            }
        }
        ImageData temp = new ImageData();
        temp.isAd = true;
        Log.w("NATIVE_ADS", "index: " + listOfTopImages.size());
        listOfTopImages.addAll(list);
        for (int i = 5; i < listOfTopImages.size(); i = i + 6) {
            listOfTopImages.add(i, temp);
        }

        videoAdapter.notifyDataSetChanged();
    }

    class DownloadFile extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            this.progressDialog = new ProgressDialog(VideoViews.this);
            this.progressDialog.setMax(100);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setMessage("Please Wait Wallpaper Downloading");
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
            File existsfile = new File(getExternalFilesDir(null).getAbsolutePath() + "/PiczallVideo/" + WallpaperName + ".mp4");
            if (existsfile.exists()) {
                existsfile.delete();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String path = params[0];
            try {
                URL url = new URL(path);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestProperty("Accept-Encoding", "identity");
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                int file_length = c.getContentLength();
                File new_folder = new File(getExternalFilesDir(null).getAbsolutePath(), "/PiczallVideo/");
                System.out.println("------------------------------->>>>>> " + new_folder);
                if (!new_folder.exists()) {
                    new_folder.mkdir();
                }
                File output_file = new File(new_folder, imageData.getWallpaperName() + ".mp4");
                OutputStream outputStream = new FileOutputStream(output_file);
                InputStream inputStream = new BufferedInputStream(url.openStream(), 8192);


                byte[] data = new byte[1024];
                long total = 0;
                int count;
                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / file_length));
                    progressDialog.setProgress((int) ((total * 100) / file_length));
                    outputStream.write(data, 0, count);
                }
                inputStream.close();
                outputStream.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "Download complete";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null) {
                setThisWallpaper("", WallpaperName, true);
            }
        }
    }

    public void setThisWallpaper(String path, String getWallpaperName, boolean isDownloaded) {
        WallpaperName = getWallpaperName;
        if (isDownloaded) {
            SharedPref.init(this);
            SharedPref.write(ApplicationConstraints.WALLPAPER_NAME, getExternalFilesDir(null).getAbsolutePath() + "/PiczallVideo/" + WallpaperName + ".mp4");
            VideoWallpaperService vid = new VideoWallpaperService();
            Intent intent = new Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
            intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(this, vid.getClass()));
            this.startActivity(intent);
            this.finish();
        } else {
            new DownloadFile().execute(Paths._video + path);
        }
    }
}
