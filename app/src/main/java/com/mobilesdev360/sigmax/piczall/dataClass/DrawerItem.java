package com.mobilesdev360.sigmax.piczall.dataClass;

public class DrawerItem {

    int Image;
    String text;

    public DrawerItem (int Image,String text){
        this.Image=Image;
        this.text=text;
    }
    public void setImage(int image) {
        Image = image;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImage() {
        return Image;
    }

    public String getText() {
        return text;
    }
}
