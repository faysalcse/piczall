package com.mobilesdev360.sigmax.piczall.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class RegularText   extends androidx.appcompat.widget.AppCompatTextView {

    public RegularText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public RegularText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public RegularText(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        // Just Change your font name
        Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "montserrat_semibold.otf");
        setTypeface(myTypeface);
    }

}