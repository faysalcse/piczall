
package com.mobilesdev360.sigmax.piczall.utils.model.getwallpaperrating;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetWallpaperRating {

    @SerializedName("all_ratting")
    @Expose
    private List<AllRatting> allRatting = null;
    @SerializedName("avg_ratting")
    @Expose
    private Double avgRatting;

    public List<AllRatting> getAllRatting() {
        return allRatting;
    }

    public void setAllRatting(List<AllRatting> allRatting) {
        this.allRatting = allRatting;
    }

    public Double getAvgRatting() {
        return avgRatting;
    }

    public void setAvgRatting(Double avgRatting) {
        this.avgRatting = avgRatting;
    }

    @Override
    public String toString() {
        return "GetWallpaperRating{" +
                "allRatting=" + allRatting +
                ", avgRatting=" + avgRatting +
                '}';
    }
}
