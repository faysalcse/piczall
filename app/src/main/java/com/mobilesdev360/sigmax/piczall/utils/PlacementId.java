package com.mobilesdev360.sigmax.piczall.utils;

public interface PlacementId {
    long BANNER_PLACEMENT_ID = 1588824421342L;
    long Interstitial_PLACEMENT_ID = 1588481982467L;
    long NATIVE_PLACEMENT_ID = 1588451351987L;
    long REWARDED_PLACEMENT_ID = 1587767688566L;
}
