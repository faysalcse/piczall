package com.mobilesdev360.sigmax.piczall.remote.apiCall;

import com.mobilesdev360.sigmax.piczall.utils.model.checkchase.CheckPurchase;
import com.mobilesdev360.sigmax.piczall.utils.model.getwallpaperrating.GetWallpaperRating;
import com.mobilesdev360.sigmax.piczall.utils.model.purchasewallpaper.PurchaseWallpaper;
import com.mobilesdev360.sigmax.piczall.utils.model.ratevideowallpaper.RateVideoWallpaper;

public interface ApiCallListener {
    public void OnCheckPurchaseSuccess(CheckPurchase checkPurchase);
    public void OnGetWallpaperRatingSuccess(GetWallpaperRating getWallpaperRating);
    public void OnRateVideoWallpaperSuccess(RateVideoWallpaper rateVideoWallpaper);
    public void OnPurchaseWallpaperSuccess(PurchaseWallpaper purchaseWallpaper);
}
