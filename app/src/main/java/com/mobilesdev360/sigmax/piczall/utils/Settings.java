package com.mobilesdev360.sigmax.piczall.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.User;

import java.util.ArrayList;

public class Settings {
    public static User userObj;
    public static int width = 280;
    public static int height = 500;


    public static void ShowErrorDialog(final Activity activity, int Status, Errors error, String Msg) {

        ImageView imgCross;
        View view;
        final AlertDialog dialog;
        final TextView txtTitle, txtMsg, lbMoreDetail, txtDetails;
        Button btnOk;

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        LayoutInflater inflater = activity.getLayoutInflater();
        view = inflater.inflate(R.layout.message_dia_error, null);
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        imgCross = (ImageView) view.findViewById(R.id.imgCross);

        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtMsg = (TextView) view.findViewById(R.id.txtMsg);
        lbMoreDetail = (TextView) view.findViewById(R.id.lbMoreDetail);
        txtDetails = (TextView) view.findViewById(R.id.txtDetails);

        btnOk = (Button) view.findViewById(R.id.btnOk);

        if (Status == 0) {
            txtTitle.setText("Error");
            txtMsg.setText(Msg);
        } else {
            txtTitle.setText("Error");
            txtMsg.setText(error.getStatusMessage());
            if (error.getErrorDetails() != null && error.getErrorDetails().getClass().equals(String.class)) {
                txtDetails.setText(error.getErrorDetails().toString());
            } else if (error.getErrorDetails() != null) {
                ArrayList<String> list = (ArrayList<String>) error.getErrorDetails();
                String details = "";
                if (list != null && list.size() > 0) {
                    for (int a = 0; a < list.size(); a++) {

                        details += list.get(a) + "\n";
                    }
                    lbMoreDetail.setVisibility(View.VISIBLE);
                    txtDetails.setText(details);
                    lbMoreDetail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            txtDetails.setVisibility(View.VISIBLE);
                        }
                    });

                }
            }

        }

        imgCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();

    }
}
