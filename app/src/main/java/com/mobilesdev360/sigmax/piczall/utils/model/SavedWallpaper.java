package com.mobilesdev360.sigmax.piczall.utils.model;

import io.realm.RealmObject;

public class SavedWallpaper extends RealmObject {


    int iid;

    public SavedWallpaper() {

    }

    public SavedWallpaper(int id) {
        this.iid = id;
    }

    public int getId() {
        return iid;
    }

    public void setId(int id) {
        this.iid = id;
    }
}
