
package com.mobilesdev360.sigmax.piczall.utils.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoWallpaperModel {

    @SerializedName("videos_wallpaper")
    @Expose
    private List<VideoWallpaper> videoWallpaper = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;

    public List<VideoWallpaper> getVideoWallpaper() {
        return videoWallpaper;
    }

    public void setVideoWallpaper(List<VideoWallpaper> videoWallpaper) {
        this.videoWallpaper = videoWallpaper;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "VideoWallpaperModel{" +
                "videoWallpaper=" + videoWallpaper +
                ", status='" + status + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}
