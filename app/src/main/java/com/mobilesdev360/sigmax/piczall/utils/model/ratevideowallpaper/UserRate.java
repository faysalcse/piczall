
package com.mobilesdev360.sigmax.piczall.utils.model.ratevideowallpaper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserRate {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("wallpaper_id")
    @Expose
    private String wallpaperId;
    @SerializedName("wallpaper_type")
    @Expose
    private String wallpaperType;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("comment")
    @Expose
    private String comment;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWallpaperId() {
        return wallpaperId;
    }

    public void setWallpaperId(String wallpaperId) {
        this.wallpaperId = wallpaperId;
    }

    public String getWallpaperType() {
        return wallpaperType;
    }

    public void setWallpaperType(String wallpaperType) {
        this.wallpaperType = wallpaperType;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
