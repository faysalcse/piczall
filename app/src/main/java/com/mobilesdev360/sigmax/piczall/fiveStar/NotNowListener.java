package com.mobilesdev360.sigmax.piczall.fiveStar;

/**
 * Created by angtrim on 14/11/15.
 */
public interface NotNowListener {
    void onNotNow(int stars);
}
