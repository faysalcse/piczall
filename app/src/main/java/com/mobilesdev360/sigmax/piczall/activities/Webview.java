package com.mobilesdev360.sigmax.piczall.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mobilesdev360.sigmax.piczall.R;

public class Webview extends AppCompatActivity {
    private static final String HTML ="\"Route to Area8 - <a href=\\\"https://goo.gl/maps/1kYTbu6ZTTifBA5S8\\\">CLICK HERE</a><br />\\n\" +\n" +
            "                \"<br />\\n\" +\n" +
            "                \"https://my.eventbuizz.com<br />\\n\" +\n" +
            "                \"Entrance&nbsp;<br />\\n\" +\n" +
            "                \":<a href=\\\"http://goo.gl/maps/1kYTbu6ZTTifBA5S8\\\"><img alt=\\\"\\\" src=\\\"https://my.eventbuizz.com/assets/editorImages/1574700430-1.jpg\\\" style=\\\"width: 100%; height: 100%;\\\" /></a><br />\\n\" +\n" +
            "                \"<br />\\n\" +\n" +
            "                \"&nbsp;\"";
    private static final String TEL_PREFIX = "tel:";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        WebView wv = (WebView) findViewById(R.id.webview);
        wv.setWebViewClient(new CustomWebViewClient());
        wv.loadData(HTML, "text/html", "utf-8");
    }

    private class CustomWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView wv, String url) {

                Intent intent = new Intent(Intent.ACTION_ALL_APPS);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                return true;

        }
    }
}
