package com.mobilesdev360.sigmax.piczall.utils.admobutil;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.mobilesdev360.sigmax.piczall.R;

public class AdmobUtil {
    private static final AdmobUtil ourInstance = new AdmobUtil();
    private RewardedAd rewardedAd;
    private AdmobListener admobListener;
    public static AdmobUtil getInstance() {
        return ourInstance;
    }

    private AdmobUtil() {

    }

    public void setAdmobListener(AdmobListener admobListener) {
        this.admobListener = admobListener;
    }

    public void loadRewardAd(final Context context)
    {
        rewardedAd = new RewardedAd(context, context.getString(R.string.admob_reward_video_id));
        final Activity activityContext = (Activity) context;
                RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
            @Override
            public void onRewardedAdLoaded() {
                // Ad successfully loaded.
                RewardedAdCallback adCallback = new RewardedAdCallback() {
                    @Override
                    public void onRewardedAdOpened() {
                        // Ad opened.
                        Log.d("chkReward","reward opened");
                        if(admobListener!=null)
                        {
                            admobListener.OnAdOpen();
                        }
                    }

                    @Override
                    public void onRewardedAdClosed() {
                        // Ad closed.
                        Log.d("chkReward","reward closed");
                        if(admobListener!=null)
                        {
                            admobListener.OnAdClosed();
                        }
                    }

                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem reward) {
                        // User earned reward.
                        Log.d("chkReward","reward earned. point type:"+reward.getType());
                        Log.d("chkReward","reward earned. point amount:"+reward.getAmount());
                        if(admobListener!=null)
                        {
                            admobListener.OnPointEarned(reward);
                        }
                    }

                    @Override
                    public void onRewardedAdFailedToShow(int errorCode) {
                        // Ad failed to display
                        Log.d("chkReward","reward error: "+errorCode);
                        if(admobListener!=null)
                        {
                            admobListener.OnAdFailed("error code:"+errorCode);
                        }

                    }
                };
                rewardedAd.show(activityContext, adCallback);
            }

            @Override
            public void onRewardedAdFailedToLoad(int errorCode) {
                // Ad failed to load.
                Log.d("chkReward","reward ad failed to load. Code: "+errorCode);
                if(admobListener!=null)
                {
                    admobListener.OnAdFailed("Reward failed to load. Error code: "+errorCode);
                }
            }
        };
        rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);

    }

    public static InterstitialAd mAdMobInterstitialAd;
    public static void intentialadd(Context context) {

//            mAdMobInterstitialAd = new InterstitialAd(context);
//            mAdMobInterstitialAd.setAdUnitId(context.getString(R.string.interstitial));
//            AdRequest adRequest = new AdRequest.Builder()
//                    .build();
//            mAdMobInterstitialAd.loadAd(adRequest);
//            show_adds(context);


    }
    public static void show_adds(Context context)
    {

//        mAdMobInterstitialAd.setAdListener(new AdListener() {
//        public void onAdLoaded() {
//            if (mAdMobInterstitialAd.isLoaded()) {
//                mAdMobInterstitialAd.show();
//            }
//            else
//            {
//                Toast.makeText(context,"NO LOADED ADD", Toast.LENGTH_SHORT).show();
//            }
//        }
//        public void onAdClosed()
//        {
////            intentialadd(context);
//        }
//    });
    }


}
