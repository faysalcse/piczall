package com.mobilesdev360.sigmax.piczall.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.adapters.AllPopularImagesAdapter;
import com.mobilesdev360.sigmax.piczall.adapters.PopularHorizontalAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;


public class Popular extends Fragment implements WebServices.RequestResults {
    public static String liked = "1";
    View rootView;
    RecyclerView recycPopularImages;
    List<ImageData> listOfTopImages;
    List<ImageData> listOfHeaderImages;
    PopularHorizontalAdapter topAdapter;
    int pageStart = 0, pageSize = 18;
    boolean canScroll, IsMoreScroll, IsFirstCall = true;
    int lastLastitem = 0;
    //1122 means without login
    //1133 means withlogin
    int RequestCode = 1122;
    ProgressDialog pDialog;
    TextView txtNoDataFound;
    AllPopularImagesAdapter mainAdapter;
    GridViewWithHeaderAndFooter gridView;
    View headerView;
    SwipeRefreshLayout pullToRefresh;

    public Popular() {
        // Required empty public constructor
    }

    //like/wallpaper
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_popular, container, false);
        Initialize(inflater);
        return rootView;
    }

    void Initialize(LayoutInflater inflater) {
        listOfTopImages = new ArrayList();
        listOfHeaderImages = new ArrayList();
        headerView = inflater.inflate(R.layout.gridview_header, null);
        gridView = (GridViewWithHeaderAndFooter) rootView.findViewById(R.id.gridview);
        recycPopularImages = (RecyclerView) headerView.findViewById(R.id.cardView);
        recycPopularImages.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recycPopularImages.setLayoutManager(MyLayoutManager);
        recycPopularImages.setVisibility(View.GONE);
        gridView.addHeaderView(headerView);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");


        pullToRefresh = rootView.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Settings.userObj == null) {
                    RequestCode = 5522;
                    String url = Paths._like_wallpaper + "/" + pageStart + "/" + pageSize;
//                    String url = Paths.BaseUrl + "like/wallpaper?page_num_start=" + 0 + "&page_size=" + 10;
                    WebServices.getInstance().MyGetCallWithoutAuth(getActivity(), url, Popular.this);
                } else {
                    RequestCode = 5533;
//                    String url = Paths.BaseUrl + "like/wallpaper?page_num_start=" + 0 + "&page_size=" + 10 + "&user_id=" + Settings.userObj.getId();
                    String url = Paths._like_wallpaper + "/" + pageStart + "/" + pageSize;
                    WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, Popular.this);
                }
                pullToRefresh.setRefreshing(false);
            }
        });

        if (Settings.userObj == null) {
            RequestCode = 5522;
            String url = Paths._like_wallpaper + "/" + pageStart + "/" + pageSize;
//            String url = Paths.BaseUrl + "like/wallpaper?page_num_start=" + 0 + "&page_size=" + 10;
            WebServices.getInstance().MyGetCallWithoutAuth(getActivity(), url, this);
        } else {
            RequestCode = 5533;
//            String url = Paths.BaseUrl + "like/wallpaper?page_num_start=" + 0 + "&page_size=" + 10 + "&user_id=" + Settings.userObj.getId();
            String url = Paths._like_wallpaper + "/" + pageStart + "/" + pageSize;
            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, this);
        }
    }

    @Override
    public void requestFailed(VolleyError error) {
        pDialog.cancel();
//        VolleyError myError = error;
//        if (error != null) {
//            if (error.getMessage() != null && !error.getMessage().equals(""))
//                Settings.ShowErrorDialog(getActivity(), 0, null, error.getMessage());
//            else
//                Settings.ShowErrorDialog(getActivity(), 0, null, "Internet Or Server Error.");
//        } else {
//            Settings.ShowErrorDialog(getActivity(), 0, null, "Internet Or Server Error.");
//        }
    }

    @Override
    public void requestSucceeded(JSONObject response) {
        pDialog.cancel();
        try {
            if (response.getString("status").equals("Successful")) {
                Gson gson = new GsonBuilder().create();
                if (RequestCode == 5522 || RequestCode == 5533) {
                    ImageData[] allImages = gson.fromJson(response.getJSONArray("wallpaper").toString(), ImageData[].class);
                    listOfHeaderImages = new ArrayList();
                    listOfHeaderImages = Arrays.asList(allImages);
                    if (listOfHeaderImages.size() > 0 & recycPopularImages != null) {
                        topAdapter = new PopularHorizontalAdapter(listOfHeaderImages, getActivity());
                        recycPopularImages.setAdapter(topAdapter);
                    }

                    //gridView.addHeaderView(headerView);
                    getPopularImages();
                } else if (RequestCode == 1133 || RequestCode == 1122) {
                    canScroll = true;
                    ImageData[] allImages = gson.fromJson(response.getJSONArray("wallpaper").toString(), ImageData[].class);
                    listOfTopImages = new ArrayList<>();
                    for (int a = 0; a < allImages.length; a++) {
                        listOfTopImages.add(allImages[a]);
                    }


                    if (IsFirstCall == true && listOfTopImages.size() > 0) {
                        // txtNoDataFound.setVisibility(View.GONE);
                        IsFirstCall = false;
                        if (listOfTopImages.size() < pageSize) {
                            IsMoreScroll = false;
                        } else {

                        }

                        mainAdapter = new AllPopularImagesAdapter(getActivity(), listOfTopImages);
                        gridView.setAdapter(mainAdapter);
                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(getActivity(), ViewImageScreen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                intent.putExtra("SelectedImage", mainAdapter.getItem(position));
                                intent.putExtra("like", "1");
                                Popular.liked = "1";
                                startActivity(intent);

                            }
                        });
                        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(AbsListView view, int scrollState) {
                            }

                            @Override
                            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                if (canScroll) {
                                    final int lastItem = firstVisibleItem + visibleItemCount;
                                    if (lastItem == totalItemCount && IsMoreScroll) {
                                        if (lastLastitem != lastItem) {
                                            lastLastitem = lastItem;
                                            pageStart = pageStart + pageSize;
                                            canScroll = false;
//                                           pDialog.show();

                                            if (RequestCode == 1122) {
                                                String url = Paths._like_wallpaper + "/" + pageStart + "/" + pageSize;
//                                               String url = Paths.BaseUrl + "like/wallpaper?page_num_start=" + pageStart + "&page_size=" + pageSize;
                                                WebServices.getInstance().MyGetCallWithoutAuth(getActivity(), url, Popular.this);
                                            } else if (RequestCode == 1133) {
                                                String url = Paths.BaseUrl + "like/wallpaper?page_num_start=" + pageStart + "&page_size=" + pageSize + "&user_id=" + Settings.userObj.getId();
                                                WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, Popular.this);
                                            }

                                        }
                                    }
                                }

                            }
                        });
                    } else if (IsFirstCall && listOfTopImages.size() == 0) {
                        gridView.setAdapter(null);
                        // txtNoDataFound.setVisibility(View.VISIBLE);
                    } else {
                        if (listOfTopImages.size() < pageSize) {
                            IsMoreScroll = false;
                            mainAdapter.AddMyData(listOfTopImages);
                        } else {
                            mainAdapter.AddMyData(listOfTopImages);
                        }

                    }
                }
            } else {
                Gson gsonTemp = new GsonBuilder().create();
                Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
//                Settings.ShowErrorDialog(getActivity(), 1, error, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // pDialog.cancel();
        }

    }

    void getPopularImages() {

        IsFirstCall = true;
        // pDialog.show();
        pageStart = 10;
        lastLastitem = 0;
        canScroll = true;
        IsMoreScroll = true;
        lastLastitem = 0;
        if (Settings.userObj == null) {
            RequestCode = 1122;
            String url = Paths._like_wallpaper + "/" + pageStart + "/" + pageSize;
//            String url = Paths.BaseUrl + "like/wallpaper?page_num_start=" + pageStart + "&page_size=" + pageSize;
            WebServices.getInstance().MyGetCallWithoutAuth(getActivity(), url, this);
        } else {
            RequestCode = 1133;
            String url = Paths._like_wallpaper + "/" + pageStart + "/" + pageSize;

//            String url = Paths.BaseUrl + "like/wallpaper?page_num_start=" + pageStart + "&page_size=" + pageSize + "&user_id=" + Settings.userObj.getId();
            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, this);
        }
    }
}