package com.mobilesdev360.sigmax.piczall.utils.progresshandler;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;

public class ProgressbarHandler {
    static ProgressDialog progressDialog;
    public static void ShowLoadingProgress(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        progressDialog.show();
        AppCompatActivity activity= (AppCompatActivity) context;
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    public static void DismissProgress(Context context)
    {
        if(progressDialog==null)
        {
            progressDialog=new ProgressDialog(context);
        }
        if(progressDialog.isShowing())
        progressDialog.dismiss();
        AppCompatActivity activity= (AppCompatActivity) context;
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
