package com.mobilesdev360.sigmax.piczall.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class RememberMe {


    Context context;
    SharedPreferences sharedpreferences;

    public RememberMe(Context context) {
        this.context = context;
        sharedpreferences = context.getSharedPreferences("AppInfo", Context.MODE_PRIVATE);
    }

    //User  //its means is daily service run  "0","1"------> -55 is service notification id
    // Device_Notification
    public void AddInfo(String Key, String Value) {

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Key, Value);
        editor.commit();


    }

    public String getInfo(String Key, String Def) {
        String subscriberType = sharedpreferences.getString(Key, Def);

        return subscriberType;

    }

}
