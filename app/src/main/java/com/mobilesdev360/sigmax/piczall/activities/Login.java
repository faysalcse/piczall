package com.mobilesdev360.sigmax.piczall.activities;

import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.SocialLoginData;
import com.mobilesdev360.sigmax.piczall.dataClass.User;
import com.mobilesdev360.sigmax.piczall.dialogs.MessageDialog;
import com.mobilesdev360.sigmax.piczall.utils.ApplicationConstraints;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.RememberMe;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;
import com.mobilesdev360.sigmax.piczall.utils.preferenceutil.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity implements WebServices.RequestResults, GoogleApiClient.OnConnectionFailedListener {
    public static final String TAG = "Login";
    EditText txtEmail, txtPassword;
    TextView txtForgetPassword;
    ProgressDialog pDialog;
    Button btnLogin;
    TextView txtSignup;
    GoogleApiClient mGoogleApiClient;
    int RC_SIGN_IN = 111;
    int requestCode = 951;
    GoogleSignInOptions gso;
    LoginResult fbloginData = null;
    SocialLoginData socialLoginData;
    SocialLoginData socialDataForFacebook;
    ImageView txtFacebook, txtGoogle;
    CallbackManager callbackManager;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Initialize();
    }

    void Initialize() {
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        txtSignup = findViewById(R.id.txtSignup);
        txtForgetPassword = findViewById(R.id.txtForgetPassword);
        txtFacebook = findViewById(R.id.txtFacebook);
        txtGoogle = findViewById(R.id.txtGoogle);
        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

//        txtEmail.setText("ali@pxp.com");
//        txtPassword.setText("qqqqqq12");

        txtSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Register
                Intent intent = new Intent(Login.this, Register.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginBtnClick();
//                txtEmail.requestFocus();
//                hideKeyBoard();
//                pDialog.show();
//                if(dataValidation()==true){
//                    LoginBtnClick();
//                }else{
//                    pDialog.cancel();
//                }
            }
        });
        txtForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, ForgetPassword.class);
                startActivityForResult(intent, 7512);
            }
        });


        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        //loginResult.getAccessToken().getUserId()
                        fbloginData = loginResult;
                        GraphRequest request = GraphRequest.newMeRequest(fbloginData.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Gson gson = new GsonBuilder().create();
                                socialDataForFacebook = gson.fromJson(object.toString(), SocialLoginData.class);

                                Map<String, Object> params = new HashMap<String, Object>();
                                params.put("social_id", loginResult.getAccessToken().getUserId());
                                params.put("social_token", loginResult.getAccessToken().getToken());
                                if (socialDataForFacebook.getEmail() != null)
                                    params.put("email", socialDataForFacebook.getEmail());

                                params.put("is_facebook", "1");
                                params.put("is_google", "0");
                                params.put("OS", "1");
                                requestCode = 999;
//
                                String url = Paths.BaseUrl + "sociallogin";
                                WebServices.getInstance().MyPostRequestWithoutAuth(Login.this, url, params, Login.this);
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name,last_name,email,name,id");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        MessageDialog.ShowMessageDialog(Login.this, "Alert", "Cancel Facebook Login!");

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        MessageDialog.ShowMessageDialog(Login.this, "Error", "Cancel Facebook Login!");

                    }
                });
        txtGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginBtnClick();
//                gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//
////                56184705783-5ln67agi3g5bm1dcj9i7t8k0n5qiv25a.apps.googleusercontent.com
//                        //Old "568494558774-c5v1kgnumnpr7vrcn3rmf5oncr974jea.apps.googleusercontent.com"
//
//
//                        .requestEmail().requestId().requestProfile().requestIdToken(" 56184705783-5ln67agi3g5bm1dcj9i7t8k0n5qiv25a.apps.googleusercontent.com")
//                        .build();
//                mGoogleApiClient = new GoogleApiClient.Builder(Login.this)
//                        .enableAutoManage(Login.this, Login.this)
//                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                        .build();
//                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//                startActivityForResult(signInIntent, RC_SIGN_IN);

            }
        });
        txtFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(Login.this, Arrays.asList("public_profile", "user_friends", "email"));

            }
        });
    }

    public void hideKeyBoard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        }
    }


    private boolean dataValidation() {
        Boolean validData = true;
        if (TextUtils.isEmpty(txtEmail.getText())) {
            txtEmail.setError("Email or User Name can not be blank");
            validData = false;
        }
        if (txtEmail.getText().toString().contains("@")) {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText()).matches()) {
                txtEmail.setError("Invalid email");
                validData = false;
            }

        } else {

            if (txtEmail.getText().length() < 3) {
                txtEmail.setError("Invalid User Name");
                validData = false;
            }
        }
        String pass = txtPassword.getText().toString();
        if (TextUtils.isEmpty(txtPassword.getText())) {
            txtPassword.setError("Password can not be blank");
            validData = false;
        } else if (!(pass.matches(".*[A-Za-z].*") && pass.matches(".*[0-9].*"))) {
            txtPassword.setError("Password must have Alphanumeric characters");
            validData = false;
        } else if (txtPassword.getText().length() < 8) {
            txtPassword.setError("Password should contain atleast 8 characters");
            validData = false;
        }

        return validData;
    }

    void LoginBtnClick() {
        Intent googlePicker = AccountPicker.newChooseAccountIntent(null, null, new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, true, null, null, null, null);
        startActivityForResult(googlePicker, RC_SIGN_IN);
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("email", txtEmail.getText().toString());
//        params.put("password", "google_password");
//        String url= Paths.BaseUrl+"login";
//        WebServices.getInstance().MyPostRequestWithoutAuth(this,url,params,this);
    }

    @Override
    public void requestFailed(VolleyError error) {
        pDialog.cancel();
        VolleyError myError = error;
        if (error != null) {
            if (error.getMessage() != null && !error.getMessage().equals(""))
                Settings.ShowErrorDialog(this, 0, null, error.getMessage());
            else
                Settings.ShowErrorDialog(this, 0, null, "Internet Or Server Error.");
        } else {
            Settings.ShowErrorDialog(this, 0, null, "Internet Or Server Error.");
        }
    }

    @Override
    public void requestSucceeded(JSONObject response) {

        try {
            if (response.getString("status").equals("Successful") || (requestCode == 951 || requestCode == 753 || requestCode == 999)) {
                Gson gson = new GsonBuilder().create();
                User userObj = gson.fromJson(response.getJSONObject("user").toString(), User.class);
                Settings.userObj = userObj;

                //User
                String json = gson.toJson(userObj);
                RememberMe rememberMe = new RememberMe(this);
                rememberMe.AddInfo(ApplicationConstraints.CURRENT_USER, json);
                Intent intent = new Intent(this, Main.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                pDialog.cancel();

            } else {

                if (response.getString("status_code").equals("2045")) {
                    pDialog.cancel();
                    Intent intent = new Intent(Login.this, Register.class);
                    intent.putExtra("facebookData", socialDataForFacebook);
                    startActivity(intent);
                    finish();

                } else if (response.getString("status_code").equals("2056")) {
                    pDialog.cancel();
                    Intent intent = new Intent(Login.this, Register.class);
                    intent.putExtra("googleData", socialLoginData);
                    startActivity(intent);
                    finish();

                } else {

                    pDialog.cancel();
                    Gson gsonTemp = new GsonBuilder().create();
                    Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
                    Settings.ShowErrorDialog(this, 1, error, "");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            pDialog.cancel();
        }

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK) {
            SharedPref.init(this);

            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
//            Toast.makeText(getApplicationContext(), accountName, Toast.LENGTH_SHORT).show();
            txtEmail.setText(accountName);
            Map<String, String> params = new HashMap<String, String>();
            params.put("name", "salman");
            params.put("email", accountName);
            params.put("password", "googel_pass");

            if (txtEmail.getText().toString().isEmpty()) {
                Toast.makeText(this, "Please Enter User Name", Toast.LENGTH_SHORT).show();
            } else {

                call_api(Paths._user_login, Request.Method.POST, "", params);
            }

//            WebServices.getInstance().MyPostRequestWithoutAuth(Login.this,Paths._user_login,params,Login.this);
        }
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            if (result.isSuccess()) {
//                GoogleSignInAccount acct = result.getSignInAccount();
//                String ID = acct.getId();
//                String Token = acct.getIdToken();
//                socialLoginData=new SocialLoginData();
//                socialLoginData.setFirstName(acct.getDisplayName());
//                socialLoginData.setEmail(acct.getEmail());
//                socialLoginData.setId(ID);
//                socialLoginData.setGoogleImageUrl(acct.getPhotoUrl().toString());
//                socialLoginData.setName(acct.getDisplayName());
//                Map<String, Object> params = new HashMap<String, Object>();
////                params.put("social_id", ID);
////                params.put("social_token", Token);
////                params.put("role", "client");
////                params.put("is_google", "1");
//                if(acct.getEmail()!=null)
//                    params.put("email",acct.getEmail());
//                params.put("password","googel_pass");
//                Log.e("email", acct.getEmail());
//
////                params.put("is_facebook", "0");
////                params.put("OS", "1");
//
//                String url = Paths._user_login;
//                requestCode=753;
//                // pDialog.show();
//                WebServices.getInstance().MyPostRequestWithoutAuth(Login.this,url,params,Login.this);
//
//            }
//            mGoogleApiClient.stopAutoManage(this);
//            mGoogleApiClient.disconnect();
//        }else   if(7512==requestCode && resultCode==RESULT_OK){
//
//            MessageDialog.ShowMessageDialog(this,"Alert","New password sent on your email");
//        }else {
//            callbackManager.onActivityResult(requestCode, resultCode, data);
//        }
    }

    public void call_api(final String urL, final int Method, final String view, final Map<String, String> params) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Method, urL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if (status.equals("Successful")) {
                        JSONArray jsonArray = new JSONArray();
                        jsonArray = jsonObject.getJSONArray("user");
                        String user_id = jsonArray.getJSONObject(0).getString("id");
                        SharedPref.write("user_id", user_id);

                        if (!txtEmail.getText().toString().isEmpty()) {
                            SharedPref.write("USER_NNAME", txtEmail.getText().toString());
                        }
                        Splash.userid =SharedPref.read("user_id");

                        JSONObject myobject = jsonArray.getJSONObject(0);

                        RememberMe rememberMe = new RememberMe(Login.this);
                        rememberMe.AddInfo("User", myobject.toString());

                        Log.i(TAG, myobject.toString());

                        Intent intent = new Intent(Login.this, Main.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    } else {
                        Toast.makeText(Login.this,
                                "user not valid", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("error:", "Error-------" + volleyError.toString());

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
//            Map<String, String> params = new HashMap<String, String>();
                return params;

            }


        };

        requestQueue.add(postRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
