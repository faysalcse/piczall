package com.mobilesdev360.sigmax.piczall.activities;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.PlacementId;
import com.squareup.picasso.Picasso;

public class ViewImageFullScreen extends Activity {
    ImageView imgMain,imgcross;
    ImageData imageData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image_full_screen);

        imageData=(ImageData)getIntent().getSerializableExtra("ImageSeletedForMain");
        imgMain=(ImageView) findViewById(R.id.imgMain);
        imgcross=(ImageView) findViewById(R.id.imgcross);
        if(imageData!=null){
            Picasso.get()
                    .load(Paths._wallpapers_images + imageData.getWallpaper()).placeholder(R.drawable.itembg).error(R.drawable.itembg)
                    .fit()
                    .into(imgMain);
        }
        imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initialInMobiBannerAd();
    }

    private void initialInMobiBannerAd() {
        AdView adView = findViewById(R.id.adView);
        adView.loadAd(new AdRequest.Builder().build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
