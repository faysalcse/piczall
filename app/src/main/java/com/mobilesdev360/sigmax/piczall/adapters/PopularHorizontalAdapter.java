package com.mobilesdev360.sigmax.piczall.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.mobilesdev360.sigmax.piczall.activities.VideoViews;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.PlacementId;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class PopularHorizontalAdapter  extends RecyclerView.Adapter<PopularHorizontalAdapter.MyViewHolder> {

    private List<ImageData> list;
    Activity activity;
    private ImageLoader imageLoader;
    Intent intentIfRewardCompleted;
    private InterstitialAd mInterstitialAd;

    public PopularHorizontalAdapter(List<ImageData> Data, Activity activity) {

        list = Data;
        this.activity=activity;
        imageLoader=ImageLoader.getInstance();
        loadFacebookInterstitialAd();

    }

    private void loadFacebookInterstitialAd(){
        mInterstitialAd = new InterstitialAd(activity);
        mInterstitialAd.setAdUnitId(activity.getString(R.string.interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });


    }


    private void populateIntentToShowAfterReward(int position){
        Intent intent=new Intent(activity, VideoViews.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        ImageData imageData=list.get(position);
        Log.w("ADS_TESTING", "FEATURE AD | "+imageData.getIs_premium());
        if(imageData.getIs_premium().equals("1")) {
        }
        Gson gson=new Gson();
        intent.putExtra("SelectedImage",gson.toJson(imageData));
        intent.putExtra("image", "");
        intentIfRewardCompleted = intent;

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.popular_list_item, parent, false);
        return new MyViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if(list.get(position).isVideoType()) {
            holder.imgStatus.setVisibility(View.VISIBLE);
            if(list.get(position).getIs_premium()!=null&&list.get(position).getIs_premium().equals("1")){
                holder.imgStatus.setImageResource(R.drawable.loading);
            }else {
                holder.imgStatus.setImageResource(R.mipmap.video);
            }
            holder.progressBar.setVisibility(View.VISIBLE);
            Transformation transformation = new RoundedTransformationBuilder()
                    .borderColor(Color.TRANSPARENT)
                    .borderWidthDp(0)
                    .cornerRadiusDp(6)
                    .oval(false)
                    .build();
            Picasso.get()
                    .load(Paths._thumbnail_path_video + list.get(position).getVideo_wallpaper_thumbnail())
                    .transform(transformation)
                    .error(R.drawable.itembg).placeholder(R.drawable.itembg).into(holder.imgMain, new Callback() {
                @Override
                public void onSuccess() {
                    holder.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {

                }
            });
            holder.imgMain.setOnClickListener(v -> {
                populateIntentToShowAfterReward(position);
                if(list.get(position).getIs_premium().equals("0")){
                    activity.startActivity(intentIfRewardCompleted);
                }
                else{
                    if(mInterstitialAd.isLoaded()){
                        mInterstitialAd.show();
                    }
                    activity.startActivity(intentIfRewardCompleted);
                }
            });
        }
        else {
            holder.imgStatus.setVisibility(View.GONE);
            Picasso.get()
                    .load(Paths.thumbnailImage + list.get(position).getWallpaper())
                    .resize(Settings.width,Settings.height)
                    .error(R.drawable.loading).placeholder(R.drawable.loading).into(holder.imgMain, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {

                }
            });

            holder.imgMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(activity,ViewImageScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.putExtra("SelectedImage",list.get(position));
                    activity.startActivity(intent);
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar progressBar;
        RoundedImageView imgMain;
        ImageView imgStatus;



        public MyViewHolder(View v) {
            super(v);
            imgMain = (RoundedImageView) v.findViewById(R.id.imgMain);
            imgStatus= (ImageView) v.findViewById(R.id.imgStatus);
            progressBar= v.findViewById(R.id.progress_bar);

        }
    }
    public void SetUpImgae(ImageView imgMain,String imagepathe,ProgressBar progressBar)
    {

        imageLoader.displayImage(imagepathe, imgMain, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressBar.setVisibility(View.VISIBLE);

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }

}