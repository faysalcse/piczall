package com.mobilesdev360.sigmax.piczall.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobilesdev360.sigmax.piczall.R;

public class MessageDialog {


    public static void ShowMessageDialog(final Activity activity, String Title, String Msg){
        ImageView imgCross;
        View view;
        final AlertDialog dialog;
        TextView txtTitle,txtMsg;
        Button btnOk;

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        LayoutInflater inflater = activity.getLayoutInflater();
        view=inflater.inflate(R.layout.only_message_dialog, null);
        builder.setView(view);
        builder.setCancelable(true);
        dialog =builder.create();
        imgCross=(ImageView) view.findViewById(R.id.imgCross);
        txtTitle=(TextView) view.findViewById(R.id.txtTitle);
        txtMsg=(TextView) view.findViewById(R.id.txtMsg);
        btnOk=(Button) view.findViewById(R.id.btnOk);

        txtTitle.setText(Title);
        txtMsg.setText(Msg);

        imgCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();

    }


}

