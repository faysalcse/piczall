package com.mobilesdev360.sigmax.piczall.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.activities.Trending;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.activities.ViewTrendingImageScreen;
import com.mobilesdev360.sigmax.piczall.fragment.Feature;
import com.mobilesdev360.sigmax.piczall.fragment.Popular;

import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.activities.Splash;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.adapters.AllPopularImagesAdapter;
import com.mobilesdev360.sigmax.piczall.adapters.PopularHorizontalAdapter;
import com.mobilesdev360.sigmax.piczall.adapters.TrendingViewAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.remote.RetrofitProvider;
import com.mobilesdev360.sigmax.piczall.remote.SOService;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;
import com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel;


import java.util.ArrayList;


/**
 * Created by User on 2/12/2018.
 */

public class TrendingViewAdapter extends RecyclerView.Adapter<TrendingViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";
    AllPopularImagesAdapter mainAdapter;
    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mCatIds = new ArrayList<>();
    private Context mContext;

    public TrendingViewAdapter( Context context,ArrayList<String> names, ArrayList<String> imageUrls,ArrayList<String> catIDs) {
        mNames = names;
        mImageUrls = imageUrls;
        mCatIds=catIDs;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");
        holder.progressBar.setVisibility(View.VISIBLE);
        Glide.with(mContext)
                .asBitmap()
                .load(mImageUrls.get(position))
                .into(holder.image);

        holder.name.setText(mNames.get(position));
        holder.progressBar.setVisibility(View.GONE);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on an image: " + mNames.get(position));
              // Toast.makeText(mContext, mCatIds.get(position), Toast.LENGTH_SHORT).show();
              /*  Intent intent=new Intent(mContext, ViewTrendingImageScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("SelectedImage",mCatIds.get(position));
                mContext.startActivity(intent);
            */
                Intent intent=new Intent(mContext, Trending.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("SelectedImage",mCatIds.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView name;
        ProgressBar progressBar;
        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            name = itemView.findViewById(R.id.name);
            progressBar= itemView.findViewById(R.id.progress_bar);
        }
    }
}
