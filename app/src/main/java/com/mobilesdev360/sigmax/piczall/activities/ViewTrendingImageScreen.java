package com.mobilesdev360.sigmax.piczall.activities;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.adapters.ImageViewAdapter;
import com.mobilesdev360.sigmax.piczall.adapters.TrendingImageViewAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.dataClass.TrendingImageData;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.PlacementId;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import io.alterac.blurkit.BlurLayout;


public class ViewTrendingImageScreen extends AppCompatActivity implements WebServices.RequestResults   {
    public static final String TAG = "ViewTrendingImageScreen";
    public int pageStart = 0, pageSize = 150;
    protected Bundle bundle;
    ViewPager pagerView;
    LinearLayout liback;
    BlurLayout blurLayout;
    List<TrendingImageData> listOfTopImages;
    boolean canScroll, IsMoreScroll, IsFirstCall = true;
    int lastLastitem = 0;
    ProgressDialog pDialog;
    TrendingImageViewAdapter adapter;
    int requestCode = 1122;
    int selectedCategoryID = -1;
    TrendingImageData imageData;
    String like = "0";

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image_screen);
        bundle = getIntent().getExtras();
        blurLayout = findViewById(R.id.blurLayout);
        imageData = (TrendingImageData) getIntent().getSerializableExtra("SelectedImage");
        Log.d(TAG, "Image Data Value: " + String.valueOf(imageData));
        if (imageData != null) {
            selectedCategoryID = imageData.getCategoryId();
            Log.d(TAG, "Image Data Value ID: " + String.valueOf(selectedCategoryID));
            Log.d(TAG, "Image Data liked: " + String.valueOf(imageData.getIsLike()));
            if(imageData.getIsLike() != null) {
                if (imageData.getIsLike() == 1) {
                    like = "Yes";
                }
            }
            Initialize();
            like= imageData.getYoulike();
            if(like!=null)
            {
                if(like.equals("Yes"))
                {
                    like="Yes";
                }
                else
                {
                    like="No";
                }
            }
            Log.i(TAG, "imageData != null");

        } else {
            Log.i(TAG, "imageData == null");
        }


        if (bundle != null) {


            if (bundle.containsKey("like")) {
                like = "Yes";
            } else {

            }

        } else {
        }


        initialInMobiBannerAd();
        loadAd();
    }

    private void initialInMobiBannerAd() {
        AdView adView = findViewById(R.id.adView);
        adView.loadAd(new AdRequest.Builder().build());
    }

    private void loadAd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        blurLayout.startBlur();
    }

    @Override
    protected void onStop() {
        blurLayout.pauseBlur();
        super.onStop();
    }

    void Initialize() {
        blurLayout.setBlurRadius(10);
        blurLayout.setFPS(300);
        listOfTopImages = new ArrayList();
        pagerView = findViewById(R.id.pagerView);
        pagerView.setOffscreenPageLimit(100);
        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

        if (Splash.userid.equals("")) {
            Log.i(TAG, "Initialize without auth ");
            listOfTopImages.add(imageData);
            adapter = new TrendingImageViewAdapter(this, listOfTopImages, this, like);
            pagerView.setAdapter(adapter);
            getImagesWithCategoryID(selectedCategoryID, pageStart, pageSize, true);

        } else {

            Log.i(TAG, "Initialize with auth ");
            Log.d(TAG, "data: "+ like);
            listOfTopImages.add(imageData);
            adapter = new TrendingImageViewAdapter(this, listOfTopImages, this, like);
            pagerView.setAdapter(adapter);
            getImagesWithCategoryID(selectedCategoryID, pageStart, pageSize, true);
        }
        // add first image




        pagerView.setPageTransformer(true, new ViewPager.PageTransformer() {
            private static final float MIN_SCALE = 0.65f;
            private static final float MIN_ALPHA = 0.3f;

            @Override
            public void transformPage(View page, float position) {
                if (position < -1) {  // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    page.setAlpha(0);

                } else if (position <= 1) { // [-1,1]

                    page.setScaleX(Math.max(MIN_SCALE, 1 - Math.abs(position)));
                    page.setScaleY(Math.max(MIN_SCALE, 1 - Math.abs(position)));
                    page.setAlpha(Math.max(MIN_ALPHA, 1 - Math.abs(position)));

                } else {  // (1,+Infinity]
                    // This page is way off-screen to the right.
                    page.setAlpha(0);

                }

            }
        });
        pagerView.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                i = i + 2;
                int ss = i % pageSize;
                if (ss == 0 && IsMoreScroll) {
                    IsFirstCall = false;
                    pageStart = pageStart + pageSize;
                    getImagesWithCategoryID(selectedCategoryID, pageStart, pageSize, false);
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    public void getImagesWithCategoryID(int CategoryID, int pageStarts, int pageSizes, boolean IsFirstCall) {


        if (IsFirstCall) {
            this.IsFirstCall = true;
            lastLastitem = 0;
            canScroll = true;
            IsMoreScroll = true;
            lastLastitem = 0;
            selectedCategoryID = CategoryID;
        } else {
            this.IsFirstCall = false;
            canScroll = false;
        }
        if (Splash.userid .equals("")) {
            requestCode = 1133;
            String url = Paths._get_trending_pics + selectedCategoryID + "/" + pageStarts + "/" + pageSizes;
            WebServices.getInstance().MyGetCallWithoutAuth(this, url, this);


            Log.i(TAG, "without auth ");

        } else {

            requestCode = 1133;
            String url = Paths._get_trending_pics + selectedCategoryID + "/" + pageStarts + "/" + pageSizes;
            WebServices.getInstance().MyGetCallWithoutAuth(this, url, this);

            Log.i(TAG, "with auth ");

//            requestCode = 1144;
//            String url = Paths.BaseUrl + "wallpaper/index?page_num_start=" + pageStarts + "&page_size=" + pageSizes + "&user_id=" + Settings.userObj.getId() + "&category_id=" + CategoryID + "&current_wallpaper_id=" + imageData.getId();
//            WebServices.getInstance().MyGetCallWithAuth(this, url, this);
//            Toast.makeText(this, "with auth", Toast.LENGTH_SHORT).show();
//            Log.i(TAG, "without auth ");
        }
    }

    @Override
    public void requestFailed(VolleyError error) {
        pDialog.cancel();
        VolleyError myError = error;
        if (error != null) {
            if (error.getMessage() != null && !error.getMessage().equals(""))
                Settings.ShowErrorDialog(this, 0, null, error.getMessage());
            else
                Settings.ShowErrorDialog(this, 0, null, "Internet Or Server Error.");
        } else {
            Settings.ShowErrorDialog(this, 0, null, "Internet Or Server Error.");
        }
    }

    @Override
    public void requestSucceeded(JSONObject response) {
        pDialog.cancel();
        canScroll = true;
        try {
            if (response.getString("status").equals("Successful")) {

                Gson gson = new GsonBuilder().create();
                final TrendingImageData[] allImages = gson.fromJson(response.getJSONArray("trending_pics").toString(), TrendingImageData[].class);
                listOfTopImages = new ArrayList<>();


                for (int a = 0; a < allImages.length; a++) {

                    listOfTopImages.add(allImages[a]);

                }
                for(int i = 5; i<listOfTopImages.size(); i+=6){
                    TrendingImageData adData = new TrendingImageData();
                    adData.isAd = true;
                    listOfTopImages.add(i, adData);
                }
                adapter.AddMyData(listOfTopImages);
            } else {
                //   pDialog.cancel();
                Gson gsonTemp = new GsonBuilder().create();
                Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // pDialog.cancel();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (requestCode == 200) {
                if (adapter != null && adapter.isWriteStorageAllowed()) {

                } else if (adapter != null) {
                    adapter.requestwriteStoragePermission();

                }
            } else if (requestCode == 100) {

                if (adapter != null && adapter.isReadStorageAllowed()) {

                } else if (adapter != null) {
                    adapter.requestStoragePermission();
                }

            }

        }
    }
    public List<ImageData> removeDuplicates(List<ImageData> list) {
        List<ImageData> listImages = new ArrayList<>();
        // Set set1 = new LinkedHashSet(list);

        Set set = new TreeSet((o1, o2) -> {
            if (((ImageData) o1).getWallpaperName().equals(((ImageData) o2).getWallpaperName())) {
                String obj1=((ImageData)o1).getYoulike();
                String obj2=((ImageData)o2).getYoulike();
                if(obj1!=null&&obj2!=null) {
                    if (obj1.equals("No") && obj2.equals("No")) {
                        return 0;
                    } else if (obj1.equals("Yes") && obj2.equals("Yes"))
                    {
                        return 0;
                    }
                    else if (obj1.equals("No") && obj2.equals("Yes"))
                    {
                        return 0;
                    }
                    else if (obj1.equals("Yes") && obj2.equals("No"))
                    {
                        return 0;
                    }
                }

            }
            return 1;
        });
        set.addAll(list);

        listImages.addAll(set);
        return listImages;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void showAds() {
        if(mInterstitialAd.isLoaded()) {
            Log.e("tag"," = = = loaded == ");
            mInterstitialAd.show();
        }
    }
}
