package com.mobilesdev360.sigmax.piczall.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.adapters.AllPopularImagesAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;

public class SearchWallpapers extends AppCompatActivity implements  WebServices.RequestResults{
    View rootView;
    List<ImageData> listOfTopImages;
    GridViewWithHeaderAndFooter gridView;
    AllPopularImagesAdapter mainAdapter;
    View loadMoreView;
    int pageStart=0,pageSize=18;
    boolean canScroll,IsMoreScroll,IsFirstCall=true;
    int lastLastitem=0;
    int RequestCode=1122;
    ProgressDialog pDialog;
    TextView txtNoDataFound;
    EditText etxtSearch;
    String SeatchFor="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_wallpapers);
        ImageView imgMenu=(ImageView)findViewById(R.id.imgMenu);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Initialize();
    }


    void Initialize(){
        listOfTopImages = new ArrayList();
        gridView = (GridViewWithHeaderAndFooter) findViewById(R.id.gridview);
        txtNoDataFound= (TextView) findViewById(R.id.txtNoDataFound);
        etxtSearch= (EditText) findViewById(R.id.etxtSearch);
        pDialog = new ProgressDialog(SearchWallpapers.this);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");


        etxtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    SeatchFor=etxtSearch.getText()+"";
                    etxtSearch.requestFocus();
                    closeKeyboard();
                    pageStart=0;
                    IsFirstCall=true;
                    // pDialog.show();
                    pageStart=0;
                    lastLastitem=0;
                    canScroll=true;
                    IsMoreScroll=true;
                    lastLastitem=0;
                    if(Settings.userObj==null) {
                        RequestCode=1122;
                        String url = Paths.searchwallpaper +SeatchFor.trim()+"/"+pageStart+"/"+pageSize;
//                        String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize+"&search="+SeatchFor;
                        WebServices.getInstance().MyGetCallWithoutAuth(SearchWallpapers.this, url, SearchWallpapers.this);
                    }else{
                        RequestCode=1133;
//                        String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize+"&search="+SeatchFor;
                        String url = Paths.searchwallpaper +SeatchFor+"/"+pageStart+"/"+pageSize;
                        WebServices.getInstance().MyGetCallWithAuth(SearchWallpapers.this, url, SearchWallpapers.this);
                    }
                    return true;
                }
                return false;
            } });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                //ViewImageScreen
                Intent intent=new Intent(SearchWallpapers.this,ViewImageScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });

    }

    @Override
    public void requestFailed(VolleyError error) {
        pDialog.cancel();
        VolleyError myError=error;
        if(error!=null){
            if(error.getMessage()!=null && !error.getMessage().equals(""))
                Settings.ShowErrorDialog(this,0,null,error.getMessage());
            else
                Settings.ShowErrorDialog(this,0,null,"Internet Or Server Error.");
        }else {
            Settings.ShowErrorDialog(this,0,null,"Internet Or Server Error.");
        }
    }

    @Override
    public void requestSucceeded(JSONObject response) {
        pDialog.cancel();
        canScroll=true;
        try {
            if (response.getString("status").equals("Successful")) {
                Gson gson = new GsonBuilder().create();
                final ImageData[] allImages = gson.fromJson(response.getJSONArray("wallpaper_by_categoryname").toString(), ImageData[].class);
                listOfTopImages=new ArrayList<>();
                for(int a=0;a<allImages.length;a++){

                    listOfTopImages.add(allImages[a]);
                }


                if(IsFirstCall==true && listOfTopImages.size()>0) {
                    txtNoDataFound.setVisibility(View.GONE);
                    IsFirstCall=false;
                    if(listOfTopImages.size()<pageSize){
                        IsMoreScroll=false;
                    }else{

                    }

                    mainAdapter= new AllPopularImagesAdapter(this,listOfTopImages);
                    gridView.setAdapter(mainAdapter);
                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent=new Intent(SearchWallpapers.this,ViewImageScreen.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            intent.putExtra("SelectedImage",mainAdapter.getItem(position));
                            startActivity(intent);

                        }
                    });
                    gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {
                        }
                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            if(canScroll){
                                final int lastItem = firstVisibleItem + visibleItemCount;
                                if (lastItem == totalItemCount && IsMoreScroll) {
                                    if(lastLastitem != lastItem) {
                                        lastLastitem=lastItem;
                                        pageStart=pageStart+pageSize;
                                        canScroll=false;
                                        pDialog.show();
                                        if(RequestCode==1122){
//                                            String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize+"&search="+SeatchFor;
                                            String url = Paths.searchwallpaper +SeatchFor+"/"+pageStart+"/"+pageSize;
                                            WebServices.getInstance().MyGetCallWithoutAuth(SearchWallpapers.this, url, SearchWallpapers.this);
                                        }else if(RequestCode==1133){
                                            String url = Paths.searchwallpaper +SeatchFor+"/"+pageStart+"/"+pageSize;
//                                            String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize+"&user_id="+Settings.userObj.getId()+"&search="+SeatchFor;
                                            WebServices.getInstance().MyGetCallWithAuth(SearchWallpapers.this, url, SearchWallpapers.this);
                                        }
                                    }
                                }
                            }

                        }});
                }else if(IsFirstCall && listOfTopImages.size()==0){
                    gridView.setAdapter(null);
                    txtNoDataFound.setVisibility(View.VISIBLE);
                }else {
                    if(listOfTopImages.size()<pageSize){
                        IsMoreScroll=false;
                        mainAdapter.AddMyData(listOfTopImages);
                    }else{
                        mainAdapter.AddMyData(listOfTopImages);
                    }

                }

            } else {
                Gson gsonTemp = new GsonBuilder().create();
                Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
                Settings.ShowErrorDialog(this, 1, error, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // pDialog.cancel();
        }

    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}

