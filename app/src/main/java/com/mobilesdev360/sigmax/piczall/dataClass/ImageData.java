package com.mobilesdev360.sigmax.piczall.dataClass;

import android.graphics.Bitmap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.annotations.Ignore;

public class ImageData implements Serializable {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("wallpaper")
    @Expose
    private String wallpaper;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("wallpaper_name")
    @Expose
    private String wallpaperName;
    @SerializedName("is_like")
    @Expose
    private Integer isLike;
    @SerializedName("wallpaper_like_count")
    @Expose
    private Integer likeCount=0;
    @SerializedName("credits")
    @Expose
    private String credits;
    private Bitmap bitmap;
    private boolean isVideoType=false;

    @Ignore
    public boolean isAd = false;


    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setWallpaper(String wallpaper) {
        this.wallpaper = wallpaper;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setWallpaperName(String wallpaperName) {
        this.wallpaperName = wallpaperName;
    }

    public void setIsLike(Integer isLike) {
        this.isLike = isLike;
    }

    public Integer getId() {
        return id;
    }

    public String getWallpaper() {
        return wallpaper;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getWallpaperName() {
        return wallpaperName;
    }

    public Integer getIsLike() {
        return isLike;
    }

    public boolean isVideoType() {
        return isVideoType;
    }

    public String getCredits() {
        return credits;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    public void setVideoType(boolean videoType) {
        isVideoType = videoType;
    }

    public String getVideo_wallpaper_thumbnail() {
        return video_wallpaper_thumbnail;
    }

    public void setVideo_wallpaper_thumbnail(String video_wallpaper_thumbnail) {
        this.video_wallpaper_thumbnail = video_wallpaper_thumbnail;
    }

    @SerializedName("video_wallpaper_thumbnail")
    @Expose
    private String video_wallpaper_thumbnail;

    public String getWallpaper_thumbnail() {
        return wallpaper_thumbnail;
    }

    public void setWallpaper_thumbnail(String wallpaper_thumbnail) {
        this.wallpaper_thumbnail = wallpaper_thumbnail;
    }

    @SerializedName("wallpaper_thumbnail")
    @Expose
    private String wallpaper_thumbnail;

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    @SerializedName("video")
    @Expose
    private String video;

    public String getAverageRating() {
        return AverageRating;
    }

    public void setAverageRating(String averageRating) {
        AverageRating = averageRating;
    }

    @SerializedName("AverageRating")
    @Expose
    private String AverageRating;

    public String getYoulike() {
        return youlike;
    }

    public void setYoulike(String youlike) {
        this.youlike = youlike;
    }

    @SerializedName("youlike")
    @Expose
    private String youlike;

    public String getIs_premium() {
        return is_premium;
    }

    public void setIs_premium(String is_premium) {
        this.is_premium = is_premium;
    }

    @SerializedName("is_premium")
    @Expose
    private String is_premium;
}
