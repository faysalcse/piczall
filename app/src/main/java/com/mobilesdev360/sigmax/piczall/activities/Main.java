package com.mobilesdev360.sigmax.piczall.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.adapters.DrawerAdapter;
import com.mobilesdev360.sigmax.piczall.adapters.PagerAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.DrawerItem;
import com.mobilesdev360.sigmax.piczall.dataClass.User;
import com.mobilesdev360.sigmax.piczall.fiveStar.FiveStarsDialog;
import com.mobilesdev360.sigmax.piczall.fiveStar.NegativeReviewListener;
import com.mobilesdev360.sigmax.piczall.fiveStar.NeverListener;
import com.mobilesdev360.sigmax.piczall.fiveStar.NotNowListener;
import com.mobilesdev360.sigmax.piczall.fiveStar.ReviewListener;
import com.mobilesdev360.sigmax.piczall.notificationutils.Config;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.RememberMe;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.Tools;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;
import com.mobilesdev360.sigmax.piczall.utils.permissionutils.PermissionUtil;
import com.mobilesdev360.sigmax.piczall.utils.preferenceutil.SharedPref;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import com.yarolegovich.slidingrootnav.callback.DragListener;

import java.util.ArrayList;
import java.util.List;


import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Main extends AppCompatActivity implements DragListener, NegativeReviewListener, ReviewListener, NeverListener, NotNowListener {

    public static SlidingRootNav objSlideNav;
    Toolbar toolbar;
    List<DrawerItem> liOptions;
    ListView menuListView;
    TextView txtUserName;
    ImageView imgUser;
    ImageView imgSearch;
    ProgressDialog pDialog;
    DrawerAdapter drawerAdapter;
    TabLayout tabLayout;
    ImageView imgMenu;
    ViewPager viewPager;
    PagerAdapter adapter;
    public static String bannerid="2582311578562048_2582327748560431";
    private static final String TAG = Main.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setUpAndLoadAd(R.layout.activity_main, R.id.main, true);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });


        int memClass = ((ActivityManager) this
                .getSystemService(Context.ACTIVITY_SERVICE))
                .getLargeMemoryClass();
        displayFirebaseRegId();
        RajexHtml.Addtags("");
        request_permissioon();
        String token=SharedPref.read("Device_id");
        if(token!=null)
        {
            try {


                @SuppressLint("HardwareIds")
                String device_unique_id = android.provider.Settings.Secure.getString(this.getContentResolver(),
                        android.provider.Settings.Secure.ANDROID_ID);

                WebServices webServices = new WebServices(this);
                webServices.call_api(Paths.token_insert_update + device_unique_id + "/" + token, Request.Method.GET, "", null);
            }
            catch ( Exception e)
            {

            }

        }
        try {
            FirebaseApp.initializeApp(this);
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                Log.w("TAG", "getInstanceId failed", task.getException());
                                return;
                            }
                            String token = task.getResult().getToken();
                            RememberMe rememberMe = new RememberMe(Main.this);
                            rememberMe.AddInfo("Device_Notification", token);
                        }
                    });
        } catch (Exception e) {
        }
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        RememberMe rememberMe = new RememberMe(this);
        Gson gson = new Gson();
        String json = rememberMe.getInfo("User", "");
        if (json.equalsIgnoreCase("")) {
            Settings.userObj = null;
        } else {
            Settings.userObj = gson.fromJson(json, User.class);
        }

        initialize(savedInstanceState);

        initialInMobiBannerAd();
    }

    private void initialInMobiBannerAd() {
        AdView adView = findViewById(R.id.adView);
        adView.loadAd(new AdRequest.Builder().build());
    }

    void initialize(Bundle savedInstanceState) {
        // side menu working
        imgSearch = findViewById(R.id.imgSearch);
        imgSearch.setVisibility(View.VISIBLE);
        imgMenu = findViewById(R.id.imgMenu);
        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

        liOptions = new ArrayList<>();
        if (Splash.userid.equals("")) {
            liOptions.add(new DrawerItem(R.mipmap.popular, "Featured"));//0
            liOptions.add(new DrawerItem(R.mipmap.category, "Categories"));//1
            liOptions.add(new DrawerItem(R.mipmap.feature, "Popular"));//2
            liOptions.add(new DrawerItem(R.mipmap.ic_video, "Video"));//3
            liOptions.add(new DrawerItem(R.mipmap.icon_rateus, "Rate Us"));//3
            liOptions.add(new DrawerItem(R.mipmap.term_condition, "Terms and Conditions"));//3
            liOptions.add(new DrawerItem(R.mipmap.privacy, "Privacy policy"));//4
            liOptions.add(new DrawerItem(R.mipmap.user_icon, "Login/Signup"));//5

        } else {
            liOptions.add(new DrawerItem(R.mipmap.popular, "Featured"));//0
            liOptions.add(new DrawerItem(R.mipmap.category, "Categories"));//1
            liOptions.add(new DrawerItem(R.mipmap.feature, "Popular"));//2
            liOptions.add(new DrawerItem(R.mipmap.ic_video, "Video"));//3
            liOptions.add(new DrawerItem(R.mipmap.like, "Favourite"));
            liOptions.add(new DrawerItem(R.mipmap.icon_rateus, "Rate Us"));//3
            liOptions.add(new DrawerItem(R.mipmap.term_condition, "Terms and Conditions"));//5
            liOptions.add(new DrawerItem(R.mipmap.privacy, "Privacy policy"));//6
            liOptions.add(new DrawerItem(R.mipmap.logout, "Logout"));//7

        }


        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (objSlideNav.isMenuOpened()) {
                    objSlideNav.closeMenu();
                } else if (objSlideNav.isMenuClosed()) {
                    objSlideNav.openMenu();
                }
            }
        });
        objSlideNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu)
                .addDragListener(this)
                .inject();
        menuListView = findViewById(R.id.list);
        drawerAdapter = new DrawerAdapter(this, liOptions);
        txtUserName = findViewById(R.id.txtUserName);
        imgUser = findViewById(R.id.imgUser);


        menuListView.setDividerHeight(0);
        menuListView.setAdapter(drawerAdapter);
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position == 0) {
                    tabLayout.getTabAt(0).select();
                } else if (position == 1) {
                    tabLayout.getTabAt(1).select();
                } else if (position == 2) {
                    tabLayout.getTabAt(2).select();
                } else if (position == 3) {
                    tabLayout.getTabAt(3).select();
                } else if (position == 4) {
                    if (Splash.userid.equals("")) {
                     rateApp();
                    }else {
                        Intent intent = new Intent(Main.this, Favourite.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }

                }else if (position == 5){
                    if (Splash.userid.equals("")) {
                        Intent intent = new Intent(Main.this, TermsAndConditions.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }else{
                        rateApp();
                    }
                }
                else if (position == 6) {  // Terms and Conditions
                    if (Splash.userid.equals("")) {
                        Intent intent = new Intent(Main.this, PrivacyPolicy.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(Main.this, TermsAndConditions.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }


//                    if (Settings.userObj == null) {
//                        Intent intent = new Intent(Main.this, TermsAndConditions.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    } else {
//                        Intent intent = new Intent(Main.this, Favourite.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    }

                } else if (position == 7) {  // Privacy policy
                    if (Splash.userid.equals("")) {
                        Class<?> aClass=Main.class;
                        if(!Splash.userid.equals("")) {
                            Splash.userid = "";
                            SharedPref.clear();
                            aClass=Main.class;

                        }
                        else
                        {
                            aClass=Login.class;
                        }
                        Intent intent = new Intent(Main.this, aClass);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(Main.this, PrivacyPolicy.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }


//                    if (Settings.userObj == null) {
//                        Intent intent = new Intent(Main.this, PrivacyPolicy.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    } else {
//                        ResetPassword resetPasswordDlg = new ResetPassword();
//                        resetPasswordDlg.ShowMessageDialog(Main.this, "Reset Password");
//                    }

                } else if (position == 8) {
                    if(Splash.userid.equals("")) {

                    }else{
                        Class<?> aClass=Main.class;
                        if(!Splash.userid.equals("")) {
                            Splash.userid = "";
                            SharedPref.clear();
                            aClass=Main.class;

                        }
                        else
                        {
                            aClass=Login.class;
                        }
                        Intent intent = new Intent(Main.this, aClass);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

//                    if (Settings.userObj == null) {
//                        Intent intent = new Intent(Main.this, Login.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    }
//                    else if (Settings.userObj != null) {
//                        pDialog.show();
//                        Map<String, Object> params = new HashMap<String, Object>();
//                        String url = Paths.BaseUrl + "logout";
//                        WebServices.getInstance().MyPostRequestWithoutAuth(Main.this, url, params, new WebServices.RequestResults() {
//                            @Override
//                            public void requestFailed(VolleyError error) {
//                                pDialog.dismiss();
//                                RememberMe rememberMe = new RememberMe(Main.this);
//                                Settings.userObj = null;
//                                rememberMe.AddInfo("User", null);
//                                Intent intent = new Intent(Main.this, Main.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                finish();
//                            }
//
//                            @Override
//                            public void requestSucceeded(JSONObject response) {
//                                pDialog.dismiss();
//                                RememberMe rememberMe = new RememberMe(Main.this);
//                                rememberMe.AddInfo("User", null);
//                                Intent intent = new Intent(Main.this, Main.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                finish();
//                            }
//                        });
//
//
////                        Intent intent = new Intent(Main.this, TermsAndConditions.class);
////                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
////                        startActivity(intent);
//                    }
                }
//                else if (position == 8) {
//                    if (Settings.userObj != null) {
//                        Intent intent = new Intent(Main.this, PrivacyPolicy.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    }
//                } else if (position == 9) {
//                    pDialog.show();
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    String url = Paths.BaseUrl + "logout";
//                    WebServices.getInstance().MyPostRequestWithoutAuth(Main.this, url, params, new WebServices.RequestResults() {
//                        @Override
//                        public void requestFailed(VolleyError error) {
//                            pDialog.dismiss();
//                            RememberMe rememberMe = new RememberMe(Main.this);
//                            Settings.userObj = null;
//                            rememberMe.AddInfo("User", null);
//                            Intent intent = new Intent(Main.this, Main.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intent);
//                            finish();
//                        }
//
//                        @Override
//                        public void requestSucceeded(JSONObject response) {
//                            pDialog.dismiss();
//                            RememberMe rememberMe = new RememberMe(Main.this);
//                            rememberMe.AddInfo("User", null);
//                            Intent intent = new Intent(Main.this, Main.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intent);
//                            finish();
//                        }
//                    });
//                }

                objSlideNav.closeMenu();
            }
        });

        if (Splash.userid.equals("")) {
            txtUserName.setText("Guest");
        } else {


            SharedPref.init(this);

            String name = SharedPref.read("USER_NNAME");

            if (name != null) {
                txtUserName.setText(name);
            } else {
                txtUserName.setText("Guest");
            }




        }

        //Tablayout working
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Featured"));
        tabLayout.addTab(tabLayout.newTab().setText("Categories"));
        tabLayout.addTab(tabLayout.newTab().setText("Popular"));
        tabLayout.addTab(tabLayout.newTab().setText("Live Wall"));
        tabLayout.setTabTextColors(Color.parseColor("#c9c9c9"), Color.parseColor("#ffffff"));

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab,null);
            Typeface myTypeface = Typeface.createFromAsset(getAssets(), "montserrat_light.otf");
            tv.setTypeface(myTypeface);
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        Tools.wrapTabIndicatorToTitle(tabLayout,20,20);

        viewPager = findViewById(R.id.pager);
        adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        //. total number of pages minus 1
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main.this, SearchWallpapers.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });


    }

    public static void reduceMarginsInTabs(TabLayout tabLayout, int marginOffset) {

        View tabStrip = tabLayout.getChildAt(0);
        if (tabStrip instanceof ViewGroup) {
            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
            for (int i = 0; i < ((ViewGroup) tabStrip).getChildCount(); i++) {
                View tabView = tabStripGroup.getChildAt(i);
                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    ((ViewGroup.MarginLayoutParams) tabView.getLayoutParams()).leftMargin = marginOffset;
                    ((ViewGroup.MarginLayoutParams) tabView.getLayoutParams()).rightMargin = marginOffset;
                }
            }

            tabLayout.requestLayout();
        }
    }

    public void rateApp() {
        try {
            Intent rateIntent = rateIntentForUrl("market://details");
            startActivity(rateIntent);
        } catch (ActivityNotFoundException e) {
            Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
            startActivity(rateIntent);
        }
    }

    private Intent rateIntentForUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        } else {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }

    @Override
    public void onDrag(float progress) {

    }

    @Override
    public void onBackPressed() {
        if (objSlideNav.isMenuOpened()) {
            objSlideNav.closeMenu();
        } else if (objSlideNav.isMenuClosed()) {

            FiveStarsDialog fiveStarsDialog = new FiveStarsDialog(this,"");
            fiveStarsDialog.setRateText("Rate us 5 Stars on Google Play Store if you like our app")
                    .setTitle("Rate")
                    .setNeverButtonText("Exit")
                    .setForceMode(false)
                    .setStarColor(getResources().getColor(R.color.colorPrimaryDark))
                    .setUpperBound(3) // Market opened if a rating >= 2 is selected
                    .setNegativeReviewListener(this) // OVERRIDE mail intent for negative review
                    .setReviewListener(this) // Used to listen for reviews (if you want to track them )
                    .setNever(this)
                    .setNotNowListener(this)
                    .showAfter(1);
        }
    }

    public void request_permissioon() {
        if (PermissionUtil.checkall_permission(Main.this, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE)) {
        } else {
            PermissionUtil.request_allpermission(Main.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE);
        }
    }


    @Override
    public void onNever(int stars) {
        this.finish();
    }

    @Override
    public void onNotNow(int stars) {
//        this.finish();
    }


    @Override
    public void onNegativeReview(int i) {
        this.finish();
    }

    @Override
    public void onReview(int i) {
//        this.finish();
    }
    private void displayFirebaseRegId() {
        SharedPref.init(this);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId;
        regId = pref.getString("regId", null);
        Log.e("Splash", "Firebase reg id: " + regId);
        if (!TextUtils.isEmpty(regId)) {
            System.out.println("Firebase Reg Id: " + regId);
            SharedPref.write( "Device_id", regId);
            Log.e("Device_id", regId);

        } else {
            FirebaseApp.initializeApp(getApplicationContext());
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            SharedPref.write( "Device_id", refreshedToken);
            if (!TextUtils.isEmpty(refreshedToken)) {
                System.out.println("Firebase Reg Id is not received yet! Trying Again: \n" + refreshedToken);
                SharedPref.write( "Device_id", refreshedToken);
            } else {
                System.out.println("Firebase Reg Id is not received yet! Trying Again: \n" + refreshedToken);

            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
