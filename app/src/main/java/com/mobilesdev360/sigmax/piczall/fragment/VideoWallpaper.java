package com.mobilesdev360.sigmax.piczall.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.activities.VideoViews;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.adapters.AllPopularImagesAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.remote.RetrofitProvider;
import com.mobilesdev360.sigmax.piczall.remote.SOService;
import com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoWallpaper extends Fragment {
    View rootView;
    List<ImageData> listOfTopImages;
    GridViewWithHeaderAndFooter gridView;
    AllPopularImagesAdapter mainAdapter;
    View loadMoreView;
    int pageStart = 0, pageSize = 18;
    boolean canScroll, IsMoreScroll, IsFirstCall = true;
    int lastLastitem = 0;
    int RequestCode = 1122;
    ProgressDialog pDialog;
    TextView txtNoDataFound;
    SwipeRefreshLayout pullToRefresh;
    private SOService mService;

    public VideoWallpaper() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_video_wallpaper, container, false);
        mService = RetrofitProvider.getClient().create(SOService.class);
        pullToRefresh = rootView.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getVideoWallpaerper();
                pullToRefresh.setRefreshing(false);
            }
        });
        getVideoWallpaerper();
        return rootView;
    }

    private VideoWallpaper getVideoWallpaerper() {
        mService.getVideoDataall().enqueue(new Callback<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel>() {
            @Override
            public void onResponse(Call<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel> call, Response<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel> response) {
                if (response.isSuccessful()) {
                    VideoWallpaperModel model = response.body();
                    List<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper> videoWallpaperList = model.getVideoWallpaper();


                    Log.d("chkresponse", "success" + response.body());
                    initAll(videoWallpaperList);
                } else {
                    Log.d("chkresponse", "error:" + response.code());
                }
            }

            @Override
            public void onFailure(Call<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel> call, Throwable t) {
                Log.d("chkresponse", "failed:" + t.getMessage());
            }
        });
        return null;
    }

    private void initAll(List<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper> videoWallpapers) {
        listOfTopImages = new ArrayList();
        gridView = (GridViewWithHeaderAndFooter) rootView.findViewById(R.id.gridview);
        txtNoDataFound = (TextView) rootView.findViewById(R.id.txtNoDataFound);
        //get data and init adapter,view
        listOfTopImages = new ArrayList<>();

//        Collections.reverse(videoWallpapers);

        for (com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper v : videoWallpapers) {
            ImageData imageData = new ImageData();
            imageData.setId(v.getId());
//            imageData.setWallpaper(v.getWallpaper());
            imageData.setWallpaperName(v.getWallpaperName());
            imageData.setCategoryId(3);
            imageData.setLikeCount(3);
            imageData.setIsLike(2);
            imageData.setVideoType(true);
            imageData.setCredits("2.0");
            imageData.setIs_premium(v.getIs_premium());


            //new
            imageData.setAverageRating(v.getAverageRating());
            imageData.setWallpaper(v.getVideo());
            imageData.setVideo_wallpaper_thumbnail(v.getVideo_wallpaper_thumbnail());

            listOfTopImages.add(imageData);
        }


        mainAdapter = new AllPopularImagesAdapter(getActivity(), listOfTopImages, new AllPopularImagesAdapter.MainAdapterItemClickListener() {
            @Override
            public void onItemClick(ImageView imageView, int position) {
                if (mainAdapter.getItem(position).isVideoType() == true) {
                    Log.w("NATIVE", "live video");
                    Intent intent = new Intent(getActivity(), VideoViews.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    ImageData imageData = mainAdapter.getItem(position);
                    Gson gson = new Gson();
                    intent.putExtra("SelectedImage", gson.toJson(imageData));
                    intent.putExtra("image", "");
                    startActivity(intent);
                } else {
                    Log.w("NATIVE", "view image screen");
                    Intent intent = new Intent(getActivity(), ViewImageScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.putExtra("SelectedImage", mainAdapter.getItem(position));
                    startActivity(intent);
                }
            }
        });
        gridView.setAdapter(mainAdapter);

    }

}
