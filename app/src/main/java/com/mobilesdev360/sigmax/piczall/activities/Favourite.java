package com.mobilesdev360.sigmax.piczall.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.Fragment;

import com.mobilesdev360.sigmax.piczall.fragment.MyFavourites;
import com.mobilesdev360.sigmax.piczall.R;

public class Favourite extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        replaceFragment(new MyFavourites());
    }
    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.framefavouriteContainer, fragment)
                .commit();
    }
}
