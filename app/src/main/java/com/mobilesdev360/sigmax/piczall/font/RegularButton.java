package com.mobilesdev360.sigmax.piczall.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class RegularButton  extends androidx.appcompat.widget.AppCompatButton {

    public RegularButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public RegularButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public RegularButton(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        // Just Change your font name
        Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(),  "montserrat_semibold.otf");
        setTypeface(myTypeface);
    }

}
