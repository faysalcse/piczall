package com.mobilesdev360.sigmax.piczall.activities;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.adapters.VideoAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.dataClass.User;
import com.mobilesdev360.sigmax.piczall.fragment.VideoWallpaper;
import com.mobilesdev360.sigmax.piczall.remote.RetrofitProvider;
import com.mobilesdev360.sigmax.piczall.remote.SOService;
import com.mobilesdev360.sigmax.piczall.remote.apiCall.ApiCall;
import com.mobilesdev360.sigmax.piczall.remote.apiCall.ApiCallListener;
import com.mobilesdev360.sigmax.piczall.utils.ApplicationConstraints;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.RememberMe;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;
import com.mobilesdev360.sigmax.piczall.utils.admobutil.AdmobListener;
import com.mobilesdev360.sigmax.piczall.utils.admobutil.AdmobUtil;
import com.mobilesdev360.sigmax.piczall.utils.googlepay.GooglePay;
import com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel;
import com.mobilesdev360.sigmax.piczall.utils.model.checkchase.CheckPurchase;
import com.mobilesdev360.sigmax.piczall.utils.model.getwallpaperrating.GetWallpaperRating;
import com.mobilesdev360.sigmax.piczall.utils.model.purchasewallpaper.PurchaseWallpaper;
import com.mobilesdev360.sigmax.piczall.utils.model.ratevideowallpaper.RateVideoWallpaper;
import com.mobilesdev360.sigmax.piczall.utils.permissionutils.PermissionUtil;
import com.mobilesdev360.sigmax.piczall.utils.preferenceutil.SharedPref;
import com.mobilesdev360.sigmax.piczall.utils.progresshandler.ProgressbarHandler;
import com.mobilesdev360.sigmax.piczall.utils.videodownloadmanager.AppDownloadManager;
import com.mobilesdev360.sigmax.piczall.utils.videofilepath.VideoFileUtils;
import com.mobilesdev360.sigmax.piczall.utils.videowallpaperservice.VideoWallpaperService;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.varunest.sparkbutton.SparkButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class FullWallpaperActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, ApiCallListener, AdmobListener, ExoPlayer.EventListener {
    private RewardedVideoAd mRewardedVideoAd;
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 42;
    protected ExoPlayer player = null;
    protected PlayerView playerView;
    protected ProgressBar progressBar;
    protected ProgressBar prdownload;
    protected boolean is_downloaded = false;
    Context context;
    List<ImageData> listOfImages;
    Activity myActivity;
    TextView progress_text;
    ImageView imageView;
    ImageView imgShare;
    SparkButton imgLike;
    SparkButton imgUnclockButton;
    ImageView imgDownload;
    TextView txtImageName;
    TextView txtNoLikes;
    TextView txtprogres;
    ImageView imgcross;
    VideoView videoView;
    ImageView dollerSignView;
    TextView amoutTextView;
    ProgressDialog pDialog;
    private long downloadId = -1;
    private ImageData imageData;
    private TextView ratingTextView;
    private RatingBar ratingBars;
    private ApiCall apiCall;
    private Uri uri;
    private ConstraintLayout ratingConstraintLayout;
    private boolean isPurchase;
    private AdmobUtil admobUtil;
    private PaymentsClient mPaymentsClient;
    private boolean isPreparing;
    protected boolean iSpremium=false;
    protected ViewPager viewPager;
    private SOService mService;
    VideoAdapter videoAdapter;
    ArrayList<ImageData> listOfTopImages;
    ImageView imgblur;

//    private ImageView testImageView;

    //download reciever
    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Fetching the download id received with the broadcast
            Log.d("checkbroadcast", "download complete");
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (downloadId == id) {
                Log.d("checkbroadcast", "download complete with id");
                Toast.makeText(context, "Download Completed", Toast.LENGTH_SHORT).show();
                setThisWallpaper();
                return;
            }

        }
    };
    private long downloadID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_wallpaper_view_image);
        request_permissioon();
        String imageDataString = getIntent().getStringExtra("SelectedImage");
        Gson gson = new Gson();
        playerView = findViewById(R.id.video_view);
        imgblur =findViewById(R.id.image);
//        AdmobUtil.intentialadd(this);
//        AdmobUtil.show_adds(FullWallpaperActivity.this);


        apiCall = ApiCall.getInstance();
        apiCall.initApiCall(this);
        imageData = gson.fromJson(imageDataString, ImageData.class);

        registerReceiver(onDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        context = FullWallpaperActivity.this;

//        testImageView = findViewById(R.id.fullImageView);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int width = displayMetrics.widthPixels;
        final int height = displayMetrics.heightPixels;


        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(width + 200, height + 200);
//        testImageView.setLayoutParams(layoutParams);
        mService = RetrofitProvider.getClient().create(SOService.class);
//        getVideoWallpaerper();
        initView();
        retriveUserInfo();
        // initializeGooglePaymentClient();
    }

    public void retrieveImage() {
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.TRANSPARENT)
                .borderWidthDp(0)
                .cornerRadiusDp(6)
                .oval(false)
                .build();
        Picasso.get()
                .load(Paths._thumbnail_path_video + imageData.getVideo_wallpaper_thumbnail())
                .transform(transformation)
                .error(R.drawable.itembg).placeholder(R.drawable.itembg).into(imageView);
        Picasso.get()
                .load(Paths._thumbnail_path_video + imageData.getVideo_wallpaper_thumbnail())
                .error(R.drawable.itembg).placeholder(R.drawable.itembg).into(imgblur);
    }

    private void initializeGooglePaymentClient() {
        // initialize a Google Pay API client for an environment suitable for testing
        mPaymentsClient =
                Wallet.getPaymentsClient(
                        this,
                        new Wallet.WalletOptions.Builder()
                                .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                                .build());
    }

    private void processPayment() {
        PaymentDataRequest request = null;
        try {
            request = PaymentDataRequest.fromJson(GooglePay.getPaymentDataRequest(imageData.getCredits()).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (request != null) {
            AutoResolveHelper.resolveTask(
                    mPaymentsClient.loadPaymentData(request),
                    FullWallpaperActivity.this,
                    // LOAD_PAYMENT_DATA_REQUEST_CODE is a constant integer value you define
                    LOAD_PAYMENT_DATA_REQUEST_CODE);
        }
    }

    public void play_video(Uri url) {
//        imageView.setVisibility(View.GONE);
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
//Create the player
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);

        playerView.setPlayer(player);
        player.addListener(this);
//        Uri uri = Uri.parse(url);
        playerView.setUseController(false);
        MediaSource mediaSource = buildMediaSource(url);
        player.prepare(mediaSource, true, false);
        player.setPlayWhenReady(true);
        progressBar.setVisibility(View.VISIBLE);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultHttpDataSourceFactory("Exoplayer-local")).
                createMediaSource(uri);
    }

    public String retriveUserInfo() {
        RememberMe rememberMe = new RememberMe(this);
        Gson gson = new Gson();
        String json = rememberMe.getInfo("User", "");
        if (json.equalsIgnoreCase("")) {
            Settings.userObj = null;
            return "";
        } else {
            Settings.userObj = gson.fromJson(json, User.class);
            String userId = "" + Settings.userObj.getId();
            Log.d("chekcuser", "userid:" + userId);
            return userId;

        }
    }

    public void initView() {
        viewPager=findViewById(R.id.videopager);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        prdownload = (ProgressBar) findViewById(R.id.prdownload);
        dollerSignView = findViewById(R.id.doller_sign_view);
        amoutTextView = findViewById(R.id.amout_text);

        imageView = (ImageView) findViewById(R.id.imageView);
        imgShare = (ImageView) findViewById(R.id.imgShare);
        imgLike = (SparkButton) findViewById(R.id.imgLike);
        imgUnclockButton = findViewById(R.id.img_set_video_wallpaper);
        imgUnclockButton.setVisibility(View.VISIBLE);
        imgDownload = findViewById(R.id.imgDownload);
        txtprogres = (TextView) findViewById(R.id.txtprogress);
        txtImageName = findViewById(R.id.txtImageName);
        txtNoLikes = findViewById(R.id.txtNoLikes);
        imgcross = findViewById(R.id.imgcross);
        videoView = (VideoView) findViewById(R.id.videoView);
        ratingConstraintLayout = findViewById(R.id.constraint_layout_rating_number_section);
        ratingBars = findViewById(R.id.rating_bar);
        ratingTextView = findViewById(R.id.text_view_rating_number);
        ratingConstraintLayout.setVisibility(View.VISIBLE);
        admobUtil = AdmobUtil.getInstance();
        admobUtil.setAdmobListener(this);
        retrieveImage();


    }
    public void SetUpMainContent()
    {
        initVideoComponents();
        String rating=imageData.getAverageRating();
        if(rating!=null&&!rating.equals(""))
        {
            rating=rating.substring(0,1);
            ratingTextView.setText(rating);
            ratingBars.setRating(Float.parseFloat(rating));
        }
        else{
            ratingTextView.setText("0");
        }

        ratingConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRatingDialog();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void setCurrencyVisibility() {
        Double amount = Double.parseDouble(imageData.getCredits());
        if (amount > 0) {
            dollerSignView.setVisibility(View.GONE);
            amoutTextView.setVisibility(View.GONE);
            amoutTextView.setText(imageData.getCredits());
        } else {
            dollerSignView.setVisibility(View.GONE);
            amoutTextView.setVisibility(View.GONE);

        }
        if (!retriveUserInfo().isEmpty()) {
            apiCall.callCheckPurchaseApi(retriveUserInfo(), "" + imageData.getId());
        } else {
            //check if user purchase this wallpaper

            //retrive current rating of this wallapper
        }
    }

    @Override
    protected void onStop() {

        super.onStop();
//        if(player!=null)
//        {
//            player.stop();
//        }
    }

    private void initVideoComponents() {
        if(imageData.getIs_premium().equals("1"))
        {
            iSpremium=true;
            imgUnclockButton.setInactiveImage(R.drawable.lock);
            loadRewardedVideoAd();
        }

        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

        setCurrencyVisibility();
        imgDownload.setVisibility(View.GONE);
        imgShare.setVisibility(View.GONE);
        txtImageName.setText(imageData.getWallpaperName());

        //retrieve current rating of this wallpaper
//        apiCall.callRateWallpaperRatingApi(""+imageData.getId(), ApplicationConstraints.TYPE_VIDEO);
        imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (imageData.isVideoType() == true) {
            File existsfile = new File(Environment.getExternalStorageDirectory() + "/PiczallVideo/" + imageData.getWallpaperName() + ".mp4");
            if (existsfile.exists()) //check the file exists and set download button action
            {
                is_downloaded = true;

                uri = Uri.parse(existsfile.getAbsolutePath());

                loadViewo(imageView, videoView, uri);
                imgUnclockButton.setVisibility(View.VISIBLE);
                imgUnclockButton.setInactiveImage(R.mipmap.video);
                imgUnclockButton.setActiveImage(R.mipmap.video);
                playerView.setVisibility(View.GONE);

            } else {
                is_downloaded=false;

                imgUnclockButton.setVisibility(View.VISIBLE);

                String url = Paths._video + imageData.getWallpaper();
                Uri uri = Uri.parse(url);
                play_video(uri);
            }
            imgUnclockButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (is_downloaded) {
                        setThisWallpaper();

                    } else {

                        if (iSpremium&&mRewardedVideoAd.isLoaded()) {
                            mRewardedVideoAd.show();
                        }
                        else
                        {
                            new DownloadFile().execute(Paths._video + imageData.getWallpaper());
                        }
//
//                            WebServices webServices=new WebServices(context);
//                            webServices.call_api(Paths.download_wallpaper+String.valueOf(imageData.getId()), Request.Method.GET,"",null);


//                            try {
//                                wallpaperManager.clear();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }


                    }
                }
            });

        }

    }

    /**
     * Show rating dialog
     */
    private void showRatingDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_review, null);
        final Button buttonSubmit = (Button) alertLayout.findViewById(R.id.button_submit);
        RatingBar ratingBar = alertLayout.findViewById(R.id.rating_bar_video);
        TextView rateText = alertLayout.findViewById(R.id.text_view_rate_text);
        changeTextForRating(ratingBar, rateText);
        EditText ratingText = alertLayout.findViewById(R.id.edit_text_review);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Splash.userid.equals(""))
                {
                    Intent intent = new Intent(context, Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
                }
                else
                {
                    call_api(Paths.make_rating_wallpaper+Splash.userid+"/"+String.valueOf(imageData.getId())+"/",ratingBar.getRating());
//                    SharedPref.write(String.valueOf(imageData.getId()),String.valueOf(ratingBar.getRating()));
//                    ratingTextView.setText(String.valueOf(ratingBar.getRating()));
//                    ratingBars.setRating(ratingBar.getRating());
                }
                apiCall.callRateVideoWallpaperApi("" + retriveUserInfo(), "" + imageData.getId(), ApplicationConstraints.TYPE_VIDEO, "" + ratingBar.getRating(), "ok");
                // rateThisVideo("" + ratingBar.getRating(), ratingText.getText().toString());
                // Snackbar.make(v, getResources().getString(R.string.rate_this_video_dialog), Snackbar.LENGTH_LONG).setAction(null, null).show();
                dialog.dismiss();

            }

        });


        dialog.show();
    }

    /**
     * Rating count and change text
     *
     * @param textView change text
     */
    private void changeTextForRating(RatingBar ratingBar, TextView textView) {
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (rating <= 1.0) {
                    textView.setText(getResources().getString(R.string.rating_dialog_hated_it));
                } else if (rating <= 2.0) {
                    textView.setText(getResources().getString(R.string.rating_dialog_disliked_it));
                } else if (rating <= 3.0) {
                    textView.setText(getResources().getString(R.string.rating_dialog_it_okay));
                } else if (rating <= 4.0) {
                    textView.setText(getResources().getString(R.string.rating_dialog_liked_it));
                } else {
                    textView.setText(getResources().getString(R.string.rating_dialog_love_it));
                }
            }

        });


    }

    private void processImageDownloadAndSave() {

        if (PermissionUtil.init(context).request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE) == true) {
            ProgressbarHandler.ShowLoadingProgress(FullWallpaperActivity.this);
            pDialog.show();
            String imageName = imageData.getWallpaper();
            if (!VideoFileUtils.GetLocalFileByName(context, imageName).exists()) {
                downloadId = AppDownloadManager.download(Paths._video + imageData.getWallpaper(), imageData.getWallpaperName(), imageData.getWallpaperName(), context);
                Log.d("checkexistence", "downloading");
            } else {
                Log.d("checkexistence", "exists");
            }

        } else {
            PermissionUtil.request_allpermission(FullWallpaperActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE);
        }
    }

    private void setThisWallpaper() {
        SharedPref.init(context);
        SharedPref.write(ApplicationConstraints.WALLPAPER_NAME, imageData.getWallpaperName());
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());
//        try {
//            wallpaperManager.clear();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        VideoWallpaperService vid = new VideoWallpaperService();
        Intent intent = new Intent(
                WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
        intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT,

                new ComponentName(context, vid.getClass()));
        context.startActivity(intent);
        this.finish();

    }

    private void loadViewo(ImageView imageView, VideoView videoView, Uri uri) {

        try {
            videoView.setVisibility(View.VISIBLE);
            videoView.setVideoURI(uri);
            videoView.start();
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    File fdelete = new File(String.valueOf(uri));
                    fdelete.delete();
                    is_downloaded = false;
                    String url = Paths._video + imageData.getWallpaper();
                    Uri uri = Uri.parse(url);
                    play_video(uri);
                    return true;
                }
            });


            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {

                    mp.setLooping(true);
                    Log.d("testlog", "prepared");
                    imageView.setVisibility(View.GONE);

                }

            });
        } catch (Exception e) {

        }


    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(onDownloadComplete);
        super.onDestroy();
    }

    @Override
    public void OnCheckPurchaseSuccess(CheckPurchase checkPurchase) {
        Log.d("chkrating", checkPurchase.toString());
        if (checkPurchase.getResponse().toLowerCase().equals("true")) {
            isPurchase = true;
            amoutTextView.setText("Purchased");
            dollerSignView.setVisibility(View.GONE);
            //processImageDownloadAndSave();
        } else {
            isPurchase = false;
        }
    }

    @Override
    public void OnGetWallpaperRatingSuccess(GetWallpaperRating getWallpaperRating) {
        DecimalFormat df = new DecimalFormat("####0.00");
        Log.d("chkrating", getWallpaperRating.toString());
        if (getWallpaperRating.getAvgRatting() != null) {
            String rating = df.format(getWallpaperRating.getAvgRatting());
            Float d = Float.parseFloat(rating);
            ratingBars.setRating(d / 5);
            ratingTextView.setText(rating);

        } else {
            ratingBars.setRating(0);
            ratingTextView.setText("0.0");
            Log.d("chkrating", getWallpaperRating.toString());
        }
    }

    @Override
    public void OnRateVideoWallpaperSuccess(RateVideoWallpaper rateVideoWallpaper) {
        if (rateVideoWallpaper.getStatusCode() != null) {
            Toast.makeText(FullWallpaperActivity.this, "Wallpaper Rated Successfully!", Toast.LENGTH_LONG).show();
            apiCall.callRateWallpaperRatingApi("" + imageData.getId(), ApplicationConstraints.TYPE_VIDEO);
        } else {
            Toast.makeText(FullWallpaperActivity.this, "You have rated this wallpaper already!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void OnPurchaseWallpaperSuccess(PurchaseWallpaper purchaseWallpaper) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(FullWallpaperActivity.this, "Wallpaper purchases sucessfully", Toast.LENGTH_LONG).show();
                amoutTextView.setText("Purchased");
                dollerSignView.setVisibility(View.GONE);
                processImageDownloadAndSave();
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }

    @Override
    public void OnAdOpen() {
        // ProgressbarHandler.DismissProgress(FullWallpaperActivity.this);
        pDialog.dismiss();
    }

    @Override
    public void OnAdClosed() {
        processImageDownloadAndSave();
    }

    @Override
    public void OnPointEarned(RewardItem rewardItem) {
    }

    @Override
    public void OnAdFailed(String message) {
        // ProgressbarHandler.DismissProgress(FullWallpaperActivity.this);
        pDialog.dismiss();
        Toast.makeText(this, "No Ads Found Please try again after sometime", Toast.LENGTH_SHORT).show();

    }

    /**
     * Handle a resolved activity from the Google Pay payment sheet
     *
     * @param requestCode the request code originally supplied to AutoResolveHelper in
     *                    requestPayment()
     * @param resultCode  the result code returned by the Google Pay API
     * @param data        an Intent from the Google Pay API containing payment or error data
     * @see <a href="https://developer.android.com/training/basics/intents/result"
     * >Getting a result
     * from an Activity
     * </a>
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // value passed in AutoResolveHelper
            case LOAD_PAYMENT_DATA_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        PaymentData paymentData = PaymentData.getFromIntent(data);
                        String json = paymentData.toJson();
                        // if using gateway tokenization, pass this token without modification
                        JSONObject paymentMethodData = null;
                        try {
                            paymentMethodData = new JSONObject(json)
                                    .getJSONObject("paymentMethodData");
                            String paymentToken = paymentMethodData
                                    .getJSONObject("tokenizationData")
                                    .getString("token");
                            Log.d("checkcard", "token:" + paymentToken);
                            if (paymentToken != null) {
                                if (!paymentToken.isEmpty()) {
                                    apiCall.callPurchaseWallpaperApi(retriveUserInfo(), "" + imageData.getId());
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d("checkcard", "cancelled");
                        break;
                    case AutoResolveHelper.RESULT_ERROR:
                        Status status = AutoResolveHelper.getStatusFromIntent(data);
                        // Log the status for debugging.
                        // Generally, there is no need to show an error to the user.
                        // The Google Pay payment sheet will present any account errors.
                        Log.d("checkcard", "failed");
                        break;
                    default:
                        // Do nothing.
                }
                break;
            default:
                // Do nothing.
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_BUFFERING) {
            progressBar.setVisibility(View.VISIBLE);
//            Static_Functions.show_progrss(this);
        } else if (playbackState == ExoPlayer.STATE_READY) {

            progressBar.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
            playerView.setVisibility(View.VISIBLE);
//            Static_Functions.hidedialoge();
        } else if (playbackState == ExoPlayer.STATE_ENDED) {
//            Static_Functions.hidedialoge();
            playerView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public void finish() {
        super.finish();
        if (player != null) {
            player.stop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (is_downloaded) {
            loadViewo(imageView, videoView, uri);
        } else {
            imageView.setVisibility(View.VISIBLE);
            initVideoComponents();
        }
    }

    public void request_permissioon() {
        if (PermissionUtil.checkall_permission(FullWallpaperActivity.this, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE)) {
        } else {
            PermissionUtil.request_allpermission(FullWallpaperActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE);
        }
    }

    private void beginDownload() {
        String folder = Environment.getExternalStorageDirectory() + File.separator + "PiczallVideo/";

        //Create androiddeft folder if it does not exist
        File directory = new File(folder);

        if (!directory.exists()) {
            directory.mkdirs();
        }
        /*
        Create a DownloadManager.Request with all the information necessary to start the download
         */
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse("http://speedtest.ftp.otenet.gr/files/test10Mb.db"))
                .setTitle(imageData.getWallpaperName())// Title of the Download Notification
                .setDescription("Downloading")// Description of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)// Visibility of the download Notification
                .setDestinationUri(Uri.fromFile(directory))// Uri of the destination file
                // Set if charging is required to begin the download
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true);// Set if download is allowed on roaming network
        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadID = downloadManager.enqueue(request);// enqueue puts the download request in the queue.
    }

    public void rateApp() {
        try {
            Intent rateIntent = rateIntentForUrl("market://details");
            startActivity(rateIntent);
        } catch (ActivityNotFoundException e) {
            Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
            startActivity(rateIntent);
        }
    }

    private Intent rateIntentForUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        } else {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }



    private class DownloadFile extends AsyncTask<String, String, String> {
        int count;
        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(FullWallpaperActivity.this);
            this.progressDialog.setMax(100);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setMessage("Please Wait Wallpaper Downloading");
            this.progressDialog.setCancelable(false);
//            this.progressDialog.setMax(100);
            this.progressDialog.show();

        }

        @Override
        protected String doInBackground(String... f_url) {

            try {
//                URL url = new URL(f_url[0]);

                URL url = new URL(f_url[0]);
//                URLConnection urlConnection=url.openConnection();
//                urlConnection.connect();
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestProperty("Accept-Encoding", "identity");
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                int lengthOfFile = c.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
//                fileName = timestamp + "_" + fileName;

                //External directory path to save file
                folder = Environment.getExternalStorageDirectory() + File.separator + "PiczallVideo/";

                //Create androiddeft folder if it does not exist
                File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }
                OutputStream output = new FileOutputStream(folder + imageData.getWallpaperName() + ".mp4");
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    Log.e("Progress: ", String.valueOf((int) ((total * 100) / lengthOfFile)));
                    this.progressDialog.setProgress((int) ((total * 100) / lengthOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + folder + imageData.getWallpaperName();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());

            }

            return null;
        }


        /**
         * Updating progress bar
         */
        @Override
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            this.progressDialog.setProgress(Integer.parseInt(progress[0]));
            Log.e("value of progresss", String.valueOf(progress));
        }

        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();
            setThisWallpaper();
            // Display File path after downloading
            Toast.makeText(getApplicationContext(),
                    message, Toast.LENGTH_LONG).show();
        }
    }
    public void call_api(final String urL ,final Float rating) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Request.Method.GET, urL+String.valueOf(rating), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if (status.equals("Successful")) {
                        Toast.makeText(context, "Rated", Toast.LENGTH_SHORT).show();
                        ratingBars.setRating(rating);
                        ratingTextView.setText(String.valueOf(rating));

                    } else {
                    }
                } catch (Exception e) {

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("error:", "Error-------" + volleyError.toString());

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
//            Map<String, String> params = new HashMap<String, String>();
                return null;
            }


        };

        requestQueue.add(postRequest);
    }
    private void loadRewardedVideoAd() {
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.loadAd("ca-app-pub-6801520769496471/7520754761",
                new AdRequest.Builder().build());
        mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {

            }

            @Override
            public void onRewardedVideoAdOpened() {

            }

            @Override
            public void onRewardedVideoStarted() {

            }

            @Override
            public void onRewardedVideoAdClosed() {

            }

            @Override
            public void onRewarded(com.google.android.gms.ads.reward.RewardItem rewardItem) {

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {

            }

            @Override
            public void onRewardedVideoCompleted() {
                WebServices webServices=new WebServices(context);
                webServices.call_api(Paths.download_wallpaper+String.valueOf(imageData.getId()), Request.Method.GET,"",null);
                new DownloadFile().execute(Paths._video + imageData.getWallpaper());
            }
        });
    }

    private VideoWallpaper getVideoWallpaerper() {
        mService.getVideoDataall().enqueue(new Callback<VideoWallpaperModel>() {
            @Override
            public void onResponse(Call<VideoWallpaperModel> call, retrofit2.Response<VideoWallpaperModel> response) {
                if (response.isSuccessful()) {
                    VideoWallpaperModel model = response.body();
                    List<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper> videoWallpaperList = model.getVideoWallpaper();


                    Log.d("chkresponse", "success" + response.body());
                    initAll(videoWallpaperList);
                } else {
                    Log.d("chkresponse", "error:" + response.code());
                }
            }

            @Override
            public void onFailure(Call<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel> call, Throwable t) {
                Log.d("chkresponse", "failed:" + t.getMessage());
            }
        });
        return null;
    }


    private void initAll(List<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper> videoWallpapers) {
      listOfTopImages=new ArrayList<>();
      listOfTopImages.add(imageData);
        viewPager.setPageMargin(10);
        videoAdapter=new VideoAdapter(this,listOfTopImages);
        viewPager.setAdapter(videoAdapter);
        viewPager.setOnPageChangeListener(this);

//        Collections.reverse(videoWallpapers);

        for (com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper v : videoWallpapers) {
            ImageData imageData = new ImageData();
            imageData.setId(v.getId());
//            imageData.setWallpaper(v.getWallpaper());
            imageData.setWallpaperName(v.getWallpaperName());
            imageData.setCategoryId(3);
            imageData.setLikeCount(3);
            imageData.setIsLike(2);
            imageData.setVideoType(true);
            imageData.setCredits("2.0");
            imageData.setIs_premium(v.getIs_premium());


            //new
            imageData.setAverageRating(v.getAverageRating());
            imageData.setWallpaper(v.getVideo());
            imageData.setVideo_wallpaper_thumbnail(v.getVideo_wallpaper_thumbnail());

            listOfTopImages.add(imageData);
        }

        videoAdapter.notifyDataSetChanged();
    }
    @Override
    public void onPageScrolled(int i, float v, int i1) {
    }

    @Override
    public void onPageSelected(int i) {
        imageData=listOfTopImages.get(i);
        SetUpMainContent();

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

}
