package com.mobilesdev360.sigmax.piczall.adapters;

import android.app.Activity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobilesdev360.sigmax.piczall.dataClass.CategoryData;
import com.mobilesdev360.sigmax.piczall.fragment.Category;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoryListAdapter   extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {

    private List<CategoryData> list;
    Activity activity;
    Category myCategoryObject;
    public CategoryListAdapter(List<CategoryData> Data, Activity activity, Category category) {
        list = Data;
        this.activity=activity;
        myCategoryObject=category;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Picasso.get()
                .load(Paths._thumbnail_cat + list.get(position).getImage())
                .resize(300,300)
                .error(R.mipmap.category_ph).placeholder(R.mipmap.category_ph).into(holder.imgCategory);
        Log.e("cate_image path", Paths._thumbnail_cat+list.get(position).getImage());
        holder.txtCategoryName.setText(list.get(position).getName());
        if(list.get(position).isSelected()){
            holder.txtCategoryName.setTextColor(ContextCompat.getColor(activity, R.color.red_dark));
        }else{
            holder.txtCategoryName.setTextColor(ContextCompat.getColor(activity, R.color.white_text));
        }
        holder.liContainerCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCategoryObject.pageStart=0;
                myCategoryObject.getImagesWithCategoryID(list.get(position),myCategoryObject.pageStart,myCategoryObject.pageSize,true);
                for(int a=0;a<list.size();a++){
                    if(a==position){
                        list.get(a).setSelected(true);
                    }else{
                        list.get(a).setSelected(false);
                    }
                }
                notifyDataSetChanged();
            }
        });
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtCategoryName;
        ImageView imgCategory;
        FrameLayout liContainerCategory;


        public MyViewHolder(View v) {
            super(v);
            imgCategory = (ImageView) v.findViewById(R.id.imgCategory);
            txtCategoryName= (TextView) v.findViewById(R.id.txtCategoryName);
            liContainerCategory= (FrameLayout) v.findViewById(R.id.liContainerCategory);
        }
    }


}