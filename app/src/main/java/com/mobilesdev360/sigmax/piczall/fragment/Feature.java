package com.mobilesdev360.sigmax.piczall.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.activities.Splash;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.adapters.AllPopularImagesAdapter;
import com.mobilesdev360.sigmax.piczall.adapters.PopularHorizontalAdapter;
import com.mobilesdev360.sigmax.piczall.adapters.TrendingViewAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.dataClass.TrendingImageData;
import com.mobilesdev360.sigmax.piczall.remote.RetrofitProvider;
import com.mobilesdev360.sigmax.piczall.remote.SOService;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;
import com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Feature extends Fragment implements WebServices.RequestResults {
    private static final String TAG = "Feature";


    LinearLayoutManager layoutManager ;
    RecyclerView recyclerView;
    View rootView;
    List<ImageData> listOfTopImages;
    List<TrendingImageData> listOfTopImages2;
    GridViewWithHeaderAndFooter gridView;
    AllPopularImagesAdapter mainAdapter;
    View loadMoreView;
    int pageStart = 0, pageSize = 10;
    boolean canScroll, IsMoreScroll, IsFirstCall = true;
    int lastLastitem = 0;
    int RequestCode = 1122;
    ProgressDialog pDialog;
    TextView txtNoDataFound;
    SwipeRefreshLayout pullToRefresh;
    List<ImageData> listOfVideoImages;
    PopularHorizontalAdapter videoAdapter;
    RecyclerView videoImageRecyclerView;
    View headerView;
    private SOService mService;


    private TrendingViewAdapter mExampleAdapter;
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mCatIds = new ArrayList<>();
    private RequestQueue mRequestQueue;

    public Feature() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_feature, container, false);
        Initialize(inflater);
        return rootView;
    }

    void Initialize(LayoutInflater inflater) {
        listOfTopImages = new ArrayList();
        gridView = rootView.findViewById(R.id.gridview);
        txtNoDataFound = rootView.findViewById(R.id.txtNoDataFound);
        headerView = inflater.inflate(R.layout.gridview_header, null);
        videoImageRecyclerView = headerView.findViewById(R.id.cardView);
        videoImageRecyclerView.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL);

        videoImageRecyclerView.setLayoutManager(layoutManager);
        gridView.addHeaderView(headerView);
        TextView txtTop = rootView.findViewById(R.id.top_text);
        TextView txtBottom = headerView.findViewById(R.id.txtBottom);
        txtTop.setText("Trending");
        txtBottom.setText("Featured");
       // txtTop.setVisibility(View.VISIBLE);


        pDialog = new ProgressDialog(getActivity());
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

        pullToRefresh = rootView.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageStart = 0;
                IsFirstCall = true;
                pDialog.show();
                pageSize = 20;
                lastLastitem = 0;
                canScroll = true;
                IsMoreScroll = true;
                lastLastitem = 0;
//
//                if(Settings.userObj==null) {
//                    RequestCode=1122;
//                    String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize;;
//                    WebServices.getInstance().MyGetCallWithoutAuth(getActivity(), url, Feature.this);
//                }else{
//                    RequestCode=1133;
//                    String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize+"&user_id="+Settings.userObj.getId();;
//                    WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, Feature.this);
//                }

                getVideoWallpapers();
                pullToRefresh.setRefreshing(false);
            }
        });
        getVideoWallpapers();
        getImages();

    }

    void getVideoWallpapers() {
        mService = RetrofitProvider.getClient().create(SOService.class);
        getVideoWallpaerper();
    }

    private VideoWallpaper getVideoWallpaerper() {
        mService.getVideoData().enqueue(new Callback<VideoWallpaperModel>() {
            @Override
            public void onResponse(Call<VideoWallpaperModel> call, Response<VideoWallpaperModel> response) {
                if (response.isSuccessful()) {
                    VideoWallpaperModel model = response.body();
                    List<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper> videoWallpaperList = model.getVideoWallpaper();
                //    initAll(videoWallpaperList);
                    getFeature();
                } else {
                }
            }

            @Override
            public void onFailure(Call<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaperModel> call, Throwable t) {
            }
        });
        return null;
    }

   /*  private void initAll(List<com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper> videoWallpapers) {

       listOfVideoImages = new ArrayList();

//        Collections.reverse(videoWallpapers);

        for (com.mobilesdev360.sigmax.piczall.utils.model.VideoWallpaper v : videoWallpapers) {
            ImageData imageData = new ImageData();
            imageData.setId(v.getId());
//            imageData.setWallpaper(v.getWallpaper());
            imageData.setWallpaperName(v.getWallpaperName());
            imageData.setCategoryId(3);
            imageData.setLikeCount(3);
            imageData.setIsLike(1);
            imageData.setVideoType(true);
            imageData.setCredits("2.0");
            imageData.setAverageRating(v.getAverageRating());


            //new
            imageData.setWallpaper(v.getVideo());
            imageData.setIs_premium(v.getIs_premium());

            imageData.setVideo_wallpaper_thumbnail(v.getVideo_wallpaper_thumbnail());
            listOfVideoImages.add(imageData);
        }
        videoAdapter = new PopularHorizontalAdapter(listOfVideoImages, getActivity());
        videoImageRecyclerView.setAdapter(videoAdapter);

    }
*/

    void getFeature() {
        pageStart = 0;
        IsFirstCall = true;
        // pDialog.show();
        pageStart = 0;
        lastLastitem = 0;
        canScroll = true;
        IsMoreScroll = true;
        lastLastitem = 0;

        if (Splash.userid.equals("")) {
            RequestCode = 1122;
//            String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize;
            String url = Paths._featured_wallpapers + "/" + pageStart + "/" + pageSize;
            WebServices.getInstance().MyGetCallWithoutAuth(getActivity(), url, Feature.this);
        } else {
            RequestCode = 1133;
//
//           String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize+"&user_id="+Settings.userObj.getId();;
//            String url = Paths._today_wallpapers + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize+"&user_id="+Settings.userObj.getId();;
//            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, Feature.this);4
//            String url = Paths.get_like_wallpaper_with_userid_or_all+Splash.userid + "/" + pageStart + "/" + pageSize;
            String url = Paths._featured_wallpapers + "/" + pageStart + "/" + pageSize;
            Log.d(TAG, "features: " + url);
            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, Feature.this);
        }

    }

    @Override
    public void requestFailed(VolleyError error) {
        pDialog.cancel();
//        VolleyError myError=error;
//        if(error!=null){
//            if(error.getMessage()!=null && !error.getMessage().equals(""))
//                Settings.ShowErrorDialog(getActivity(),0,null,error.getMessage());
//            else
//                Settings.ShowErrorDialog(getActivity(),0,null,"Internet Or Server Error.");
//        }else {
//            Settings.ShowErrorDialog(getActivity(),0,null,"Internet Or Server Error.");
//        }
    }

    @Override
    public void requestSucceeded(JSONObject response) {

        pDialog.cancel();
        canScroll = true;
        try {
            if (response.getString("status").equals("Successful")) {
                Gson gson = new GsonBuilder().create();
//                wallpaper
                final ImageData[] allImages = gson.fromJson(response.getJSONArray("featured_wallpaper").toString(), ImageData[].class);
                listOfTopImages = new ArrayList<>();
                for (int a = 0; a < allImages.length; a++) {
                    listOfTopImages.add(allImages[a]);
                }
                if (IsFirstCall == true && listOfTopImages.size() > 0) {
                    txtNoDataFound.setVisibility(View.GONE);
                    IsFirstCall = false;
                    if (listOfTopImages.size() < pageSize) {
                        IsMoreScroll = false;
                    } else {

                    }
                    mainAdapter = new AllPopularImagesAdapter(getActivity(), listOfTopImages);
                    gridView.setAdapter(mainAdapter);
                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getActivity(), ViewImageScreen.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            intent.putExtra("SelectedImage", mainAdapter.getItem(position));
                            Popular.liked = "1";
                            Log.w("ADS_TESTING", "Item click");

                            startActivity(intent);
                        }
                    });
                    gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {
                        }

                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            if (canScroll) {
                                final int lastItem = firstVisibleItem + visibleItemCount;
                                if (lastItem == totalItemCount && IsMoreScroll) {
                                    if (lastLastitem != lastItem) {
                                        lastLastitem = lastItem;
                                        pageStart = pageStart + pageSize;
                                        canScroll = false;
//                                        pDialog.show();
                                        if (RequestCode == 1122) {
                                            String url = Paths._featured_wallpapers + "/" + pageStart + "/" + pageSize;
//                                            String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStart+"&page_size="+pageSize;
                                            WebServices.getInstance().MyGetCallWithoutAuth(getActivity(), url, Feature.this);
                                        } else if (RequestCode == 1133) {
                                            String url = Paths._featured_wallpapers + "/" + pageStart + "/" + pageSize;
//                                            String url = Paths.get_like_wallpaper_with_userid_or_all+Splash.userid + "/" + pageStart + "/" + pageSize;

                                            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, Feature.this);
                                        }
                                    }
                                }
                            }

                        }
                    });
                } else if (IsFirstCall && listOfTopImages.size() == 0) {
                    gridView.setAdapter(null);
                    txtNoDataFound.setVisibility(View.VISIBLE);
                } else {
                    if (listOfTopImages.size() < pageSize) {
                        IsMoreScroll = false;
                        mainAdapter.AddMyData(listOfTopImages);
                    } else {
                        mainAdapter.AddMyData(listOfTopImages);
                    }

                }

            } else {
                Gson gsonTemp = new GsonBuilder().create();
                Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
                Settings.ShowErrorDialog(getActivity(), 1, error, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // pDialog.cancel();
        }
    }


    private void getImages(){


        mRequestQueue = Volley.newRequestQueue(getActivity());
        String url = Paths._get_trending+"/0/10";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new com.android.volley.Response.Listener<JSONObject>(){

                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("wallpaper2");
                     for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject hit = jsonArray.getJSONObject(i);
                                String creatorName = hit.getString("folder_name");
                                String imageUrl = hit.getString("folder_thumbnail");
                                String id = hit.getString("id");
                                mImageUrls.add(Paths._trending_thumb_folder+imageUrl);
                                mNames.add(creatorName);
                                mCatIds.add(id);
                            }
                            initRecyclerView();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mRequestQueue.add(request);



    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: init recyclerview");

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerViewTrending);
        recyclerView.setLayoutManager(layoutManager);
        TrendingViewAdapter adapter = new TrendingViewAdapter(getActivity(), mNames, mImageUrls,mCatIds);
        recyclerView.setAdapter(adapter);
    }


}
