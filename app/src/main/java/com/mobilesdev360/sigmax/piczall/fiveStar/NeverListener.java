package com.mobilesdev360.sigmax.piczall.fiveStar;

/**
 * Created by angtrim on 14/11/15.
 */
public interface NeverListener {
    void onNever(int stars);
}
