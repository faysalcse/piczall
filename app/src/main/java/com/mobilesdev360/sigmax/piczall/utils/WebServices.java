package com.mobilesdev360.sigmax.piczall.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class WebServices {

    private static WebServices instance = null;
    public RequestResults myCallback;

    public WebServices() {
    }


    public static WebServices getInstance() {
        if (instance == null) {
            return instance = new WebServices();
        } else {
            return instance;
        }
    }

    Context context;

    public WebServices(Context context) {
        this.context=context;
    }

    public void MyGetCallWithoutAuth(Context context, String url, final RequestResults myCallback) {
        this.myCallback = myCallback;
        String tag_json_obj = "GET_REQUEST";
        url = url.replaceAll("\\s+", "%20");
        Log.d("urlss", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        myCallback.requestSucceeded(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myCallback.requestFailed(error);

                    }
                }
        ) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }

        };
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(getRequest, tag_json_obj);

    }

    public void MyPostRequestWithoutAuth(Context context, String parseUrl, Map<String, Object> params, final RequestResults myCallback) {
        this.myCallback = myCallback;
        String tag_json_obj = "json_obj_req";

        String url = parseUrl;
        url = url.replaceAll("\\s+", "%20");
        JSONObject obj = new JSONObject(params);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, obj,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        myCallback.requestSucceeded(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                myCallback.requestFailed(error);

            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    public void MyGetCallWithAuth(Context context, String url, final RequestResults myCallback) {
        this.myCallback = myCallback;
        String tag_json_obj = "GET_REQUEST";
        url = url.replaceAll("\\s+", "%20");
        Log.d("urlss", url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        myCallback.requestSucceeded(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        myCallback.requestFailed(error);
                        Log.i("", "Error: " + error.getMessage());
                    }
                }
        ) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                if (Settings.userObj.getToken() != null) {
                    headers.put("Authorization", "Bearer " + Settings.userObj.getToken());
                    headers.put("Accept", "application/json");
                }

                return headers;
            }

        };
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(getRequest, tag_json_obj);
    }

    public void MyPostRequestWithAuth(Context context, String parseUrl, Map<String, Object> params, final RequestResults myCallback) {
        this.myCallback = myCallback;
        String tag_json_obj = "json_obj_req";

        String url = parseUrl;
        url = url.replaceAll("\\s+", "%20");
        JSONObject obj = new JSONObject(params);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, obj,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        myCallback.requestSucceeded(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                myCallback.requestFailed(error);

            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                if (Settings.userObj.getToken() != null) {
                    headers.put("Authorization", "Bearer " + Settings.userObj.getToken());
                    headers.put("Accept", "application/json");
                }
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }


    public interface RequestResults {

        public void requestFailed(VolleyError error);

        public void requestSucceeded(JSONObject response);

    }
    public void call_api(final String urL, final int Method, final String view, final Map<String,String> params)
    {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Method, urL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response",response);
                try {
                    JSONObject jsonObject =new JSONObject(response);
                    String status=jsonObject.getString("status");
                    if(status.equals("Successful"))
                    {
                        //do  what you want
                    }
                    else
                    {
                        //do  what you want..
                    }
                }
                catch (Exception e)
                {
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("error:","Error-------"+ volleyError.toString());

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
//            Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        requestQueue.add(postRequest);}


}
