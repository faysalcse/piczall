package com.mobilesdev360.sigmax.piczall.dataClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("reset_password_token")
    @Expose
    private Object resetPasswordToken;
    @SerializedName("account_verification_token")
    @Expose
    private String accountVerificationToken;
    @SerializedName("account_status")
    @Expose
    private int accountStatus;
    @SerializedName("fb_profile_id")
    @Expose
    private String fbProfileId;
    @SerializedName("google_profile_id")
    @Expose
    private String googleProfileId;
    @SerializedName("social_id")
    @Expose
    private String socialId;
    @SerializedName("api_token")
    @Expose
    private String apiToken;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("image_name")
    @Expose
    private String image_name;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setResetPasswordToken(Object resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public void setAccountVerificationToken(String accountVerificationToken) {
        this.accountVerificationToken = accountVerificationToken;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

    public void setFbProfileId(String fbProfileId) {
        this.fbProfileId = fbProfileId;
    }

    public void setGoogleProfileId(String googleProfileId) {
        this.googleProfileId = googleProfileId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Object getResetPasswordToken() {
        return resetPasswordToken;
    }

    public String getAccountVerificationToken() {
        return accountVerificationToken;
    }

    public int getAccountStatus() {
        return accountStatus;
    }

    public String getFbProfileId() {
        return fbProfileId;
    }

    public String getGoogleProfileId() {
        return googleProfileId;
    }

    public String getSocialId() {
        return socialId;
    }

    public String getApiToken() {
        return apiToken;
    }

    public String getToken() {
        return token;
    }

    public String getImage_name() {
        return image_name;
    }
}

