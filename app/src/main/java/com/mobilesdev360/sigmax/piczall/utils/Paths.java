package com.mobilesdev360.sigmax.piczall.utils;

public class Paths {

    public static String _BaseUrl = "http://www.piczall.online/admin/";
    public static String _BaseUrl2 = "http://www.piczall.online/sys/";
    public static String download_wallpaper=_BaseUrl+"download_wallpaper";
    public static String token_insert_update=_BaseUrl+"token_insert_update/";
    public static String _getcategories = _BaseUrl + "api/getcategories";
    public static String _get_trending = _BaseUrl + "api/trending";
    public static String _get_trending_pics = _BaseUrl + "api/trending_pics/";
    public static String _trending_thumb_folder = _BaseUrl2 ;
    public static String _trending_media = _BaseUrl2 +"media/images/";
    public static String searchwallpaper = _BaseUrl + "api/searchwallpaper/";
    public static String get_like_wallpaper_with_userid_or_all=_BaseUrl+"api/get_like_wallpaper_with_userid_or_all/";
    public static String _like_wallpaper = _BaseUrl + "api/like_wallpaper";
    public static String _wallpaper_by_category_id = _BaseUrl + "api/wallpaper_by_category_id/";
    public static String _wallpaper_by_category_id_by_userid = _BaseUrl + "api/wallpaper_by_category_id_by_userid/";
    public static String _user_login = _BaseUrl + "api/user_login";
    public static String make_like_wallpaper = _BaseUrl + "api/make_like_wallpaper";
    public static String _today_wallpapers = _BaseUrl + "api/today_wallpapers";
    public static String _featured_wallpapers = _BaseUrl + "api/featured_wallpapers";
    public static String _featured_videos_wallpapers = _BaseUrl + "api/featured_videos_wallpapers";
    public static String _thumbnail_path_video = _BaseUrl + "uploads/media/videos/images/thumbnail/";
    public static String _thumbnail_path_wallpaper = _BaseUrl + "uploads/media/images/thumbnail/";
    public static String _wallpapers_images = _BaseUrl + "uploads/media/images/";
    public static String _trending_images = _BaseUrl + "sys/media/images/";
    public static String _video = _BaseUrl + "uploads/media/videos/";
    public static String _thumbnail_cat = _BaseUrl + "uploads/media_category/thumbnail/";
    public static String make_rating_wallpaper = _BaseUrl + "api/make_rating_wallpaper/";
    public static String remove_unlike_wallpaper = _BaseUrl + "api/make_unlike_wallpaper/";
//    http://piczall.online/admin/api/make_rating_wallpaper/8/9/4


//    public static String BaseUrl="http://melinsight.com/livewallpaper/public/index.php/api/";
//    public static String OrignalImage="http://melinsight.com/livewallpaper/public/image/";
//    public static String thumbnailImage="http://melinsight.com/livewallpaper/public/tmpuploads/";

//    public static String BaseUrl="http://192.168.0.104/livewallpaper/public/index.php/api/";
//    public static String OrignalImage="http://192.168.0.104/livewallpaper/public/image/";
//    public static String thumbnailImage="http://192.168.0.104/livewallpaper/public/tmpuploads/";
//    public static String videopath="http://192.168.0.104/livewallpaper/public/video/";


//    public static String BaseUrl="http://melinsight.com/livewallpaper/public/index.php/api/";
//    public static String OrignalImage="http://melinsight.com/livewallpaper/public/image/";
//    public static String thumbnailImage="http://melinsight.com/livewallpaper/public/tmpuploads/";
//    public static String videopath="http://melinsight.com/livewallpaper/public/video/";

    public static String BaseUrl = "http://piczall.com/livewallpaper/public/index.php/api/";
    public static String OrignalImage = "http://piczall.com/livewallpaper/public/image/";
    public static String thumbnailImage = "http://piczall.com/livewallpaper/public/tmpuploads/";
    public static String videopath = "";

    //http://piczall.com/piczall-server/public/index.php/api/wallpaper/index?category_id=2&user_id=1&search=testt
    public static String imagesPathLocal = "Piczall";
    public static String imagesPathLocalForShare = "PiczallShare";



}
