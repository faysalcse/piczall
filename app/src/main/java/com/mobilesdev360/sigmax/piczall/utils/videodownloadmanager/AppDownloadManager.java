package com.mobilesdev360.sigmax.piczall.utils.videodownloadmanager;
/*
DownloadManager manager = (android.app.DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
@Param DownloadManager manager  should inititate with above method from activity to call download method.
 */

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;

import com.mobilesdev360.sigmax.piczall.R;

public class AppDownloadManager {

    public static long download(String downloadUrl, String title, String filename, Context context)
    {

        Uri uri = Uri.parse(downloadUrl);
        android.app.DownloadManager.Request request = new android.app.DownloadManager.Request(uri);
        request.setTitle(title);

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(android.app.DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(context.getApplicationContext().getString(R.string.app_name), filename);

        DownloadManager manager= (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        long id=manager.enqueue(request);

        return id;

    }

}
