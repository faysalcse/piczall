package com.mobilesdev360.sigmax.piczall.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.dataClass.TrendingImageData;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.squareup.picasso.Picasso;

public class ViewTrendingImageFullScreen extends Activity {
    ImageView imgMain,imgcross;
    TrendingImageData imageData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image_full_screen);

        imageData=(TrendingImageData) getIntent().getSerializableExtra("ImageSeletedForMain");
        imgMain=(ImageView) findViewById(R.id.imgMain);
        imgcross=(ImageView) findViewById(R.id.imgcross);
        if(imageData!=null){
            Picasso.get()
                    .load(Paths._trending_media + imageData.getWallpaper()).placeholder(R.drawable.itembg).error(R.drawable.itembg)
                    .fit()
                    .into(imgMain);
        }
        imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        initialInMobiBannerAd();
    }

    private void initialInMobiBannerAd() {
        AdView adView = findViewById(R.id.adView);
        adView.loadAd(new AdRequest.Builder().build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
