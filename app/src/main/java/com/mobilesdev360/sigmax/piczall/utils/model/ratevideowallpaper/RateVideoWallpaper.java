
package com.mobilesdev360.sigmax.piczall.utils.model.ratevideowallpaper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RateVideoWallpaper {

    @SerializedName("user_rate")
    @Expose
    private UserRate userRate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;

    public UserRate getUserRate() {
        return userRate;
    }

    public void setUserRate(UserRate userRate) {
        this.userRate = userRate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "RateVideoWallpaper{" +
                "userRate=" + userRate +
                ", status='" + status + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}
