package com.mobilesdev360.sigmax.piczall.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobilesdev360.sigmax.piczall.dataClass.DrawerItem;
import com.mobilesdev360.sigmax.piczall.R;

import java.util.List;

public class DrawerAdapter  extends ArrayAdapter<DrawerItem> {

    Context context;
    List<DrawerItem> listData;
    public DrawerAdapter(Context contexts, List<DrawerItem> list) {
        super(contexts, R.layout.menu_list_item,list);
        context=contexts;
        listData=list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.menu_list_item, parent, false);
            holder = new ViewHolderItem();
            holder.menuItemName = (TextView) convertView.findViewById(R.id.menuItemName);
            holder.menuImage = (ImageView) convertView.findViewById(R.id.menuImage);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderItem) convertView.getTag();
        }
        holder.menuItemName.setText(listData.get(position).getText());
        holder.menuImage.setImageResource(listData.get(position).getImage());

        return convertView;
    }

    static class ViewHolderItem  {
        public TextView menuItemName;
        public ImageView menuImage;


    }


    public  void AddMyData(List<DrawerItem> list){
        for(int a=0;a<list.size();a++){
            listData.add(list.get(a));
        }
        notifyDataSetChanged();
    }





    public interface OnItemSelectedListener {
        void onItemSelected(int position);
    }
}