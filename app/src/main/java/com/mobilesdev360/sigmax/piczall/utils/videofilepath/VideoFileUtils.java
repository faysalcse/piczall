package com.mobilesdev360.sigmax.piczall.utils.videofilepath;

import android.content.Context;
import android.os.Environment;

import com.mobilesdev360.sigmax.piczall.R;

import java.io.File;

public class VideoFileUtils {
    public static String GetLocalFilePathByName(Context context,String filename)
    {
        File file = Environment.getExternalStoragePublicDirectory(context.getApplicationContext().getString(R.string.app_name));
        if(!file.exists())
        {
            file.mkdir();
        }
        File file1=new File(file.getAbsoluteFile()+File.separator+filename);
        return file1.getAbsoluteFile().getAbsolutePath();
    }
    public static File GetLocalFileByName(Context context,String filename)
    {
        File file = Environment.getExternalStoragePublicDirectory(context.getApplicationContext().getString(R.string.app_name));
        if(!file.exists())
        {
            file.mkdir();
        }
        File file1=new File(file.getAbsoluteFile()+File.separator+filename);
        return file1;
    }
    public static File GetLocalFileByNames(Context context,String filename)
    {
        File existsfile= new File(Environment.getExternalStorageDirectory()+"/PiczallVideo/"+filename+".mp4");

        return existsfile;
    }
}
