package com.mobilesdev360.sigmax.piczall.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.makeramen.roundedimageview.RoundedImageView;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.dataClass.TrendingImageData;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class AllTrendingImagesAdapter extends BaseAdapter {
    private Context mContext;
    List<TrendingImageData> listData;
    private MainAdapterItemClickListener listener;
    private ImageLoader imageLoader;
//    private InterstitialAd interstitialAd;
    private InterstitialAd mInterstitialAd;
    // Constructor
    public AllTrendingImagesAdapter(Context c, List<TrendingImageData> list) {
        mContext = c;
        listData=list;
        imageLoader=ImageLoader.getInstance();
//        interstitialAd = new InterstitialAd(mContext, "2582311578562048_2586406174819255");
//        loadFacebookInterstitialAd();
        loadAd();
    }

    public AllTrendingImagesAdapter(Context c, List<TrendingImageData> list, MainAdapterItemClickListener listener) {
        mContext = c;
        listData=list;
        this.listener=listener;
        imageLoader=ImageLoader.getInstance();
//        interstitialAd = new InterstitialAd(mContext, "2582311578562048_2586406174819255");
//        loadFacebookInterstitialAd();
        loadAd();
    }

    private void loadAd() {
        mInterstitialAd = new InterstitialAd(mContext);
        mInterstitialAd.setAdUnitId(mContext.getString(R.string.interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });


    }


    public int getCount() {
        return listData.size();
    }

    public TrendingImageData getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    // create a new ImageView for each item referenced by the adapters
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolderItem holder;
        if (convertView == null) {
            LayoutInflater inflater=LayoutInflater.from(mContext);
            convertView= inflater.inflate(R.layout.main_image_item, parent, false);
            holder = new ViewHolderItem();

            holder.imgMain = convertView.findViewById(R.id.imgMain);
            holder.gifImageView =  convertView.findViewById(R.id.progress_bar);
            holder.imgStatus= convertView.findViewById(R.id.imgStatus);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderItem) convertView.getTag();
        }
        holder.gifImageView.setVisibility(View.VISIBLE);
        if(listData.get(position).isVideoType()) {
            holder.imgStatus.setVisibility(View.VISIBLE);
            if(listData.get(position).getIs_premium()!=null&&listData.get(position).getIs_premium().equals("1")){
                holder.imgStatus.setImageResource(R.drawable.ic_baseline_lock_24 );
            }else {
                holder.imgStatus.setImageResource(R.mipmap.video);
            }


            Transformation transformation = new RoundedTransformationBuilder()
                    .borderColor(Color.TRANSPARENT)
                    .borderWidthDp(0)
                    .cornerRadiusDp(6)
                    .oval(false)
                    .build();
            Picasso.get()
                    .load(Paths._thumbnail_path_video + listData.get(position).getVideo_wallpaper_thumbnail())
                    .resize(Settings.width,Settings.height)
                    .transform(transformation)
                    .error(R.drawable.itembg).placeholder(R.drawable.itembg).into(holder.imgMain, new Callback() {
                @Override
                public void onSuccess() {
                    holder.gifImageView.setVisibility(View.GONE);
                    Log.e("LOEDED",listData.get(position).getVideo_wallpaper_thumbnail());
                }

                @Override
                public void onError(Exception e) {
                }
            });

            if(listener!=null) {
                holder.imgMain.setOnClickListener(v -> {
                    Log.w("ADS_TESTING", "click 1");
                    ImageView imageView=holder.imgMain;
                    if(listData.get(position).getIs_premium().equals("1")){
                        listener.onItemClick(imageView, position);
                    } else {
                        listener.onItemClick(imageView, position);
                    }
                });
            }
        }
        else {
         holder.imgStatus.setVisibility(View.GONE);
            Picasso.get()
                    .load(Paths._trending_media + listData.get(position).getWallpaper_thumbnail())
                    .resize(Settings.width,Settings.height)
                    .error(R.drawable.itembg).placeholder(R.drawable.itembg).into(holder.imgMain, new Callback() {
                @Override
                public void onSuccess() {
                    holder.gifImageView.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {

                }
            });
            Log.d("chkpic", "path:" + Paths._trending_media + listData.get(position).getWallpaper_thumbnail());

            if (listener != null) {
                holder.imgMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick((ImageView) v, position);
                    }
                });
            }
        }
        return convertView;
    }

    static class ViewHolderItem  {

        public RoundedImageView imgMain;
        public ImageView imgStatus;
        public ProgressBar gifImageView;
    }
    public  void AddMyData(List<TrendingImageData> myListMew){
        for(int a=0;a<myListMew.size();a++){
            listData.add(myListMew.get(a));
        }
        notifyDataSetChanged();
    }
    public interface MainAdapterItemClickListener{
        public void onItemClick(ImageView imageView, int position);
    }
}