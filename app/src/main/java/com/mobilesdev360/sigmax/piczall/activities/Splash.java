package com.mobilesdev360.sigmax.piczall.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.notificationutils.Config;

import com.mobilesdev360.sigmax.piczall.utils.preferenceutil.SharedPref;

public class Splash extends AppCompatActivity {
    public static String userid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        SharedPref.init(this);
        displayFirebaseRegId();

//        AudienceNetworkAds.initialize(this);
        userid =SharedPref.read("user_id");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                    Intent intent=new Intent(Splash.this,Main.class);
                    startActivity(intent);
                    finish();
            }
        },2000);
    }
    private void displayFirebaseRegId() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId;
        regId = pref.getString("regId", null);
        Log.e("Splash", "Firebase reg id: " + regId);
        if (!TextUtils.isEmpty(regId)) {
            System.out.println("Firebase Reg Id: " + regId);
            SharedPref.write( "Device_id", regId);
            Log.e("Device_id", regId);

        } else {
            FirebaseApp.initializeApp(getApplicationContext());
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            SharedPref.write( "Device_id", refreshedToken);
            if (!TextUtils.isEmpty(refreshedToken)) {
                System.out.println("Firebase Reg Id is not received yet! Trying Again: \n" + refreshedToken);
                SharedPref.write( "Device_id", refreshedToken);
            } else {
                System.out.println("Firebase Reg Id is not received yet! Trying Again: \n" + refreshedToken);

            }
        }
    }
}
