package com.mobilesdev360.sigmax.piczall.utils.admobutil;

import com.google.android.gms.ads.rewarded.RewardItem;

public interface AdmobListener {
    public void OnAdOpen();
    public void OnAdClosed();
    public void OnPointEarned(RewardItem rewardItem);
    public void OnAdFailed(String message);
}
