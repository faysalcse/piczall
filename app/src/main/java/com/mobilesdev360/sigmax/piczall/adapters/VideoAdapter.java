package com.mobilesdev360.sigmax.piczall.adapters;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.viewpager.widget.PagerAdapter;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.activities.VideoViews;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.PlacementId;
import com.squareup.picasso.Picasso;
import com.varunest.sparkbutton.SparkButton;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class VideoAdapter extends PagerAdapter {
    public ArrayList<ImageData> imageDataArrayList;
    public Activity context;
    public static final String TAG = "VideoAdapter";
    private LinearLayout mContainer;
    private UnifiedNativeAd nativeAd;
    private RewardedAd rewardedAd;

    public VideoAdapter(Activity context, ArrayList<ImageData> imageDataArrayList) {
        this.imageDataArrayList = imageDataArrayList;
        this.context = context;

        rewardedAd = new RewardedAd(context, context.getString(R.string.reward_video_ad));


        rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);

    }

    private RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
        @Override
        public void onRewardedAdLoaded() {
            // Ad successfully loaded.
        }

        @Override
        public void onRewardedAdFailedToLoad(int errorCode) {
            // Ad failed to load.
        }
    };

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return (view == o);
    }

    @Override
    public int getCount() {
        return imageDataArrayList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(container.getContext());
        ViewGroup viewGroup = (ViewGroup) layoutInflater.inflate(R.layout.video_pager_listitem, container, false);
        ImageView image = viewGroup.findViewById(R.id.image);
        ImageView imageView = viewGroup.findViewById(R.id.imageView);
        ImageView imgcross = viewGroup.findViewById(R.id.imgcross);
        TextView textname = viewGroup.findViewById(R.id.txtImageName);
        SparkButton imgUnclockButton = viewGroup.findViewById(R.id.img_set_video_wallpaper);

        boolean is_downloaded = false;

        View view = viewGroup.findViewById(R.id.cardMainImageContainer);
        FrameLayout frameLayout = viewGroup.findViewById(R.id.fl_adplaceholder);

        ProgressBar progressBar = viewGroup.findViewById(R.id.progress_bar);
        VideoView videoView = viewGroup.findViewById(R.id.videoView);
        textname.setText(imageDataArrayList.get(position).getWallpaperName());
        RatingBar ratingBars = viewGroup.findViewById(R.id.rating_bar_video);
        TextView ratingTextView = viewGroup.findViewById(R.id.text_view_rating_number);
        String rating = imageDataArrayList.get(position).getAverageRating();

        mContainer = viewGroup.findViewById(R.id.native_container);
        if (imageDataArrayList.get(position).isAd) {
            view.setVisibility(View.GONE);
            frameLayout.setVisibility(View.VISIBLE);
            Log.w("NATIVE_ADS", "position: " + position);
            textname.setText("ADVERTISEMENT");
            imgUnclockButton.setVisibility(View.GONE);
            loadNativeVideoAd(frameLayout);
        } else {
            view.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.GONE);
        }

        PlayerView playerView = viewGroup.findViewById(R.id.video_view);
        Picasso.get()
                .load(Paths._thumbnail_path_video + imageDataArrayList.get(position).getVideo_wallpaper_thumbnail())
                .error(R.drawable.itembg).placeholder(R.drawable.itembg).into(image);
        Picasso.get()
                .load(Paths._thumbnail_path_video + imageDataArrayList.get(position).getVideo_wallpaper_thumbnail())
                .error(R.drawable.itembg).placeholder(R.drawable.itembg).into(imageView);
        if (imageDataArrayList.get(position).isVideoType() == true) {
            File existsfile = new File(Environment.getExternalStorageDirectory() + "/PiczallVideo/" +
                    imageDataArrayList.get(position).getWallpaperName() + ".mp4");
            if (existsfile.exists()) //check the file exists and set download button action
            {
                is_downloaded = true;

                Uri uri = Uri.parse(existsfile.getAbsolutePath());

//                loadViewo(imageView, videoView, uri);

                playerView.setVisibility(View.GONE);
                try {
                    videoView.setVisibility(View.VISIBLE);
                    videoView.setVideoURI(uri);
                    videoView.start();
                    imgUnclockButton.setVisibility(View.VISIBLE);
                    imgUnclockButton.setInactiveImage(R.mipmap.video);
                    imgUnclockButton.setActiveImage(R.mipmap.video);
                    videoView.setOnErrorListener((mp, what, extra) -> {
                        File fdelete = new File(String.valueOf(uri));
                        fdelete.delete();
                        imgUnclockButton.setInactiveImage(R.mipmap.download_icon);
                        imgUnclockButton.setActiveImage(R.mipmap.download_icon);

                        return true;
                    });


                    videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {

                            mp.setLooping(true);
                            Log.d("testlog", "prepared");
                            imageView.setVisibility(View.GONE);

                        }

                    });
                } catch (Exception e) {

                }

            } else {
                is_downloaded = false;
//
                imgUnclockButton.setVisibility(View.VISIBLE);
                String url = Paths._video + imageDataArrayList.get(position).getWallpaper();
                Uri uri = Uri.parse(url);
                //        imageView.setVisibility(View.GONE);
                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
                TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
//Create the player
                ExoPlayer player = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
                playerView.setPlayer(player);
                playerView.setUseController(false);
                MediaSource mediaSource = new ExtractorMediaSource.Factory(
                        new DefaultHttpDataSourceFactory("Exoplayer-local")).
                        createMediaSource(uri);
                player.prepare(mediaSource, true, false);
                player.addListener(new ExoPlayer.EventListener() {
                    @Override
                    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                    }

                    @Override
                    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                    }

                    @Override
                    public void onLoadingChanged(boolean isLoading) {

                    }

                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                        if (playbackState == ExoPlayer.STATE_BUFFERING) {
                            progressBar.setVisibility(View.VISIBLE);
                        } else if (playbackState == ExoPlayer.STATE_READY) {

                            progressBar.setVisibility(View.GONE);
                            imageView.setVisibility(View.GONE);
                            playerView.setVisibility(View.VISIBLE);
                        } else if (playbackState == ExoPlayer.STATE_ENDED) {
                            ;
                            playerView.setVisibility(View.GONE);
                            imageView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onRepeatModeChanged(int repeatMode) {

                    }

                    @Override
                    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                    }

                    @Override
                    public void onPlayerError(ExoPlaybackException error) {

                    }

                    @Override
                    public void onPositionDiscontinuity(int reason) {

                    }

                    @Override
                    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                    }

                    @Override
                    public void onSeekProcessed() {

                    }
                });
//        Uri uri = Uri.parse(url);
                playerView.setVisibility(View.VISIBLE);
                player.setPlayWhenReady(true);
//                progressBar.setVisibility(View.VISIBLE);
                player.setRepeatMode(Player.REPEAT_MODE_ALL);
            }
        }
        container.addView(viewGroup);
        boolean finalIs_downloaded = is_downloaded;
        imgUnclockButton.setOnClickListener(v -> {
            if (!finalIs_downloaded && imageDataArrayList.get(position).getIs_premium() != null && imageDataArrayList.get(position).getIs_premium().equals("1")) {
                View dialogView = View.inflate(context, R.layout.dialog_rewared_layout, null);
                AlertDialog alertDialog = new AlertDialog.Builder(context)
                        .setView(dialogView)
                        .create();

                LinearLayout showads = dialogView.findViewById(R.id.watch_video);

                alertDialog.show();

                RewardedAdCallback adCallback = new RewardedAdCallback() {
                    @Override
                    public void onRewardedAdOpened() {
                        // Ad opened.
                    }

                    @Override
                    public void onRewardedAdClosed() {
                        // Ad closed.
                        showads.setVisibility(View.VISIBLE);
                        rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
                    }

                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem reward) {
                        alertDialog.dismiss();
                        ((VideoViews) context).setThisWallpaper(imageDataArrayList.get(position).getWallpaper(), imageDataArrayList.get(position).getWallpaperName(), finalIs_downloaded);
                    }

                    @Override
                    public void onRewardedAdFailedToShow(int errorCode) {
                        // Ad failed to display.
                       // Toast.makeText(context, "Failed to load ads", Toast.LENGTH_SHORT).show();
                        Log.e("tag", " = error code= =  " + errorCode);
                    }
                };

                showads.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "Please wait for few moments to show ads", Toast.LENGTH_SHORT).show();
                        showads.setVisibility(View.GONE);
                        rewardedAd.show(context, adCallback);
                    }
                });


            } else {
                ((VideoViews) context).setThisWallpaper(imageDataArrayList.get(position).getWallpaper(), imageDataArrayList.get(position).getWallpaperName(), finalIs_downloaded);
            }
        });
        imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((VideoViews) context).finish();
            }
        });


        return viewGroup;

    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }


    private void loadNativeVideoAd(FrameLayout frameLayout) {
        AdLoader.Builder builder = new AdLoader.Builder(context, context.getString(R.string.native_ad));

        // OnUnifiedNativeAdLoadedListener implementation.
        builder.forUnifiedNativeAd(unifiedNativeAd -> {
            // otherwise you will have a memory leak.
            if (nativeAd != null) {
                nativeAd.destroy();
            }
            nativeAd = unifiedNativeAd;
            UnifiedNativeAdView adView = (UnifiedNativeAdView) context.getLayoutInflater().inflate(R.layout.ad_unified, null);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        });

        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(false)
                .build();

        NativeAdOptions adOptions = new NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .build();

        builder.withNativeAdOptions(adOptions);

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {

            }
        }).build();

        adLoader.loadAd(new AdRequest.Builder().build());

    }

    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Set the media view.
        adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        adView.setNativeAd(nativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAd.getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.
        if (vc.hasVideoContent()) {
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    super.onVideoEnd();
                }
            });
        }
    }

}
