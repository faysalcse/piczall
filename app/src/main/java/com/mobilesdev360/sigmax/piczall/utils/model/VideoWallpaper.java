
package com.mobilesdev360.sigmax.piczall.utils.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoWallpaper {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("wallpaper")
    @Expose
    private String wallpaper;
    @SerializedName("wallpaper_name")
    @Expose
    private String wallpaperName;
    @SerializedName("credits")
    @Expose
    private String credits;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWallpaper() {
        return wallpaper;
    }

    public void setWallpaper(String wallpaper) {
        this.wallpaper = wallpaper;
    }

    public String getWallpaperName() {
        return wallpaperName;
    }

    public void setWallpaperName(String wallpaperName) {
        this.wallpaperName = wallpaperName;
    }

    public String getCredits() {
        return credits;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    @Override
    public String toString() {
        return "VideoWallpaper{" +
                "id=" + id +
                ", wallpaper='" + wallpaper + '\'' +
                ", wallpaperName='" + wallpaperName + '\'' +
                ", credits='" + credits + '\'' +
                '}';
    }


    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideo_wallpaper_thumbnail() {
        return video_wallpaper_thumbnail;
    }

    public void setVideo_wallpaper_thumbnail(String video_wallpaper_thumbnail) {
        this.video_wallpaper_thumbnail = video_wallpaper_thumbnail;
    }

    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("video_wallpaper_thumbnail")
    @Expose
    private String video_wallpaper_thumbnail;

    public String getAverageRating() {
        return AverageRating;
    }

    public void setAverageRating(String averageRating) {
        AverageRating = averageRating;
    }

    @SerializedName("AverageRating")
    @Expose
    private String AverageRating;
    public String getIs_premium() {
        return is_premium;
    }

    public void setIs_premium(String is_premium) {
        this.is_premium = is_premium;
    }

    @SerializedName("is_premium")
    @Expose
    private String is_premium;

}
