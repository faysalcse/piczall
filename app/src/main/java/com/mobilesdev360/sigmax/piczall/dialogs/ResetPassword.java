package com.mobilesdev360.sigmax.piczall.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPassword {
    ImageView imgCross;
    View view;
     AlertDialog dialog;
    TextView txtTitle;
    Activity activitys;
    Button btnOk;
    EditText etxtOldPassword,etxtPassword,etxtConfirmPassword;
    ProgressDialog pDialog;

    public  void ShowMessageDialog(final Activity activity, String Title){

        pDialog = new ProgressDialog(activity);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        LayoutInflater inflater = activity.getLayoutInflater();
        view=inflater.inflate(R.layout.reset_password, null);
        builder.setView(view);
        builder.setCancelable(true);
        dialog =builder.create();
        imgCross=(ImageView) view.findViewById(R.id.imgCross);
        txtTitle=(TextView) view.findViewById(R.id.txtTitle);
        etxtOldPassword=(EditText) view.findViewById(R.id.etxtOldPassword);
        etxtPassword=(EditText) view.findViewById(R.id.etxtPassword);
        etxtConfirmPassword=(EditText) view.findViewById(R.id.etxtConfirmPassword);
        btnOk=(Button) view.findViewById(R.id.btnOk);
        txtTitle.setText(Title);
        activitys=activity;

        imgCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dataValidation()){
                    sendRegisterCall();
                }

            }
        });
        dialog.show();

    }
    void sendRegisterCall(){
        //  UserRemeberMe forToken=new UserRemeberMe(getBaseContext());

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user_id", Settings.userObj.getId());
        params.put("current_password", etxtOldPassword.getText().toString());
        params.put("new_password", etxtPassword.getText().toString());

        String url= Paths.BaseUrl+"resetPassword";
        WebServices.getInstance().MyPostRequestWithAuth(activitys, url, params, new WebServices.RequestResults() {
            @Override
            public void requestFailed(VolleyError error) {
                pDialog.dismiss();
                VolleyError myError=error;
                if(error!=null){
                    if(error.getMessage()!=null && !error.getMessage().equals(""))
                        Settings.ShowErrorDialog(activitys,0,null,error.getMessage());
                    else
                        Settings.ShowErrorDialog(activitys,0,null,"Internet Or Server Error.");
                }else {
                    Settings.ShowErrorDialog(activitys,0,null,"Internet Or Server Error.");
                }
            }

            @Override
            public void requestSucceeded(JSONObject response) {
                pDialog.dismiss();
                try {
                    if (response.getString("status").equals("successful")) {
                        Gson gson = new GsonBuilder().create();
                        Toast.makeText(activitys,"Password Updated Successfully",Toast.LENGTH_SHORT).show();
                       dialog.dismiss();
                    }else {
                        Gson gsonTemp = new GsonBuilder().create();
                        Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
                        Settings.ShowErrorDialog(activitys,1,error,"");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    private  boolean dataValidation(){
        Boolean validData=true;
        if (TextUtils.isEmpty(etxtOldPassword.getText())) {
            etxtOldPassword.setError("Old Password can not be blank");
            validData=false;
        }
        String pass=etxtPassword.getText().toString();
        if (TextUtils.isEmpty(etxtPassword.getText())) {
            etxtPassword.setError("Password can not be blank");
            validData=false;
        }
        if (TextUtils.isEmpty(etxtConfirmPassword.getText())) {
            etxtConfirmPassword.setError("Password can not be blank");
            validData=false;
        }


        else if (!(pass.matches(".*[A-Za-z].*") && pass.matches(".*[0-9].*"))) {
            etxtPassword.setError("Password must have Alphanumeric characters");
            validData=false;
        }else if (etxtPassword.getText().length()<8) {
            etxtPassword.setError("Password should contain atleast 8 characters");
            validData=false;
        }

        if (!etxtPassword.getText().toString().equals(etxtConfirmPassword.getText().toString())) {
            etxtConfirmPassword.setError("Password did not match");
            validData=false;
        }
        return validData;
    }


}
