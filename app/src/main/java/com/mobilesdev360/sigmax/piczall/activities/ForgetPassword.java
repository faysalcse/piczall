package com.mobilesdev360.sigmax.piczall.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgetPassword extends AppCompatActivity  implements WebServices.RequestResults{
    EditText txtEmail,txtPassword;
    ProgressDialog pDialog;
    Button btnLogin;
    TextView txtSignup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        Initialize();
    }

    void Initialize(){
        txtEmail=(EditText)findViewById(R.id.txtEmail);
        txtPassword=(EditText)findViewById(R.id.txtPassword);
        btnLogin=(Button)findViewById(R.id.btnLogin);
        txtSignup=(TextView)findViewById(R.id.txtSignup);
        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtEmail.requestFocus();
                hideKeyBoard();
                pDialog.show();
                if(dataValidation()==true){
                    LoginBtnClick();
                }else{
                    pDialog.cancel();
                }
            }
        });
    }

    public void hideKeyBoard(){
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
    private  boolean dataValidation(){
        Boolean validData=true;
        if (TextUtils.isEmpty(txtEmail.getText())) {
            txtEmail.setError("Email can not be blank");
            validData=false;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText()).matches()) {
            txtEmail.setError("Invalid email");
            validData=false;
        }

        return validData;
    }
    void LoginBtnClick(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", txtEmail.getText().toString());
        String url= Paths.BaseUrl+"forgotpassword";
        WebServices.getInstance().MyPostRequestWithoutAuth(this,url,params,this);
    }

    @Override
    public void requestFailed(VolleyError error) {
        pDialog.cancel();
        VolleyError myError=error;
        if(error!=null){
            if(error.getMessage()!=null && !error.getMessage().equals(""))
                Settings.ShowErrorDialog(this,0,null,error.getMessage());
            else
                Settings.ShowErrorDialog(this,0,null,"Internet Or Server Error.");
        }else {
            Settings.ShowErrorDialog(this,0,null,"Internet Or Server Error.");
        }
    }

    @Override
    public void requestSucceeded(JSONObject response) {


        try {
            if (response.getString("status").equals("successful")) {
                Gson gson = new GsonBuilder().create();
                Intent intent=new Intent();
                setResult(RESULT_OK, intent);
                finish();
               // User userObj = gson.fromJson(response.getJSONObject("user").toString(), User.class);
//                Settings.userObj=userObj;
//                //User
//                String json = gson.toJson(userObj);
//                RememberMe rememberMe=new RememberMe(this);
//                rememberMe.AddInfo("User",json);
//                Intent intent=new Intent(this,Main.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                finish();
                pDialog.cancel();

            } else {

                pDialog.cancel();
                Gson gsonTemp = new GsonBuilder().create();
                Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
                Settings.ShowErrorDialog(this, 1, error, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            pDialog.cancel();
        }

    }



}

