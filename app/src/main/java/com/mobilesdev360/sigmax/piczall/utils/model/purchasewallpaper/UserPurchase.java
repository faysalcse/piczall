
package com.mobilesdev360.sigmax.piczall.utils.model.purchasewallpaper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserPurchase {

    @SerializedName("v_wallpaper_id")
    @Expose
    private String vWallpaperId;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getVWallpaperId() {
        return vWallpaperId;
    }

    public void setVWallpaperId(String vWallpaperId) {
        this.vWallpaperId = vWallpaperId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
