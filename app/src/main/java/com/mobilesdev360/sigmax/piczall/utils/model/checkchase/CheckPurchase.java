package com.mobilesdev360.sigmax.piczall.utils.model.checkchase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckPurchase {

@SerializedName("response")
@Expose
private String response;

public String getResponse() {
return response;
}

public void setResponse(String response) {
this.response = response;
}

    @Override
    public String toString() {
        return "CheckPurchase{" +
                "response='" + response + '\'' +
                '}';
    }
}