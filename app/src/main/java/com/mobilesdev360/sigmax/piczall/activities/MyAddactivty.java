package com.mobilesdev360.sigmax.piczall.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.mobilesdev360.sigmax.piczall.R;

public class MyAddactivty extends AppCompatActivity {
    AdView mAdView = null;
    RelativeLayout.LayoutParams params;

    protected void onPause() {
        if (mAdView != null) mAdView.pause();
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        if (mAdView != null) mAdView.resume();
    }

    protected void onDestroy() {
        if (mAdView != null) mAdView.destroy();
        super.onDestroy();
    }

    public void setUpAndLoadAd(int xmlId, int layoutId, boolean bottom) {


        int topOrBottom;

        if (xmlId != 0) setContentView(xmlId); // if you setContentView before
        mAdView = new AdView(getBaseContext());
        mAdView.setAdUnitId(getResources().getString(R.string.banner_ad_unit_id));
        mAdView.setAdSize(AdSize.BANNER);
        LinearLayout layout = (LinearLayout) findViewById(layoutId);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            if (bottom) topOrBottom = RelativeLayout.ALIGN_PARENT_BOTTOM;
            else topOrBottom = RelativeLayout.ALIGN_PARENT_TOP;
            params.addRule(topOrBottom);
            layout.addView(mAdView, params);
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(getResources().getString(R.string.banner_ad_unit_id)) // My Galaxy SIII Phone*/
                    .build();
            mAdView.loadAd(adRequest);


    }
}
