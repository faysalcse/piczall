package com.mobilesdev360.sigmax.piczall.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.SocialLoginData;
import com.mobilesdev360.sigmax.piczall.dataClass.User;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.RememberMe;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity implements WebServices.RequestResults{
    EditText txtUsername,txtEmail,txtPassword,txtConfirmPassword;
    ProgressDialog pDialog;
    Button btnLogin;
    TextView txtSignup;
    SocialLoginData socialLoginDataForGoogle;
    SocialLoginData socialLoginDataForFacebook;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        socialLoginDataForGoogle=(SocialLoginData)getIntent().getSerializableExtra("googleData");
        socialLoginDataForFacebook=(SocialLoginData)getIntent().getSerializableExtra("facebookData");

        Initialize();
    }

    void Initialize(){
        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

        txtUsername=(EditText)findViewById(R.id.txtUsername);
        txtEmail=(EditText)findViewById(R.id.txtEmail);
        txtPassword=(EditText)findViewById(R.id.txtPassword);
        txtConfirmPassword=(EditText)findViewById(R.id.txtConfirmPassword);
        txtSignup=(TextView) findViewById(R.id.txtSignup);
        btnLogin=(Button)findViewById(R.id.btnLogin);

        if(socialLoginDataForGoogle!=null){
            txtEmail.setText(socialLoginDataForGoogle.getEmail());
            txtEmail.setEnabled(false);
            txtUsername.setText(socialLoginDataForGoogle.getName());

        }else  if(socialLoginDataForFacebook!=null){
            txtEmail.setText(socialLoginDataForFacebook.getEmail());
            txtUsername.setText(socialLoginDataForFacebook.getName());

        }
        ClickListner();
    }

    void ClickListner(){

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtUsername.requestFocus();
                hideKeyBoard();
                pDialog.show();
                if(isDataValid()==true){
                    sendRegisterCall();
                }else{
                    pDialog.dismiss();
                }
            }
        });
        txtSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    boolean isDataValid(){

        Boolean validData=true;


        if (TextUtils.isEmpty(txtEmail.getText())) {
            txtEmail.setError("Email name can not be blank");
            validData=false;
        }

        if (TextUtils.isEmpty(txtUsername.getText())) {
            txtUsername.setError("Username can not be blank");
            validData=false;
        }
        if (txtUsername.getText().toString().length()<3) {
            txtUsername.setError("User Name length can't less then 3");
            validData=false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText()).matches()) {
            txtEmail.setError("Invalid email");
            validData=false;
        }
        String pass=txtPassword.getText().toString();
        if (TextUtils.isEmpty(txtPassword.getText())) {
            txtPassword.setError("Password can not be blank");
            validData=false;
        }
        else if (!(pass.matches(".*[A-Za-z].*") && pass.matches(".*[0-9].*") )) {
            txtPassword.setError("Password must have Alphanumeric characters");
            validData=false;
        }else if (txtPassword.getText().length()<8) {
            txtPassword.setError("Password should contain at least 8 characters");
            validData=false;
        }

        if (TextUtils.isEmpty(txtConfirmPassword.getText())) {
            txtConfirmPassword.setError("Confirm Password can not be blank");
            validData=false;
        }

        if (!txtPassword.getText().toString().equals(txtConfirmPassword.getText().toString())) {
            txtConfirmPassword.setError("Password did not match");
            validData=false;
        }


        return validData;

    }
    public void hideKeyBoard(){
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        }
    }

    void sendRegisterCall(){
        //  UserRemeberMe forToken=new UserRemeberMe(getBaseContext());
        RememberMe rememberMe=new RememberMe(this);
        String device=rememberMe.getInfo("Device_Notification","a");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", txtEmail.getText().toString());
        params.put("username", txtUsername.getText().toString());
        params.put("password", txtPassword.getText().toString());
        params.put("device_token", device);

        if(socialLoginDataForFacebook!=null)
            params.put("social_id", socialLoginDataForFacebook.getId());

        if(socialLoginDataForGoogle!=null) {
            params.put("social_id", socialLoginDataForGoogle.getId());
        }
        String url= Paths.BaseUrl+"register";
        WebServices.getInstance().MyPostRequestWithoutAuth(this,url,params,this);

    }


    @Override
    public void requestFailed(VolleyError error) {
        pDialog.dismiss();
        VolleyError myError=error;
        if(error!=null){
            if(error.getMessage()!=null && !error.getMessage().equals(""))
                Settings.ShowErrorDialog(this,0,null,error.getMessage());
            else
                Settings.ShowErrorDialog(this,0,null,"Internet Or Server Error.");
        }else {
            Settings.ShowErrorDialog(this,0,null,"Internet Or Server Error.");
        }
    }

    @Override
    public void requestSucceeded(JSONObject response) {
        pDialog.dismiss();
        try {
            if (response.getString("status").equals("successful")) {
                Gson gson = new GsonBuilder().create();
                User userObj = gson.fromJson(response.getJSONObject("user").toString(), User.class);
                Settings.userObj=userObj;
                //User
                String json = gson.toJson(userObj);
                RememberMe rememberMe=new RememberMe(this);
                rememberMe.AddInfo("User",json);
                Intent intent=new Intent(this,Main.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }else {
                Gson gsonTemp = new GsonBuilder().create();
                Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
                Settings.ShowErrorDialog(this,1,error,"");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}

