package com.mobilesdev360.sigmax.piczall.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobilesdev360.sigmax.piczall.R;

public class PrivacyPolicy extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        TextView txtData=(TextView)findViewById(R.id.txtData);
        ImageView imgMenu=(ImageView) findViewById(R.id.imgMenu);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtData.setText("" +
                "Privacy Policy\n" +
                "\n" +
                "Site Usage and User Tracking\n" +
                "Sigmax Applications do not collect specific data but only track information to determine which areas of our site users like and would consider visiting again. Sigmax Applications do not track what an individual user reads or views, just how well each page performs overall. This helps us to provide quality software programming services and helps ensure customer satisfaction.\n" +
                "\n" +
                "Personal Information Sharing\n" +
                "Sigmax Applications does not share or distribute any information or documents about individual users with any third party, except to comply with applicable laws or valid legal processes or to protect the personal safety of our users or the public.\n" +
                "\n" +
                "Security\n" +
                "Mostly Sigmax Applications cannot access your personal information. Only our managers, who are in charge of customer communications, are exempt from this rule.\n" +
                "\n" +
                "Changes\n" +
                "Sigmax Applications  reserves the right to change, alter, or update this Privacy Statement at any time. As a result, Sigmax Applications encourage you to review this privacy statement periodically to track changes.\n" +
                "\n" +
                "Contact Information\n" +
                "Sigmax Applications welcomes your comments and questions on this statement. Please do not hesitate to contact us via e-mail at mohtashim107@gmail.com.\n" +
                "\n" +
                "Sigmax Applications  Don’t Collect Information of Children\n" +
                "Our mobile applications comply with the Children’s Online Privacy Protection Act. Sigmax Applications  don’t knowingly collect personal information from children under the age of 13, and if in the event that a user identifies himself or herself as a child under the age of 13 through a support request, Sigmax Applications  will not collect, store or use, and will delete in a secure manner, any personal information of such user.\n" +
                "\n");

//        txtData.setText("Privacy Policy\n" +
//                "\n" +
//                "Effective date: February 28, 2019\n" +
//                "\n" +
//                "Piczall (\"us\", \"we\", or \"our\") operates the mobile application (the \"Service\").\n" +
//                "\n" +
//                "This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data. Our Privacy Policy for Piczall is created with the help of the Free Privacy Policy website.\n" +
//                "\n" +
//                "We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions.\n" +
//                "\n" +
//                "Information Collection And Use\n" +
//                "\n" +
//                "We collect several different types of information for various purposes to provide and improve our Service to you.\n" +
//                "\n" +
//                "Types of Data Collected\n" +
//                "\n" +
//                "Personal Data\n" +
//                "\n" +
//                "While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you (\"Personal Data\"). Personally identifiable information may include, but is not limited to:\n" +
//                "\n" +
//                "First name and last name\n" +
//                "\n" +
//                "Phone number\n" +
//                "\n" +
//                "Address, State, Province, ZIP/Postal code, City\n" +
//                "\n" +
//                "Cookies and Usage Data\n" +
//                "\n" +
//                "Usage Data\n" +
//                "\n" +
//                "When you access the Service by or through a mobile device, we may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data (\"Usage Data\").\n" +
//                "\n" +
//                "Tracking & Cookies Data\n" +
//                "\n" +
//                "We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.\n" +
//                "\n" +
//                "Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.\n" +
//                "\n" +
//                "You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.\n" +
//                "\n" +
//                "Examples of Cookies we use:\n" +
//                "\n" +
//                "Session Cookies. We use Session Cookies to operate our Service.\n" +
//                "\n" +
//                "Preference Cookies. We use Preference Cookies to remember your preferences and various settings.\n" +
//                "\n" +
//                "Security Cookies. We use Security Cookies for security purposes.\n" +
//                "\n" +
//                "Use of Data\n" +
//                "\n" +
//                "Piczall uses the collected data for various purposes:\n" +
//                "\n" +
//                "To provide and maintain the Service\n" +
//                "\n" +
//                "To notify you about changes to our Service\n" +
//                "\n" +
//                "To allow you to participate in interactive features of our Service when you choose to do so\n" +
//                "\n" +
//                "To provide customer care and support\n" +
//                "\n" +
//                "To provide analysis or valuable information so that we can improve the Service\n" +
//                "\n" +
//                "To monitor the usage of the Service\n" +
//                "\n" +
//                "To detect, prevent and address technical issues\n" +
//                "\n" +
//                "Transfer Of Data\n" +
//                "\n" +
//                "Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.\n" +
//                "\n" +
//                "If you are located outside Pakistan and choose to provide information to us, please note that we transfer the data, including Personal Data, to Pakistan and process it there.\n" +
//                "\n" +
//                "Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.\n" +
//                "\n" +
//                "Piczall will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.\n" +
//                "\n" +
//                "Disclosure Of Data\n" +
//                "\n" +
//                "Legal Requirements\n" +
//                "\n" +
//                "Piczall may disclose your Personal Data in the good faith belief that such action is necessary to:\n" +
//                "\n" +
//                "To comply with a legal obligation\n" +
//                "\n" +
//                "To protect and defend the rights or property of Piczall\n" +
//                "\n" +
//                "To prevent or investigate possible wrongdoing in connection with the Service\n" +
//                "\n" +
//                "To protect the personal safety of users of the Service or the public\n" +
//                "\n" +
//                "To protect against legal liability\n" +
//                "\n" +
//                "Security Of Data\n" +
//                "\n" +
//                "The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.\n" +
//                "\n" +
//                "Service Providers\n" +
//                "\n" +
//                "We may employ third party companies and individuals to facilitate our Service (\"Service Providers\"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.\n" +
//                "\n" +
//                "These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.\n" +
//                "\n" +
//                "Links To Other Sites\n" +
//                "\n" +
//                "Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.\n" +
//                "\n" +
//                "We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.\n" +
//                "\n" +
//                "Children's Privacy\n" +
//                "\n" +
//                "Our Service does not address anyone under the age of 18 (\"Children\").\n" +
//                "\n" +
//                "We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.\n" +
//                "\n" +
//                "Changes To This Privacy Policy\n" +
//                "\n" +
//                "We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.\n" +
//                "\n" +
//                "We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the \"effective date\" at the top of this Privacy Policy.\n" +
//                "\n" +
//                "You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.\n" +
//                "\n" +
//                "Contact Us\n" +
//                "\n" +
//                "If you have any questions about this Privacy Policy, please contact us:\n" +
//                "\n" +
//                "By email: piczallwallpapers@gmail.com\n" +
//                "\n" +
//                "By phone number: +923244335660\n" +
//                "\n");
    }

}
