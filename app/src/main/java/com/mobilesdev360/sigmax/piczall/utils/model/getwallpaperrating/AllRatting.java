
package com.mobilesdev360.sigmax.piczall.utils.model.getwallpaperrating;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllRatting {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("rate")
    @Expose
    private Double rate;
    @SerializedName("comment")
    @Expose
    private String comment;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
