package com.mobilesdev360.sigmax.piczall.utils.dialogutil;

import android.text.Editable;
import android.text.TextUtils;

public class Text {

    /**
     * Convenient method to save developer time
     * @param text
     * @return
     */
    public static boolean isNotEmpty(String text) {
        return !TextUtils.isEmpty(text);
    }

    /**
     * Convenient method to save developer time
     * @param text
     * @return
     */
    public static boolean isNotEmpty(CharSequence text) {
        return !TextUtils.isEmpty(text);
    }

    /**
     * Convenient method to save developer time
     * @param text
     * @return
     */
    public static boolean isNotEmpty(Editable text) {
        return !TextUtils.isEmpty(text);
    }

}
