package com.mobilesdev360.sigmax.piczall.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.adapters.AllPopularImagesAdapter;
import com.mobilesdev360.sigmax.piczall.adapters.CategoryListAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.CategoryData;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Category extends Fragment implements WebServices.RequestResults {
    public int pageStart = 0, pageSize = 10;
    View rootView;
    List<ImageData> listOfTopImages;
    CategoryListAdapter topAdapter;
    List<CategoryData> listOfCategory;
    RecyclerView recycCategory;
    boolean canScroll, IsMoreScroll, IsFirstCall = true;
    int lastLastitem = 0;
    ProgressDialog pDialog;
    TextView txtNoDataFound;
    GridViewWithHeaderAndFooter gridView;
    AllPopularImagesAdapter mainAdapter;
    CategoryData selectedCategory = null;
    //1122 get category
    //1133 get images without login
    //1144 get images with login
    int requestCode = 1122;
    TextView txtSelectedCategoryName;
    SwipeRefreshLayout pullToRefresh;

    public Category() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_category, container, false);
        Initialize();

        return rootView;
    }

    void Initialize() {
        txtNoDataFound = rootView.findViewById(R.id.txtNoDataFound);
        gridView = rootView.findViewById(R.id.gridview);
        recycCategory = rootView.findViewById(R.id.cardView);
        txtSelectedCategoryName = rootView.findViewById(R.id.txtSelectedCategoryName);
        recycCategory.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recycCategory.setLayoutManager(MyLayoutManager);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        listOfTopImages = new ArrayList();

        pullToRefresh = rootView.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestCode = 1122;
//                String url = Paths.BaseUrl+"getcategories?is_parent=1";
                String url = Paths._getcategories;
                WebServices.getInstance().MyGetCallWithoutAuth(getContext(), url, Category.this);

                pullToRefresh.setRefreshing(false);
            }
        });
        requestCode = 1122;
//        String url = Paths.BaseUrl+"getcategories?is_parent=1";
        WebServices.getInstance().MyGetCallWithoutAuth(getContext(), Paths._getcategories, this);

    }


    @Override
    public void requestFailed(VolleyError error) {
        pDialog.cancel();
//        VolleyError myError=error;
//        if(error!=null){
//            if(error.getMessage()!=null && !error.getMessage().equals(""))
//                Settings.ShowErrorDialog(getActivity(),0,null,error.getMessage());
//            else
//                Settings.ShowErrorDialog(getActivity(),0,null,"Internet Or Server Error.");
//        }else {
//            Settings.ShowErrorDialog(getActivity(),0,null,"Internet Or Server Error.");
//        }
    }

    @Override
    public void requestSucceeded(JSONObject response) {
        pDialog.cancel();
        try {
            if (response.getString("status").equals("Successful")) {
                if (requestCode == 1122) {
                    Gson gson = new GsonBuilder().create();
                    final CategoryData[] allCategory = gson.fromJson(response.getJSONArray("category").toString(), CategoryData[].class);
                    listOfCategory = new ArrayList<>();
                    listOfCategory = Arrays.asList(allCategory);
                    if (listOfCategory.size() > 0 & recycCategory != null) {
                        listOfCategory.get(0).setSelected(true);
                        topAdapter = new CategoryListAdapter(listOfCategory, getActivity(), this);
                        recycCategory.setAdapter(topAdapter);
                        selectedCategory = listOfCategory.get(0);
                        pageStart = 0;
                        getImagesWithCategoryID(selectedCategory, pageStart, pageSize, true);
                    }
                } else {
                    canScroll = true;
                    Gson gson = new GsonBuilder().create();
                    final ImageData[] allImages = gson.fromJson(response.getJSONArray("wallpaper_by_categoryId").toString(), ImageData[].class);
                    listOfTopImages = new ArrayList<>();
                    for (int a = 0; a < allImages.length; a++) {

                        listOfTopImages.add(allImages[a]);
                    }


                    if (IsFirstCall == true && listOfTopImages.size() > 0) {
                        txtNoDataFound.setVisibility(View.GONE);
                        IsFirstCall = false;
                        if (listOfTopImages.size() < pageSize) {
                            IsMoreScroll = false;
                        } else {

                        }
                        gridView.setAdapter(null);
                        mainAdapter = new AllPopularImagesAdapter(getActivity(), listOfTopImages);
                        gridView.setAdapter(mainAdapter);
                        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(getActivity(), ViewImageScreen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                intent.putExtra("SelectedImage", mainAdapter.getItem(position));
                                Popular.liked = "0";
                                startActivity(intent);

                            }
                        });
                        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(AbsListView view, int scrollState) {
                            }

                            @Override
                            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                if (canScroll) {
                                    final int lastItem = firstVisibleItem + visibleItemCount;
                                    if (lastItem == totalItemCount && IsMoreScroll) {
                                        if (lastLastitem != lastItem) {
                                            lastLastitem = lastItem;
                                            pageStart = pageStart + pageSize;
                                            canScroll = false;
                                            getImagesWithCategoryID(selectedCategory, pageStart, pageSize, false);
                                        }
                                    }
                                }

                            }
                        });
                    } else if (IsFirstCall && listOfTopImages.size() == 0) {
                        gridView.setAdapter(null);
                        txtNoDataFound.setVisibility(View.VISIBLE);
                    } else {
                        if (listOfTopImages.size() < pageSize) {
                            IsMoreScroll = false;
                            mainAdapter.AddMyData(listOfTopImages);
                        } else {
                            mainAdapter.AddMyData(listOfTopImages);
                        }

                    }

                }

            } else {
                //   pDialog.cancel();
                Gson gsonTemp = new GsonBuilder().create();
                Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
//                Settings.ShowErrorDialog(getActivity(), 1, error, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // pDialog.cancel();
        }

    }

    public void getImagesWithCategoryID(CategoryData CategoryID, int pageStarts, int pageSizes, boolean IsFirstCall) {
        txtSelectedCategoryName.setText(CategoryID.getName() + "");
        if (IsFirstCall) {
            this.IsFirstCall = true;
            lastLastitem = 0;
            canScroll = true;
            IsMoreScroll = true;
            lastLastitem = 0;
            selectedCategory = CategoryID;
        } else {
            // pDialog.show();
            this.IsFirstCall = false;
            canScroll = false;
        }
        if (Settings.userObj == null) {
            requestCode = 1133;
            String url = Paths._wallpaper_by_category_id + CategoryID.getId() + "/" + pageStarts + "/" + pageSizes;

//            String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStarts+"&page_size="+pageSizes+"&category_id="+CategoryID.getId();
            WebServices.getInstance().MyGetCallWithoutAuth(getActivity(), url, this);
        } else {
            requestCode = 1144;
//            String url = Paths.BaseUrl + "wallpaper/index?page_num_start="+pageStarts+"&page_size="+pageSizes+"&user_id="+Settings.userObj.getId()+"&category_id="+CategoryID.getId();;
//            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, this);
            String url = Paths._wallpaper_by_category_id + CategoryID.getId() + "/" + pageStarts + "/" + pageSizes;
            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, this);
        }
    }

}
