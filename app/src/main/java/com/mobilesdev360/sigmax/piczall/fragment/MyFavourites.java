package com.mobilesdev360.sigmax.piczall.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.adapters.AllPopularImagesAdapter;
import com.mobilesdev360.sigmax.piczall.dataClass.Errors;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.Settings;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;


public class MyFavourites extends Fragment  implements WebServices.RequestResults{
    private static final String TAG = "MyFavourites";
    View rootView;
    List<ImageData> listOfTopImages;
    GridViewWithHeaderAndFooter gridView;
    AllPopularImagesAdapter mainAdapter;
    View loadMoreView;
    int pageStart=0,pageSize=18;
    boolean canScroll,IsMoreScroll,IsFirstCall=true;
    int lastLastitem=0;
    ProgressDialog pDialog;
    TextView txtNoDataFound;
    SwipeRefreshLayout pullToRefresh;
    ImageData imageData;

    public MyFavourites() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_my_favourites, container, false);
        ImageView imgMenu=(ImageView)rootView.findViewById(R.id.imgMenu);
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        Initialize();
        return rootView;
    }
    void Initialize(){
        listOfTopImages = new ArrayList();
        gridView = (GridViewWithHeaderAndFooter) rootView.findViewById(R.id.gridview);
        txtNoDataFound= (TextView) rootView.findViewById(R.id.txtNoDataFound);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

        pullToRefresh = rootView.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageStart=0;
                IsFirstCall=true;
                // pDialog.show();
                pageStart=0;
                lastLastitem=0;
                canScroll=true;
                IsMoreScroll=true;
                lastLastitem=0;

                String url = Paths.get_like_wallpaper_with_userid_or_all+Settings.userObj.getId()+"/"+pageStart+"/"+pageSize;
                WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, MyFavourites.this);

                pullToRefresh.setRefreshing(false);
            }
        });


        pageStart=0;
        IsFirstCall=true;
        // pDialog.show();
        pageStart=0;
        lastLastitem=0;
        canScroll=true;
        IsMoreScroll=true;
        lastLastitem=0;

          //  String url = Paths.BaseUrl + "user/like/wallpaper?page_num_start="+pageStart+"&page_size="+pageSize+"&user_id="+Settings.userObj.getId();
        String url = Paths.get_like_wallpaper_with_userid_or_all+Settings.userObj.getId()+"/"+pageStart+"/"+pageSize;
            Log.d(TAG, "Endpoint: " + url);
            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, this);




        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                //ViewImageScreen
                Intent intent=new Intent(getActivity(),ViewImageScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });

    }

    @Override
    public void requestFailed(VolleyError error) {
        pDialog.cancel();
      /*  VolleyError myError=error;
        if(error!=null){
            if(error.getMessage()!=null && !error.getMessage().equals(""))
                Settings.ShowErrorDialog(getActivity(),0,null,error.getMessage());
            else
                Settings.ShowErrorDialog(getActivity(),0,null,"Internet Or Server Error.");
        }else {
            Settings.ShowErrorDialog(getActivity(),0,null,"Internet Or Server Error.");
        }*/
    }

    @Override
    public void requestSucceeded(JSONObject response) {
        Log.d(TAG, String.valueOf(response));
        pDialog.cancel();
        canScroll=true;
        try {
            Log.d(TAG, String.valueOf(response.getString("status")));
            if (response.getString("status").equals("Successful")) {
                Gson gson = new GsonBuilder().create();
                final ImageData[] allImages = gson.fromJson(response.getJSONArray("featured_wallpaper").toString(), ImageData[].class);
                listOfTopImages=new ArrayList<>();
                for(int a=0;a<allImages.length;a++){
                    listOfTopImages.add(allImages[a]);
                }

                if(IsFirstCall==true && listOfTopImages.size()>0) {
                    txtNoDataFound.setVisibility(View.GONE);
                    IsFirstCall=false;
                    if(listOfTopImages.size()<pageSize){
                        IsMoreScroll=false;
                    }else{
                    }

                    mainAdapter= new AllPopularImagesAdapter(getActivity(),listOfTopImages);
                    gridView.setAdapter(mainAdapter);
                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent=new Intent(getActivity(),ViewImageScreen.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            intent.putExtra("SelectedImage",mainAdapter.getItem(position));
                            startActivity(intent);

                        }
                    });
                    gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {
                        }
                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            if(canScroll){
                                final int lastItem = firstVisibleItem + visibleItemCount;
                                if (lastItem == totalItemCount && IsMoreScroll) {
                                    if(lastLastitem != lastItem) {
                                        lastLastitem=lastItem;
                                        pageStart=pageStart+pageSize;
                                        canScroll=false;
                                      //  pDialog.show();
                                        String url = Paths.get_like_wallpaper_with_userid_or_all+Settings.userObj.getId()+"/"+pageStart+"/"+pageSize;
                                            WebServices.getInstance().MyGetCallWithAuth(getActivity(), url, MyFavourites.this);


                                    }
                                }
                            }

                        }});
                }else if(IsFirstCall && listOfTopImages.size()==0){

                    gridView.setAdapter(null);
                    txtNoDataFound.setVisibility(View.VISIBLE);
                }else {
                    if(listOfTopImages.size()<pageSize){
                        IsMoreScroll=false;
                        mainAdapter.AddMyData(listOfTopImages);
                    }else{
                        mainAdapter.AddMyData(listOfTopImages);
                    }

                }

            } else {
                Log.d(TAG, "Alert");
                Gson gsonTemp = new GsonBuilder().create();
                Errors error = gsonTemp.fromJson(response.toString(), Errors.class);
           //     Settings.ShowErrorDialog(getActivity(), 1, error, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            // pDialog.cancel();
        }

    }

}
