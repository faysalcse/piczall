package com.mobilesdev360.sigmax.piczall.adapters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.PagerAdapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.mobilesdev360.sigmax.piczall.R;
import com.mobilesdev360.sigmax.piczall.activities.Login;
import com.mobilesdev360.sigmax.piczall.activities.VideoViews;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageFullScreen;
import com.mobilesdev360.sigmax.piczall.activities.ViewImageScreen;
import com.mobilesdev360.sigmax.piczall.activities.ViewTrendingImageFullScreen;
import com.mobilesdev360.sigmax.piczall.activities.ViewTrendingImageScreen;
import com.mobilesdev360.sigmax.piczall.dataClass.ImageData;
import com.mobilesdev360.sigmax.piczall.dataClass.TrendingImageData;
import com.mobilesdev360.sigmax.piczall.utils.AppController;
import com.mobilesdev360.sigmax.piczall.utils.DownloadService;
import com.mobilesdev360.sigmax.piczall.utils.Paths;
import com.mobilesdev360.sigmax.piczall.utils.WebServices;
import com.mobilesdev360.sigmax.piczall.utils.model.SavedWallpaper;
import com.mobilesdev360.sigmax.piczall.utils.preferenceutil.SharedPref;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.varunest.sparkbutton.SparkButton;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import io.realm.RealmResults;

public class TrendingImageViewAdapter extends PagerAdapter {
    Activity context;
    List<TrendingImageData> listOfImages;
    Activity myActivity;
    LayoutInflater mLayoutInflater;
    ProgressDialog pDialog;
    String user_id = "";
    String liked = "0";
    Uri imageUri;
    Bitmap bitmap;
    private InterstitialAd mInterstitialAd;
    private UnifiedNativeAd nativeAd;
    private RewardedAd rewardedAd;

    int last_clicked_position = -1;
    public static final String TAG = "ImageViewAdapter";
//    NativeAd nativeAd;

    RealmResults<SavedWallpaper> allSavedWallpapers;
    ArrayList<SavedWallpaper> savedWallpapersList = new ArrayList<>();



    public TrendingImageViewAdapter(Activity context, List<TrendingImageData> listImages, Activity activity, String liked) {
        Log.d(TAG, "Liked Value:" + liked);
        SharedPref.init(context);
        user_id = SharedPref.read("user_id");
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listOfImages = listImages;
        myActivity = activity;
        pDialog = new ProgressDialog(activity);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.setMessage("Downloading...");
        this.liked = liked;
        loadFacebookInterstitialAd();
        allSavedWallpapers = AppController.realm.where(SavedWallpaper.class).findAll();

        if (allSavedWallpapers.size() > 0) {
            savedWallpapersList.addAll(AppController.realm.copyFromRealm(allSavedWallpapers));
        }
        AdLoader adLoader = new AdLoader.Builder(context, context.getString(R.string.native_ad))
                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        // Show the ad.
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());

        rewardedAd = new RewardedAd(context, context.getString(R.string.reward_video_ad));


        rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
    }

    private RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
        @Override
        public void onRewardedAdLoaded() {
            // Ad successfully loaded.
        }

        @Override
        public void onRewardedAdFailedToLoad(int errorCode) {
            // Ad failed to load.
        }
    };

    private void loadFacebookInterstitialAd(){
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(context.getString(R.string.interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });


    }


    @Override
    public int getCount() {
        return listOfImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.viewimage_screnn_item, container, false);

        final ImageView imageView = itemView.findViewById(R.id.imageView);
        ImageView imgShare = itemView.findViewById(R.id.imgShare);
        final SparkButton imgLike = itemView.findViewById(R.id.imgLike);
        ImageView imgDownload = itemView.findViewById(R.id.imgDownload);
        ProgressBar progressBar = itemView.findViewById(R.id.progress_bar);
        TextView txtImageName = itemView.findViewById(R.id.txtImageName);
        TextView txtNoLikes = itemView.findViewById(R.id.txtNoLikes);
        ImageView imgcross = itemView.findViewById(R.id.imgcross);

        View view = itemView.findViewById(R.id.cardMainImageContainer);

        FrameLayout frameLayout = itemView.findViewById(R.id.fl_adplaceholder);

        imgLike.setColors(ContextCompat.getColor(context, R.color.color_heart), ContextCompat.getColor(context, R.color.white_text));
        imgLike.setAnimationSpeed(1.5f);
        Log.w("NATIVE_ADS", "image view adapter position "+position);
        if(listOfImages.get(position).isAd){
            Log.w("NATIVE_ADS", "is ad "+position);

            frameLayout.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);

            loadNativeVideoAd(frameLayout);

            imgDownload.setVisibility(View.GONE);
            imgLike.setVisibility(View.GONE);
            imgShare.setVisibility(View.GONE);
            txtImageName.setText("ADVERTISEMENT");
        } else {
            frameLayout.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
            txtImageName.setText(listOfImages.get(position).getWallpaperName());
            Picasso.get()
                    .load(Paths._trending_media + listOfImages.get(position).getWallpaper()).placeholder(R.drawable.itembg)
                    .error(R.drawable.loading).fit()
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {

                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
        }


        txtNoLikes.setText(listOfImages.get(position).getLikeCount() + " Likes");
        TrendingImageData imageData=listOfImages.get(position);
        String like=imageData.getYoulike();
        if(position==0)
        {
            if (liked.equals("Yes")) {
                imgLike.setChecked(true);

            } else {
                imgLike.setChecked(false);
            }
        }
        else {
            if (liked.equals("Yes")) {
                imgLike.setChecked(true);

            } else {
                imgLike.setChecked(false);
            }
        }


        imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myActivity.finish();
            }
        });
        imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position%5==0)
                {
                    ((ViewTrendingImageScreen)context).showAds();
                }

                if (user_id.equals("")) {
                    Intent intent = new Intent(context, Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);

                } else {
                    if (imgLike.isChecked()) {
                        imgLike.setChecked(false);
                        String url = Paths.make_like_wallpaper + "/" + user_id + "/" + listOfImages.get(position).getId()+"/0";
                        Log.d(TAG, "like paper: " + url);
                        call_api1(url, Request.Method.POST, "", null);
                        String url_delete = Paths.remove_unlike_wallpaper + user_id+ "/" + listOfImages.get(position).getId();
                        Log.d(TAG, "delete paper: " + url_delete);
                        call_api2(url_delete, Request.Method.GET,"",null);
                    } else {
                        String url = Paths.make_like_wallpaper + "/" + user_id + "/" + listOfImages.get(position).getId()+"/1";
                        imgLike.setChecked(true);
                        call_api1(url, Request.Method.POST, "", null);
                    }


                }


            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myActivity, ViewTrendingImageFullScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("ImageSeletedForMain", listOfImages.get(position));
                myActivity.startActivity(intent);
            }
        });
        imgDownload.setOnClickListener(v -> {
            if (isWriteStorageAllowed()) {
                if(listOfImages.get(position).getIs_premium()!=null&&listOfImages.get(position).getIs_premium().equals("1")) {
                    View dialogView = View.inflate(context, R.layout.dialog_rewared_layout, null);
                    AlertDialog alertDialog = new AlertDialog.Builder(context)
                            .setView(dialogView)
                            .create();

                    LinearLayout showads = dialogView.findViewById(R.id.watch_video);

                    alertDialog.show();

                    RewardedAdCallback adCallback = new RewardedAdCallback() {
                        @Override
                        public void onRewardedAdOpened() {
                            // Ad opened.
                        }

                        @Override
                        public void onRewardedAdClosed() {
                            // Ad closed.
                            showads.setVisibility(View.VISIBLE);
                            rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
                        }

                        @Override
                        public void onUserEarnedReward(@NonNull RewardItem reward) {
                            alertDialog.dismiss();
                            showDownloadingDialog(Paths._trending_media + listOfImages.get(position).getWallpaper(),String.valueOf(listOfImages.get(position).getId()), position);
                        }

                        @Override
                        public void onRewardedAdFailedToShow(int errorCode) {
                            // Ad failed to display.
                            // Toast.makeText(context, "Failed to load ads", Toast.LENGTH_SHORT).show();
                            Log.e("tag", " = error code= =  " + errorCode);
                        }
                    };

                    showads.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(context, "Please wait for few moments to show ads", Toast.LENGTH_SHORT).show();
                            showads.setVisibility(View.GONE);
                            rewardedAd.show(context, adCallback);
                        }
                    });

                } else {

                    if (position % 3 == 0) {
                        ((ViewTrendingImageScreen) context).showAds();
                    }
                    showDownloadingDialog(Paths._trending_media + listOfImages.get(position).getWallpaper(),String.valueOf(listOfImages.get(position).getId()), position);
                }

            } else {
                Toast.makeText(myActivity, "Please Give Permission.", Toast.LENGTH_SHORT).show();
                requestwriteStoragePermission();
            }

        });

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position%5==0) {
                    ((ViewTrendingImageScreen)context).showAds();
                }
                DownloadImageForShare(Paths._trending_media + listOfImages.get(position).getWallpaper());

            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public void AddMyData(List<TrendingImageData> myListMew) {
        for (int a = 0; a < myListMew.size(); a++) {
            if (!listOfImages.contains(myListMew.get(a))) {
                listOfImages.add(myListMew.get(a));
            }
        }
       listOfImages = removeDuplicates(listOfImages);

        notifyDataSetChanged();
    }

    private void showDownloadingDialog(final String imageName,String wallpaperID, int position) {

        Log.e(TAG, "image name: " + imageName);

        final CharSequence[] items = {"Download", "Set For HomeScreen", "Set For LockScreen"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(myActivity);
        builder.setTitle("Downloading Type!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Download")) {
                    //DownloadImage(imageName, 100);
                    Intent intent = new Intent(myActivity, DownloadService.class);
                    intent.putExtra("ImageAndOption", imageName + ",100" + "," + listOfImages.get(position).getId());
                    myActivity.startService(intent);
                } else if (items[item].equals("Set For HomeScreen")) {
                    //DownloadImage(imageName, 200);
                    Intent intent = new Intent(myActivity, DownloadService.class);
                    intent.putExtra("ImageAndOption", imageName + ",200" + "," + listOfImages.get(position).getId());
                    myActivity.startService(intent);
                } else if (items[item].equals("Set For LockScreen")) {
                    //DownloadImage(imageName, 300);
                    Intent intent = new Intent(myActivity, DownloadService.class);
                    intent.putExtra("ImageAndOption", imageName + ",300" + "," + listOfImages.get(position).getId());
                    myActivity.startService(intent);
                }
                WebServices webServices=new WebServices(context);
                webServices.call_api(Paths.download_wallpaper+String.valueOf(wallpaperID),Request.Method.GET,"",null);
                dialog.dismiss();
            }
        });
        builder.show();
    }

    void DownloadImage(String imageName, final int OnlyDownload) {
        pDialog.setMessage("Downloading...");
        pDialog.show();
        Picasso.get()
                .load(Paths.OrignalImage + imageName)
                .into(new Target() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        SaveFileIntoStorage(bitmap);
                        // set On Wallpaper
                        if (OnlyDownload == 200) {
                            WallpaperManager myWallpaperManager = WallpaperManager.getInstance(myActivity);
                            try {

                                // myWallpaperManager.setBitmap(bitmap);
                                myWallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK);
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        } else if (OnlyDownload == 300) {
                            WallpaperManager myWallpaperManager
                                    = WallpaperManager.getInstance(myActivity);
                            try {
                                myWallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK);
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        pDialog.cancel();
                        Toast.makeText(myActivity, "Successfully Download", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        pDialog.cancel();
                        Toast.makeText(myActivity, "Downloading Failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
    }

    boolean SaveFileIntoStorage(Bitmap imgBitmap) {
        boolean isSave = true;
        File sd = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File imageFolder = new File(sd.getAbsolutePath() + File.separator + Paths.imagesPathLocal);

        if (!imageFolder.isDirectory()) {
            imageFolder.mkdirs();
        }

        File mediaFile = new File(imageFolder + File.separator + "img_" + System.currentTimeMillis() + ".jpg");

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(mediaFile);
            imgBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.close();

            Uri contentUri = Uri.fromFile(mediaFile);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(contentUri);
            context.sendBroadcast(mediaScanIntent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            isSave = false;
        } catch (IOException e) {
            e.printStackTrace();
            isSave = false;
        }
        return isSave;
    }

    public boolean isWriteStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(myActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        //If permission is granted returning true
        return result == PackageManager.PERMISSION_GRANTED;

        //If permission is not granted returning false
    }

    public void requestwriteStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(myActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(myActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
    }

    public boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(myActivity, Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        return result == PackageManager.PERMISSION_GRANTED;

        //If permission is not granted returning false
    }

    public void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(myActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(myActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
    }

    void DownloadImageForShare(String imageName) {
        pDialog.setMessage("Sharing...");
        pDialog.show();
        Picasso.get()
                .load(imageName)
                .into(new Target() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        SaveFileIntoStorageForShare(bitmap);
                        pDialog.cancel();
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        pDialog.cancel();

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
    }

    void SaveFileIntoStorageForShare(Bitmap imgBitmap) {
        boolean isSave = true;
        String fileName;

        File sd = Environment.getExternalStorageDirectory();
        File imageFolder = new File(sd.getAbsolutePath() + File.separator + Paths.imagesPathLocalForShare);

        if (!imageFolder.isDirectory()) {
            imageFolder.mkdirs();
        }

        File mediaFile = new File(imageFolder + File.separator + "img_" + System.currentTimeMillis() + ".jpg");
        fileName = mediaFile.getPath();
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(mediaFile);
            imgBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.close();
            Uri imagePath = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".my.package.name.provider", mediaFile);
            Intent share = new Intent(Intent.ACTION_SEND);
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, imagePath);
            myActivity.startActivity(Intent.createChooser(share, "Share Image Using"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            isSave = false;
        } catch (IOException e) {
            e.printStackTrace();
            isSave = false;
        }
    }

    public void call_api1(final String urL, final int Method, final String view, final Map<String, String> params) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Method, urL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("status_message");
                    if (status.equals("Successful") && message.equals("Wallpaper like successfully")) {
                        Toast.makeText(context, "Liked", Toast.LENGTH_SHORT).show();

                    } else if(status.equals("Successful") && message.equals("Wallpaper unlike successfully")) {
                        Toast.makeText(context, "Unliked", Toast.LENGTH_SHORT).show();
                    }
                    else{

                    }
                } catch (Exception e) {

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("error:", "Error-------" + volleyError.toString());

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
//            Map<String, String> params = new HashMap<String, String>();
                return params;
            }


        };

        requestQueue.add(postRequest);
    }
    public void call_api2(final String urL, final int Method, final String view, final Map<String, String> params) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Method, urL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if (status.equals("Successful")) {

                    }
                    else{

                    }
                } catch (Exception e) {

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("error:", "Error-------" + volleyError.toString());

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
//            Map<String, String> params = new HashMap<String, String>();
                return params;
            }


        };

        requestQueue.add(postRequest);
    }
    public List<TrendingImageData> removeDuplicates(List<TrendingImageData> list) {
        List<TrendingImageData> listImages = new ArrayList<>();
        // Set set1 = new LinkedHashSet(list);
        Set set = new TreeSet((o1, o2) -> {
            if (((TrendingImageData) o1).getWallpaperName()!=null && ((TrendingImageData) o1).getWallpaperName()!=null && ((TrendingImageData) o1).getWallpaperName().equals(((TrendingImageData) o2).getWallpaperName())) {
                String obj1=((TrendingImageData)o1).getYoulike();
                String obj2=((TrendingImageData)o2).getYoulike();
                return 0;

            }
            return 1;
        });
        set.addAll(list);

        listImages.addAll(set);
        return listImages;
    }

//    private final class StrandAdListener extends NativeAdEventListener {
//
//
//        public StrandAdListener() {
//        }
//
//        @Override
//        public void onAdLoadSucceeded(@NonNull InMobiNative inMobiNative) {
//            //Pass the old ad view as the first parameter to facilitate view reuse.
//            View view = loadAdIntoView(inMobiNative);
//            if (view == null) {
//                Log.d(TAG, "Could not render Strand!");
//            } else {
//                mContainer.addView(view);
//            }
//        }
//
//        @Override
//        public void onAdLoadFailed(@NonNull InMobiNative inMobiNative, @NonNull InMobiAdRequestStatus inMobiAdRequestStatus) {
//            Log.d(TAG, "Ad Load failed (" + inMobiAdRequestStatus.getMessage() + ")");
//        }
//
//        @Override
//        public void onAdReceived(InMobiNative inMobiNative) {
//            Log.d(TAG, "onAdReceived");
//        }
//
//        @Override
//        public void onAdFullScreenDismissed(InMobiNative inMobiNative) {
//        }
//
//        @Override
//        public void onAdFullScreenWillDisplay(InMobiNative inMobiNative) {
//            Log.d(TAG, "Ad going fullscreen.");
//        }
//
//        @Override
//        public void onAdFullScreenDisplayed(InMobiNative inMobiNative) {
//        }
//
//        @Override
//        public void onUserWillLeaveApplication(InMobiNative inMobiNative) {
//        }
//
//        @Override
//        public void onAdImpressed(@NonNull InMobiNative inMobiNative) {
//            Log.d(TAG, "Impression recorded successfully");
//        }
//
//        @Override
//        public void onAdClicked(@NonNull InMobiNative inMobiNative) {
//            Log.d(TAG, "Ad clicked");
//        }
//
//
//        @Override
//        public void onAdStatusChanged(@NonNull InMobiNative inMobiNative) {
//        }
//
//    }
//
//    private View loadAdIntoView(@NonNull final InMobiNative inMobiNative) {
//        View adView = LayoutInflater.from(context).inflate(R.layout.layout_ad, null);
//
//        ImageView icon = (ImageView) adView.findViewById(R.id.adIcon);
//        TextView title = (TextView) adView.findViewById(R.id.adTitle);
//        TextView description = (TextView) adView.findViewById(R.id.adDescription);
//        Button action = (Button) adView.findViewById(R.id.adAction);
//        FrameLayout content = (FrameLayout) adView.findViewById(R.id.adContent);
//        RatingBar ratingBar = (RatingBar) adView.findViewById(R.id.adRating);
//
//        Picasso.get()
//                .load(inMobiNative.getAdIconUrl())
//                .into(icon);
//        title.setText(inMobiNative.getAdTitle());
//        description.setText(inMobiNative.getAdDescription());
//        action.setText(inMobiNative.getAdCtaText());
//
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        content.addView(inMobiNative.getPrimaryViewOfWidth(context, content, mContainer, displayMetrics.widthPixels));
//
//        float rating = inMobiNative.getAdRating();
//        if (rating != 0) {
//            ratingBar.setRating(rating);
//        }
//        ratingBar.setVisibility(rating != 0 ? View.VISIBLE : View.GONE);
//
//        action.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mInMobiNative.reportAdClickAndOpenLandingPage();
//            }
//        });
//
//        return adView;
//    }

    private void loadNativeVideoAd(FrameLayout frameLayout) {
        AdLoader.Builder builder = new AdLoader.Builder(context, context.getString(R.string.native_ad));

        // OnUnifiedNativeAdLoadedListener implementation.
        builder.forUnifiedNativeAd(unifiedNativeAd -> {
            // otherwise you will have a memory leak.
            if (nativeAd != null) {
                nativeAd.destroy();
            }
            nativeAd = unifiedNativeAd;
            UnifiedNativeAdView adView = (UnifiedNativeAdView) context.getLayoutInflater().inflate(R.layout.ad_unified, null);
            populateUnifiedNativeAdView(unifiedNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        });

        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(false)
                .build();

        NativeAdOptions adOptions = new NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .build();

        builder.withNativeAdOptions(adOptions);

        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {

            }
        }).build();

        adLoader.loadAd(new AdRequest.Builder().build());

    }

    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Set the media view.
        adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        adView.setNativeAd(nativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = nativeAd.getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.
        if (vc.hasVideoContent()) {
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    super.onVideoEnd();
                }
            });
        }
    }


}