package com.mobilesdev360.sigmax.piczall.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mobilesdev360.sigmax.piczall.fragment.Category;
import com.mobilesdev360.sigmax.piczall.fragment.Feature;
import com.mobilesdev360.sigmax.piczall.fragment.MyFavourites;
import com.mobilesdev360.sigmax.piczall.fragment.Popular;
import com.mobilesdev360.sigmax.piczall.fragment.VideoWallpaper;

public class  PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Feature tab1=null;
    Category tab2=null;
    Popular tab3=null;
    VideoWallpaper tab4=null;
    MyFavourites tab5 = null;
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                 tab1 = new Feature();
                return tab1;
            case 1:
                 tab2 = new Category();
                return tab2;
            case 2:
                 tab3 = new Popular();
                return tab3;
            case 3:
                tab4=new VideoWallpaper();
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
